<?php

namespace App\Http\Controllers\Api;

use App\Models\Coin;
use App\Models\Movement;
use App\Models\TypeCoin;
use App\Models\CategoryCoins;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Storage;
use Illuminate\Http\Request;

class CoinController extends Controller
{
    public function list(){
        $coins = Coin::whereDate('data_end','>',Carbon::now())->get();
        return json_encode($coins);
    }
   public function getCoinUser($id){
          $movement_coin = Movement::select(\DB::raw('sum(quantity*price_actually) as price'),
          \DB::raw('sum(quantity) as quantity'), 'coin_id','name','initial','logo')
          ->join('coins','coins.id','=','movements.coin_id')
          ->where('client_id',$id)->where('movements.state','=','A')->groupBy('movements.coin_id')->get();
          
          $tot_coin = Movement::select(\DB::raw('sum(quantity*price_actually) as total'))
          ->where('client_id',$id)->where('state','=','A')->get();

          return json_encode(array($movement_coin, $tot_coin));

 
    }


    public function infoByClient($client_id){
        $query = DB::select("SELECT c.id, c.initial, c.name, sum(quantity*price_actually) as price, coin_price, 
        sum(quantity) as quantity, cash_back_fund FROM atrium.coins c 
        inner join movements m on m.coin_id = c.id
        where client_id=$client_id and m.state='A'
        group by c.id");

        return json_encode($query);
    }


    public function getCoin(Request $req){
        $coin = Coin::find($req->id);
        return json_encode($coin);
    }

    
    public function  saveMovement(Request $req){
    
    if($req->coin=='' || !isset($req->coin) || !is_numeric($req->coin)){
        return json_encode(['state'=>500, 'msj'=>'Coin Not Found']);
    }

     if($req->unity=='' || !isset($req->unity) || !is_numeric($req->unity)){
         return json_encode(['state'=>500, 'msj'=>'Unity of Coin Not Found']);
     }

    /*if ( !Hash::check($req->second_password, Auth::guard('customusers')->user()->SecondPassword) ) {
         return json_encode(['state'=>500, 'msj'=>'Second Password Incorrect']);
     }*/

    try{

        $mov = new Movement();
        $mov->client_id = $req->id;
        $mov->coin_id = $req->coin;
        $mov->quantity = $req->unity;
        $mov->method_payment_id = $req->method_payment;
        $actual_price = Coin::find($req->coin)->actual_price;
        if(is_null($actual_price)){
            return json_encode(['state'=>500, 'msj'=>'Not Found Price Actually Coin']);
        }
        $mov->price_actually = $actual_price;
        $mov->date_movement = Carbon::now();
        $mov->observation="Buy Movi Credit Card";
        $mov->state='P';
        $mov->save();
        return json_encode(['state'=>200,'msj'=>'buy success']);
    } 
    catch(\Exception $e){
        return json_encode(['state'=>500,'msj'=>$e->getMessage()]);
    }


}

}
