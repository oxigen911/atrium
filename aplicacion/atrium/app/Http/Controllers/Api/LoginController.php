<?php

namespace App\Http\Controllers\Api;

use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Keygen;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginApi(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata); 
        $user = Client::where('email',$request->email)->first();
        
        if(is_null($user)){
           return response()->json(['success' => false, 'msg'=>'User does not exist!'], 201);                
        }else{
            if(!\Hash::check($request->password, $user->Password)){
                return response()->json(['success' =>false, 'msg'=>'Account Inactive or Unregister'], 201);
            }else{
                return response()->json(['success' => true, 'result'=>$user], 201);
            }
        }
    }

    public function registerApi(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        $postdata = file_get_contents("php://input");
        $req = json_decode($postdata); 
	      $client = new Client();
            $client->FirstName=!isset($req->FirstName) || is_null($req->FirstName)? null:$req->FirstName;            
            $client->LastName=!isset($req->LastName) || is_null($req->LastName)? null:$req->LastName;         
            $client->Email=!isset($req->Email) || is_null($req->Email)? null:$req->Email;      
            $client->Password=\Bcrypt($req->Password);
            $client->SecondPassword=\Bcrypt($req->SecondPassword);
            
            $client->UserName_NDG=$this->generateCode();
            $client->ManagerId_NDG=$this->generateCode();
            $client->BirthDate=!isset($req->BirthDate) || is_null($req->BirthDate) || !$this->validateDate($req->BirthDate)? null:$req->BirthDate;        
            $client->CountryBirth=!isset($req->CountryBirth->id) || is_null($req->CountryBirth->id)? null:$req->CountryBirth->id;         
            $client->CountryResidency=!isset($req->CountryResidency->id) || is_null($req->CountryResidency->id)? null:$req->CountryResidency->id;
            $client->CityBirth=!isset($req->CityBirth->id) || is_null($req->CityBirth->id)? null:$req->CityBirth->id;         
            /*$client->Address=!isset($req->Address) || is_null($req->Address)? null:$req->Address;  */
            $client->CertificationResidency=!isset($req->CertificationResidency) || is_null($req->CertificationResidency)? null:$req->CertificationResidency;           
            $client->DocumentType=!isset($req->DocumentType->text) || is_null($req->DocumentType->text)? null:$req->DocumentType->text;
            $client->DocumentIdentification=!isset($req->NumberDocument) || is_null($req->NumberDocument) ? null:$req->NumberDocument;
            $client->DocumentIssueDate=!isset($req->DocumentIssueDate) || is_null($req->DocumentIssueDate) || !$this->validateDate($req->DocumentIssueDate)? null:$req->DocumentIssueDate;
            $client->DocumentexpirationDate=!isset($req->DocumentexpirationDate) || is_null($req->DocumentexpirationDate) || !$this->validateDate($req->DocumentexpirationDate)? null:$req->DocumentexpirationDate;
            
            $client->ConfirmationCode= Keygen::token(38)->generate();
            try {
                $client->save();
            } catch (Exception $e) {
                Log::info("error: ".$e->getMessage());
                return json_encode(['success'=>false, 'msg'=>$e->getMessage()]);
            }
            return json_encode(['success'=>true ,'msg'=>'Client Create Success!!','code'=>$client->UserName_NDG,'identy'=>$client->id]);
        

        
    }


    public function connect()
    {
		header('Access-Control-Allow-Origin: *'); 

        $message = array(
                "tipo" => "success",
                "msj" => "Connected to IglooExchenge!"
        );
        return response()->json(['data' => $message], 201);
    }

    protected function generateCode()
    {
        $code = $this->generateNumericKey();

        // Ensure ID does not exist
        // Generate new one if ID already exists
        while (Client::where('ManagerId_NDG',$code)->count() > 0) {
            $code = $this->generateNumericKey();
        }

        return $code;
    }

    protected function generateNumericKey()
    {
        // prefixes the key with a random integer between 1 - 9 (inclusive)
        return Keygen::numeric(9)->prefix(mt_rand(1, 9))->generate(true);
    }

    public function validateDate($date, $format = 'Y-m-d')
    {
        $d = \Carbon\Carbon::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

}
