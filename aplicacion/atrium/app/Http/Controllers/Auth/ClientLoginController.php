<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Client;
class ClientLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:customusers', ['except' => ['logout','showLoginForm','login']]);
    }

    public function showLoginForm()
    {
        return view('auth.login-client');
    }

    public function login(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('customusers')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
           if(Auth::guard('customusers')->user()->Confirmed==1 && Auth::guard('customusers')->user()->Approved!=null && Auth::guard('customusers')->user()->state=='A'){
               return redirect()->route('client.dashboard');
           }else{
               Auth::guard('customusers')->logout();
               return redirect()->route('login')->withInput($request->only('email', 'remember'))->withErrors('Account is not activate or not approved or inactive');

           }
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            return redirect()->route('dash.user_approbe');

        }



        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->route('login')->withInput($request->only('email', 'remember'))->withErrors('Credentials Invalid');
    }

    public function logout()
    {
        if(Auth::guard('customusers')){
            Auth::guard('customusers')->logout();

        }
        if(Auth::user()){
            Auth::logout();

        }
        return redirect()->route('login');
    }

     protected function credentials(Request $request)
    {

        return [
            'Email' => $request->get('email'),
            'Password' => $request->get('password')
        ];

    }

    /**
     * Determine if the request field is email or username.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function field(Request $request)
    {
        $email = $this->Email;

        return filter_var($request->get($email), FILTER_VALIDATE_EMAIL) ? $email : 'emailt';
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $field = $this->field($request);

        $messages = ["{$this->username()}.exists" => 'The account you are trying to login is not registered or it has been disabled.'];

        $this->validate($request, [
            $this->Email => "required|exists:users,{$field}",
            'password' => 'required',
        ], $messages);
    }
}
