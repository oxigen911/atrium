<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\PaymentController;
use App\Models\Client;
use App\Models\Coin;
use App\Models\Movement;
use App\Models\MethodPayment;
use Auth;
use Mail;
use DB;
use Log;
use App\Mail\buyMail;
use App\Notifications\clients;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;


class ClientController extends Controller
{
   public function __construct()
   {
        $this->middleware('auth:customusers', ['except' => ['verify','check']] );
   }



   public function verify($code)
	{	
	    $client = Client::where('ConfirmationCode', $code)->first();

	    if (! $client){
	        return redirect()->route('login')->withErrors(['Code Confirmation Invalid']);
      }  
	   
	    return view('dashboard.completeRegisterWithOutAutenticate', compact('client'))->withSuccess('You have successfully confirmed your email!');
	}




	public function index(){
		$coins = Coin::whereDate('data_end','>',Carbon::now())->paginate(15);
		$movements = Movement::where('client_id',Auth::user()->id)->orderBy('date_movement','desc')->paginate(5);
    $movement_coin = Movement::select(\DB::raw('sum(quantity*price_actually) as price'),\DB::raw('sum(quantity) as quantity'), 'coin_id','movements.state as state')->where('client_id',Auth::user()->id)->where('state','A')->groupBy('coin_id')->paginate(10);
    $movement_pendi = Movement::select(\DB::raw('sum(quantity*price_actually) as price'),\DB::raw('sum(quantity) as quantity'), 'coin_id','movements.state as state')->where('client_id',Auth::user()->id)->where('state','P')->groupBy('coin_id')->paginate(10);

		return view('client.index', compact('coins','movements','movement_coin', 'movement_pendi'));
	}




	public function buyer(Request $req){

       if(isset($req->coin_id)){
           $coin = Coin::find($req->coin_id);
        }else{
           $coin=null;
        }
        
        $coins = Coin::whereDate('data_end','>',Carbon::now())->get();
		if(count($coins)==0){
		    $coins=null;
        }

		if(is_null($coins) and is_null($coin)){
            return redirect()->route('client.dashboard')->withErrors(['Not Coin Available']);
        }

        $atm = Movement::select(\DB::raw('sum(quantity*price_actually) as price'),
        \DB::raw('sum(quantity) as quantity'), 'coin_id','movements.state as state')->
        where('client_id',Auth::user()->id)->where('state','A')->where('coin_id',11)->first();
        

        $metodo_pagos =  MethodPayment::where('state','A')->get();
		return view('client.buyer',compact('coin', 'coins','metodo_pagos','atm'));
	}

   

	public function check(Request $request)
    {
     if($request->get('email'))
     {
      $email = $request->get('email');
      $data = Client::where('email', $email)->count();
	   
   if($data > 0)
      {
       return json_encode('not_unique');
      }
      else
      {
       return json_encode('unique');
      }
     }
    }



    public function buy(Request $req){

       if($req->coin=='' || !isset($req->coin) || !is_numeric($req->coin)){
           return json_encode(['state'=>500, 'msj'=>'Coin Not Found']);
       }

        if($req->price=='' || !isset($req->price) || !is_numeric($req->price)){
            return json_encode(['state'=>500, 'msj'=>'Coin Price Not Found']);
        }

        if($req->unity=='' || !isset($req->unity) || !is_numeric($req->unity)){
            return json_encode(['state'=>500, 'msj'=>'Unity of Coin Not Found']);
        }

       if ( !Hash::check($req->second_password, Auth::guard('customusers')->user()->SecondPassword) ) {
            return json_encode(['state'=>500, 'msj'=>'Second Password Incorrect']);
        }

       try{

           $mov = new Movement();
           $mov->client_id = Auth::user()->id;
           $mov->coin_id = $req->coin;
           $mov->quantity = $req->unity;
           $actual_price = Coin::find($req->coin)->actual_price;
           if(is_null($actual_price)){
               return json_encode(['state'=>500, 'msj'=>'Not Found Price Actually Coin']);
           }
           $mov->price_actually = $actual_price;
           $mov->date_movement = Carbon::now();
           $mov->state='P';
           $mov->save();
           try {
              PaymentController::payWithpaypal($req);
           } catch (Exception $e) {
             Log::info($e->getMessage());
              return json_encode(['state'=>500, 'msj'=>$e->getMessage()]);  
           }
           Mail::to(Auth::user()->Email)->send(new buyMail($mov));

           return json_encode(['state'=>200, 'msj'=>'Your purchase was successful, you will receive an email with the details of the transaction']);
       }catch (\Exception $e){
           Log::info($e->getMessage());
           return json_encode(['state'=>500, 'msj'=>$e->getMessage()]);

       }


    }

    public function profileAccount(Request $req){
        return view('client.account');
    }

    public function settings(){
        return view('client.settings');
    }

    public function sendRecoverySecondPassword(Request $req){
        if($req->password!=$req->confirmation_password){
           return redirect()->route('client.settings')->withErrors('Password not Equals');
         }   

         try {
                $client = Client::find(Auth::user()->id);
                $client->SecondPassword = \Bcrypt($req->password);
                $client->save();
               return redirect()->route('client.settings')->withSuccess('Second Password Changed Success');
            } catch (\Exception $e) {
               return redirect()->route('client.settings')->withErrors($e->getMessage());
            }
    }  

    
    
    
}
