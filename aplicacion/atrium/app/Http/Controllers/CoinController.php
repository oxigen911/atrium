<?php

namespace App\Http\Controllers;

use App\Models\Coin;
use App\Models\Movement;
use App\Models\TypeCoin;
use App\Models\CategoryCoins;
use DB;
use Storage;
use Illuminate\Http\Request;

class CoinController extends Controller
{
    public function index(){
    	$coins = Coin::all();
      $category = CategoryCoins::all();
      $type = TypeCoin::all();
    	return view('coin.index', compact('coins','category','type'));
    }

    public function store(Request $req){
    	if($req->name=='' || is_null($req->name) || $req->initial=='' || is_null($req->initial) || $req->project_value=='' || is_null($req->project_value)  || $req->coin_price=='' || is_null($req->coin_price) 
        || $req->actual_price=='' || is_null($req->actual_price) 
        || $req->data_start=='' || is_null($req->data_start) 
        || $req->data_end=='' || is_null($req->data_end)){
        return back()->withInput()->withErrors(['Field Requierd']);
      }

    	try {
        	$peticion = $req->all();    
			 

	    	if($req->file('logo')){
	    		  $uploadedFile = $req->file('logo');
		     	  $filename = time().$uploadedFile->getClientOriginalName();

			      Storage::disk('local')->putFileAs(
			        'files/',
			        $uploadedFile,
			        $filename
			      );	

			      $peticion['logo']=$filename;
	    	}

          if($req->file('white_paper')){
                  $uploadedFile2 = $req->file('white_paper');
                  $filename2 = time().$uploadedFile2->getClientOriginalName();

                  Storage::disk('local')->putFileAs(
                    'files/',
                    $uploadedFile2,
                    $filename2
                  );    

                  $peticion['white_paper']=$filename2;
            }

             if($req->file('info')){
                  $uploadedFile3 = $req->file('info');
                  $filename3 = time().$uploadedFile3->getClientOriginalName();

                  Storage::disk('local')->putFileAs(
                    'files/',
                    $uploadedFile3,
                    $filename3
                  );    

                  $peticion['info']=$filename3;
            }

            

            Coin::create($peticion);
    	} catch (\Exception $e) {
    		return back()->withInput()->withErrors([$e->getMessage()]);
    	}

    	return Redirect()->route('coin.list')->withSuccess('Done Coin');
    }


    public function update(Request $req){


        $req->validate([
            'name' => 'required',
            'initial' => 'required',
            'project_value' => 'required',
            'coin_price' => 'required',
            'actual_price' => 'required',
            'data_start' => 'required',
            'data_end' => 'required'

        ], [

            'name.required' => 'Name is required',

            'initial.required' => 'Password is required'

        ]);

      try {
          $peticion = $req->all();

        if($req->hasFile('logo')){
            $uploadedFile = $req->file('logo');
            $filename = time().$uploadedFile->getClientOriginalName();

            Storage::disk('local')->putFileAs(
              'files/',
              $uploadedFile,
              $filename
            );  

            $peticion['logo']=$filename;
        }

          if($req->hasFile('white_paper')){
                  $uploadedFile2 = $req->file('white_paper');
                  $filename2 = time().$uploadedFile2->getClientOriginalName();

                  Storage::disk('local')->putFileAs(
                    'files/',
                    $uploadedFile2,
                    $filename2
                  );    

                  $peticion['white_paper']=$filename2;
            }


           if($req->hasFile('info')){
                  $uploadedFile3 = $req->file('info');
                  $filename3 = time().$uploadedFile3->getClientOriginalName();

                  Storage::disk('local')->putFileAs(
                    'files/',
                    $uploadedFile3,
                    $filename3
                  );    

                  $peticion['info']=$filename3;
            }


           $coin = Coin::find($req->coin);
            $coin->name=$req->name;
            $coin->initial=$req->initial;

            if(isset($peticion['logo'])){
               $coin->logo=$peticion['logo'];
            }
          if(isset($peticion['white_paper'])){
              $coin->white_paper=$peticion['white_paper'];
          }

          if(isset($peticion['info'])){
              $coin->info=$peticion['info'];
          }

            $coin->project_value=$req->project_value;
            $coin->coin_price=$req->coin_price;
            $coin->actual_price=$req->actual_price;
            $coin->data_start=$req->data_start;
            $coin->data_end=$req->data_end;
            $coin->category_id=$req->category;
            $coin->type_id=$req->type_id;
            $coin->link=$req->link;
            $coin->save();

      } catch (\Exception $e) {
        return back()->withInput()->withErrors([$e->getMessage()]);
      }

      return Redirect()->route('coin.list')->withSuccess('Update Coin');
    }

    public function getCoin(Request $req){
        if($req->coin == '' || is_null($req->coin)){
          return json_encode(['state'=>500, 'msj'=>'Coin Not Valid']);
        }

        $coin = Coin::find($req->coin);
        $arre = [];

        if(!is_null($coin)){
            $arre['name']=$coin->name;
            $arre['initial']=$coin->initial;
            $arre['project_value']=$coin->project_value;
            $arre['coin_price']=$coin->coin_price;
            $arre['cash_back_fund']=$coin->cash_back_fund;
            $arre['issued_coins']=$coin->issued_coins;
	    $arre['actual_price']=$coin->actual_price;
            $arre['data_start']=$coin->data_start->format('Y-m-d');
            $arre['data_end']=$coin->data_end->format('Y-m-d');
            $arre['category_id']=$coin->category_id;
            $arre['type_id']=$coin->type_id;
            $arre['link']=$coin->link;


            return json_encode(['state'=>200, 'data'=>$arre]);

        }else{
          return json_encode(['state'=>500, 'msj'=>'not result:'.$req->coin]);
        }
    }

    function available(Request $req){
        $coin = Coin::find($req->coin);
        if(is_null($coin)){
          return json_encode(['state'=>200, 'available'=>0.00]);
        }
        $mov  = $coin->Movements()->sum('quantity');
        if(is_null($mov)){
          return json_encode(['state'=>200, 'available'=>$coin->project_value]);
        }
        if($coin->project_value-$mov==0){
            return json_encode(['state'=>200, 'available'=>0.00]);
        }

        return json_encode(['state'=>200, 'available'=>number_format($coin->project_value-$mov,5, ',', '.')]);
    }


    function saveBuyUser(Request $req){
        DB::beginTransaction();

        $move = new Movement();
        $move->client_id = $req->client;
        $move->coin_id = $req->coin;
        $move->price_actually = Coin::find($req->coin)->actual_price;
        $move->quantity = $req->quantity;
        $move->method_payment_id = 1;
        $move->date_movement= \Carbon\Carbon::now();
        $move->state = 'A';
        $move->observation = 'Tranfer Bank';
        try{
            $move->save();
            DB::commit();
        }catch (\Exception $e){
            DB::rollback();
            return redirect()->route('register.user')->withErrors([$e->getMessage()]);
        }

        return redirect()->route('register.user')->withSuccess('Add Coin Success');

    }
}
