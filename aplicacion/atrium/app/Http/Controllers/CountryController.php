<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use DB;

class CountryController extends Controller
{
    public function getState(Request $req){
    	 if($req->country!=''){

    	 	$state = State::where('country_id',$req->country)->get();

    	 	if($state==FALSE){
    	 		return "<option>Not Found</option>";
    	 	}

    	 	$html='';


            $html = $html. '<option value="null">Select State</option>';
    	 	foreach ($state as $key => $value) {
    	 		$html = $html. '<option value="'.$value->id.'">'.$value->name.'</option>';
    	 	}

    	 	return $html;
    	 }
    }



    public function getCity(Request $req){
    	 if($req->country!=''){

    	 	$city = City::where('country_id',$req->country)->get();

    	 	if($city==FALSE){
    	 		return "<option>Not Found</option>";
    	 	}

    	 	$html='';
            $html = $html. '<option value="null">Select City</option>';

    	 	foreach ($city as $key => $value) {
    	 		$html = $html. '<option value="'.$value->id.'">'.$value->name.'</option>';
    	 	}

    	 	return $html;
    	 }
    }

    public function getCityWithOutState(Request $req){
    	if($req->city!=''){

    	 	$city = City::where('country_id',$req->city)->get();

    	 	if($city==FALSE){
    	 		return "<option>Not Found</option>";
    	 	}

    	 	$html='';


            $html = $html. '<option value="null">Select City</option>';
    	 	foreach ($city as $key => $value) {
    	 		$html = $html. '<option value="'.$value->id.'">'.$value->name.'</option>';
    	 	}

    	 	return $html;
    	 }
    }


    public function fetch(Request $request)
    {
     if($request->get('q'))
     {
      $query = $request->get('q');
      $data = DB::table('countries')
        ->where('name', 'LIKE', "%{$query}%")
        ->get();
      $output = [];
      foreach($data as $row)
      {
       $output[]= ['id'=>$row->id, 'text'=>$row->name];
      }
      
      echo json_encode($output);
     }
     else{

         $data = Country::limit(20)->get();
         $output = [];
         foreach($data as $row)
         {
             $output[]= ['id'=>$row->id, 'text'=>$row->name];
         }

             echo json_encode($output);
         }

    }



    public function fetchCity(Request $request)
    {
        if($request->get('search'))
        {
            $query = $request->get('search');
            $country= $request->get('type');
            $data = DB::table('cities')
                ->where('name', 'LIKE', "%{$query}%")
                ->where('country_id',$country)
                ->get();
            $output = [];
            foreach($data as $row)
            {
                $output[]= ['id'=>$row->id, 'text'=>$row->name];
            }

            echo json_encode($output);
        }
        else{

            $country= $request->get('type');
            $data = DB::table('cities')
                ->where('country_id',$country)
                ->limit(10)
                ->get();
            $output = [];
            foreach($data as $row)
            {
                $output[]= ['id'=>$row->id, 'text'=>$row->name];
            }

            echo json_encode($output);

        }
    }


    public function getCityApi($pais)
    {
            $data = DB::table('cities')
                ->where('country_id',$pais)
                ->get();
            $output = [];
            foreach($data as $row)
            {
                $output[]= ['id'=>$row->id, 'text'=>$row->name];
            }

            echo json_encode($output);

	}
	
    public function countryAll(Request $request)
    {
     if($request->get('q'))
     {
      $query = $request->get('q');
      $data = DB::table('countries')
        ->where('name', 'LIKE', "%{$query}%")
        ->get();
      $output = [];
      foreach($data as $row)
      {
       $output[]= ['id'=>$row->id, 'text'=>$row->name];
      }
      
      echo json_encode($output);
     }
     else{

         $data = Country::all();
         $output = [];
         foreach($data as $row)
         {
             $output[]= ['id'=>$row->id, 'text'=>$row->name];
         }

             echo json_encode($output);
         }

    }


}
