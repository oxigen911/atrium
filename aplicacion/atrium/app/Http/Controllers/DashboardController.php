<?php

namespace App\Http\Controllers;
use App\Models\Client;
use App\Models\AccountBank;
use App\Models\Country;
use App\Models\Coin;
use App\Models\State;
use App\Mail\approve;
use App\Models\City;
use App\Models\Movement;
use Illuminate\Http\Request;
use Redirect;
use Auth;
use Illuminate\Support\Arr;
use App\Notifications\clients;
use App\Mail\demoMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Keygen;
use GuzzleHttp;
use App\Jobs\NewBuyerEmail;
use App\Jobs\AprobarTranferencia;
use App\Jobs\DenegarTranferencia;
use Log;
use DB;

class DashboardController extends Controller
{
    public function index(){
    	$clients = Client::where('state','A')->whereNull('Approved')->WhereNull('Deny')->paginate(15);
        $movements = Movement::where('state','P')->paginate(15);

	return view('dashboard.index', compact('clients', 'clientsAprobe', 'movements'));
    }
    public function register(){
    	$clients = Client::where('state','A')->whereNotNull('Approved')->paginate(15);
        $coins = Coin::all();
    	return view('dashboard.register2', compact('clients','coins'));
    }

    public function registerWithOutAutenticate(){
        return view('dashboard.registerWithOutAutenticate');
    }

    public function test(){
        $movement = Movement::find(135);
        $client = Client::all()->first();
         
        return view('mails.layaout', compact('movement', 'client'));
       /*

 $clients = \App\Models\Client::where('state','A')->where('Confirmed',0)->get();

        foreach($clients as  $client){
            try {
                Mail::to($client->Email)->send(new demoMail($client));        
            } catch (\Exception $e) {
                Log::info($e->getMessage());
            }
         
        }



       $a = DB::select("select * from Ciudades where Paises_Codigo='CO'");

        foreach ($a as $ciudades){
            $miciudad = DB::select("select * from cities where upper(name)='".$ciudades->Ciudad."' limit 1");
            if($miciudad!=null){
                $mipais = DB::select("select * from Paises where Codigo='".$ciudades->Paises_Codigo."' limit 1");
                if(!$mipais!=null){
                    echo "esta ciudad :".$ciudades->Ciudad." pero en otro pais:".$ciudades->Codigo."<br>";
                }
            }else{
                $mipais = DB::select("select * from Paises where Codigo='".$ciudades->Paises_Codigo."' limit 1");
                echo "la ciudad: ".$ciudades->Ciudad." no se encuentra en la bd PAIS: ".$mipais[0]->Pais."<br>";
            }
        }

        
        $client = new GuzzleHttp\Client();

        $abcdario = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

        $res = $client->get('https://en.wikipedia.org/wiki/List_of_towns_and_cities_with_100,000_or_more_inhabitants/cityname:_A');
        
        echo $res->getBody()->getContents();
       */
    }

    public function storeUser(Request $req){

        if($req->ajax()){
            $arr = $req->all();
            Log::info(json_encode($arr));
            $client = new Client();
            $client->FirstName=!isset($req->FirstName) || is_null($req->FirstName)? null:$req->FirstName;            
            $client->LastName=!isset($req->LastName) || is_null($req->LastName)? null:$req->LastName;         
            $client->Email=!isset($req->Email) || is_null($req->Email)? null:$req->Email;      
            $client->Password=\Bcrypt($req->password);
            $client->SecondPassword=\Bcrypt($req->password_2);
            
            $client->UserName_NDG=$this->generateCode();
            $client->ManagerId_NDG=$this->generateCode();
            $client->BirthDate=!isset($req->BirthDate) || is_null($req->BirthDate) || !$this->validateDate($req->BirthDate)? null:$req->BirthDate;        
            $client->CountryBirth=!isset($req->CountryBirth) || is_null($req->CountryBirth)? null:$req->CountryBirth;         
            $client->DistrictBirth=!isset($req->DistrictBirth) || is_null($req->DistrictBirth)? null:$req->DistrictBirth;            
            $client->CityBirth=!isset($req->CityBirth) || is_null($req->CityBirth)? null:$req->CityBirth;            
            $client->CountryResidency=!isset($req->CountryResidency) || is_null($req->CountryResidency)? null:$req->CountryResidency;
            $client->Address=!isset($req->Address) || is_null($req->Address)? null:$req->Address;  
            $client->CertificationResidency=!isset($req->CertificationResidency) || is_null($req->CertificationResidency)? null:$req->CertificationResidency;           
            $client->DocumentType=!isset($req->DocumentType) || is_null($req->DocumentType)? null:$req->DocumentType;
            $client->DocumentIdentification=!isset($req->DocumentIdentification) || is_null($req->DocumentIdentification)? null:$req->DocumentIdentification;
            $client->DocumentIssueDate=!isset($req->DocumentIssueDate) || is_null($req->DocumentIssueDate) || !$this->validateDate($req->DocumentIssueDate)? null:$req->DocumentIssueDate;
            $client->DocumentexpirationDate=!isset($req->DocumentexpirationDate) || is_null($req->DocumentexpirationDate) || !$this->validateDate($req->DocumentexpirationDate)? null:$req->DocumentexpirationDate;
            $client->ConfirmationCode= Keygen::token(38)->generate();
            try {
                $client->save();
            } catch (Exception $e) {
                Log::info("error: ".$e->getMessage());
                return json_encode(['state'=>500, 'msj'=>$e->getMessage()]);
            }
            Log::info("succe:".$client->UserName_NDG.'identy:'.$client->id );
            return json_encode(['state'=>200 ,'msj'=>'guardado','code'=>$client->UserName_NDG,'identy'=>$client->id]);
        }

        if($req->identy){
            
            $cl = Client::find($req->identy);

            $cl->confirmed = true;
            $cl->ConfirmationCode = null;
            
            $cl->DocumentType=!isset($req->DocumentType) || is_null($req->DocumentType)? null:$req->DocumentType;
            $cl->DocumentIdentification=!isset($req->DocumentIdentification) || is_null($req->DocumentIdentification)? null:$req->DocumentIdentification;
            $cl->DocumentIssueDate=!isset($req->DocumentIssueDate) || is_null($req->DocumentIssueDate)? null:$req->DocumentIssueDate;
            $cl->DocumentexpirationDate=!isset($req->DocumentexpirationDate) || is_null($req->DocumentexpirationDate)? null:$req->DocumentexpirationDate;
            $cl->CertificationResidency=!isset($req->CertificationResidency) || is_null($req->CertificationResidency)? null:$req->CertificationResidency;
            $cl->DocumentImage=!isset($req->DocumentImage) || is_null($req->DocumentImage)? null:$req->DocumentImage;
            $cl->NumberCard=!isset($req->NumberCard) || is_null($req->NumberCard)? null:$req->NumberCard;           
            try{
                $cl->save();
            }catch(Exception $e){
                return back()->withInput()->withErrors([$e->getMessage()]);

            }

            if(isset($req->EthereumWallet) && !is_null($req->EthereumWallet)){
                $acc = new AccountBank();

                $acc->client_id = $cl->id;
                $acc->EthereumWallet=!isset($req->EthereumWallet) || is_null($req->EthereumWallet)? null:$req->EthereumWallet;
                $acc->IBAN=!isset($req->IBAN) || is_null($req->IBAN)? null:$req->IBAN;
                $acc->HeaderAccount=!isset($req->HeaderAccount) || is_null($req->HeaderAccount)? null:$req->HeaderAccount;
                $acc->BankAccount=!isset($req->BankAccount) || is_null($req->BankAccount)? null:$req->BankAccount;
                $acc->SwiftCode=!isset($req->SwiftCode) || is_null($req->SwiftCode)? null:$req->SwiftCode;
                $acc->Prefered=!isset($req->Prefered) || is_null($req->Prefered)? null:'S';

                try{
                    $acc->save();
                }catch(Exception $e){
                    Log::info($e->getMessage());
                    return back()->withInput()->withErrors([$e->getMessage()]);

                }
            }


            return Redirect()->route('login')->withSuccess('Register Completed');


        }
/*
    	try {
            $peticion = $req->all();
            $peticion['UserName_NDG']=str_shuffle('dasdgqwgq');
            $peticion['CountryResidency']=1;
            $peticion['Address']='Mi Direccion';
            Client::create($peticion);
    	} catch (\Exception $e) {
    		return back()->withInput()->withErrors([$e->getMessage()]);
    	}


*/
    	return Redirect()->route('dash.user_approbe')->withSuccess('Done User');
    }

    public function approve(Request $req){
        try {
            
            $client = Client::find($req->clientId);
            if(is_null($client)){
                return json_encode(['msj'=>'No Existe Cliente','state'=>500]);    
            }
            if(!is_null($client->Deny)){
                return json_encode(['msj'=>'Este Cliente ya fue Denegado','state'=>500]);
            }
            $client->Approved=\Carbon\Carbon::now();
            $client->ApprovedBy=Auth::user()->id;
            $client->save();
        

        try {
               Mail::to($client->Email)->send(new approve($client)); 
        } catch (\Exception $e) {
                Log::info($e->getMessage());
               return json_encode(['msj'=>$e->getMessage(),'state'=>500]);    
        }

            
   
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return json_encode(['msj'=>$e->getMessage(),'state'=>500]);   
        }

        return json_encode(['msj'=>'Approved','state'=>200]);
    }


    public function approveTransaction(Request $req){
        try {
            DB::beginTransaction();
            $mov = Movement::find($req->movId);
            if($mov->state=='A'){
                return json_encode(['msj'=>'ya fue aprobado','state'=>500]);    
            }
            if(is_null($mov)){
                return json_encode(['msj'=>'No Existe Movements o ya fue aprobado','state'=>500]);    
            }
            
            $mov->state='A';
            $mov->date_approve = \Carbon\Carbon::now();
            $mov->save();
            
            AprobarTranferencia::dispatch($mov)->delay(now()->addMinutes(1));
            DB::commit();
        } catch (\Exception $e) {
                Log::info($e->getMessage());
                DB::rollback();
               return json_encode(['msj'=>$e->getMessage(),'state'=>500]);    
        }

            
   
        
        return json_encode(['msj'=>'Approved','state'=>200]);
    }



    public function denyTransaction(Request $req){
        try {
            
            DB::beginTransaction();
            $mov = Movement::find($req->movId);
            if($mov->state=='I'){
                return json_encode(['msj'=>'ya estaba anulado','state'=>500]);    
            }
            if(is_null($mov)){
                return json_encode(['msj'=>'No Existe Movements o ya fue aprobado','state'=>500]);    
            }
            
            $mov->state='I';
            $mov->date_deny = \Carbon\Carbon::now();
            $mov->save();
            
            DenegarTranferencia::dispatch($mov)->delay(now()->addMinutes(1));
            DB::commit();
        } catch (\Exception $e) {
                Log::info($e->getMessage());
                DB::rollback();
               return json_encode(['msj'=>$e->getMessage(),'state'=>500]);    
        }

            
   
        
        return json_encode(['msj'=>'Approved','state'=>200]);
    }

    public function deny(Request $req){
        try {
            
            $client= Client::find($req->clientId);
             if(is_null($client)){
                return json_encode(['msj'=>'No Existe Cliente','state'=>500]);    
            }
            if(!is_null($client->Approved)){
                return json_encode(['msj'=>'Este Cliente ya fue Aprobado','state'=>500]);
            }
            $client->Deny=\Carbon\Carbon::now();
            $client->DenyBy=Auth::user()->id;
            $client->DenyReason=$req->motivoRechazo;
            $client->save();
   
        } catch (\Exception $e) {      
            Log::info($e->getMessage());      
            return json_encode(['msj'=>$e->getMessage(),'state'=>500]);   
        }

   return json_encode(['msj'=>'Deny','state'=>200]);
    }


    protected function generateNumericKey()
    {
        // prefixes the key with a random integer between 1 - 9 (inclusive)
        return Keygen::numeric(9)->prefix(mt_rand(1, 9))->generate(true);
    }


    protected function generateCode()
    {
        $code = $this->generateNumericKey();

        // Ensure ID does not exist
        // Generate new one if ID already exists
        while (Client::where('ManagerId_NDG',$code)->count() > 0) {
            $code = $this->generateNumericKey();
        }

        return $code;
    }

    public function delete(Request $req){

        try{

            $client = Client::find($req->id);
            $client->state='I';
            $client->save();
            return back()->withSuccess('Client Desactived');
        }catch (\Exception $e){
            Log::info($e->getMessage());
            return back()->withErrors([$e->getMessage()]);
        }


    }

   public function validateDate($date, $format = 'Y-m-d')
    {
        $d = \Carbon\Carbon::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function sendConfirmMail(Request $req){

        $client = Client::find($req->client);
        if($client->Confirmed==0){
         try { 
                Mail::to($client->Email)->send(new demoMail($client));        
                return response()->json('Mail Send',200);
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                return response()->json($e->getMessage(),500);
            }
        }else{

            return response()->json('Account it\'s verified',500);

        }
           
    }
    
    public function detailClient(Request $req){
        $id = $req->client;
        $client = Client::find($id);
        if(is_null($client)){
            return response()->json(['state'=>500, 'data'=>'Don\'t exist clients']);
        }
        //$movs = Movement::where('client_id', $client->id)->groupBy('coin_id','state')
        //->select('coin_id',DB::raw('SUM(quantity) as quantity'),DB::raw('SUM(price_actually) as price_actually'),'state')->get();
        $movs = Movement::where('client_id', $client->id)
        ->select('coin_id',DB::raw('quantity'),DB::raw('price_actually'),'state')->
        orderBy('coin_id')->get();
        
        if(is_null($movs) || count($movs)==0){
            return response()->json(['state'=>500, 'data'=>'Don\'t exist movement for clients']);
        }
        
        $r=[];
        foreach($movs as $mov){
            $r[]=array('coin'=>$mov->coin->name, 
            'quantity'=>number_format($mov->quantity,5, ',', '.'),'price_actually'=>number_format($mov->price_actually,5, ',', '.'),'state'=>$mov->stated);
        }
        
        return response()->json(['state'=>200, 'data'=>$r]);
    }

    public function getClient(Request $req){
        $id = $req->client;
        $client = Client::find($id);
        if(is_null($client)){
            return response()->json(['state'=>500, 'data'=>'Don\'t exist clients']);
        }
        $client['Country']=$client->Country->name;
        $client['created']=$client->created_at->format('d.m.Y');
        return response()->json(['state'=>200, 'data'=>$client]);
    }

    public function getCoinPercentaje(Request $req){
        $id = $req->client;
        $client = Client::find($id);
        if(is_null($client)){
            return response()->json(['state'=>500, 'data'=>'Don\'t exist clients']);
        }
    $movs = Movement::where('client_id', $client->id)->where('state','A')->groupBy('coin_id')
        ->join('coins','coins.id','=','movements.coin_id')
        ->select('coin_id',DB::raw('SUM(quantity) as quantity'),
        DB::raw('SUM(price_actually) as price_actually'),'coins.name as name','coins.initial as initial')->get();
        
        if(count($movs)==0){
            return response()->json(['state'=>500, 'data'=>'Don\'t exist coins Authorized']);
        }
        return response()->json(['state'=>200, 'data'=>$movs]);
    }

    function confirmWithOutFix($ConfirmationCode){
        $client = Client::where('ConfirmationCode', $ConfirmationCode)->where('Confirmed',0)->first();

        if($client){
            $client->Confirmed=1;
            $client->ConfirmationCode=null;
            $client->save();
            return redirect()->away('https://www.atriumprivatebanker.com');

        }else{
            return back()->withInput()->withErrors(['Code Confirmation Error']);
        }
    }

}
