<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;

use App\Jobs\NewBuyerEmail;
use App\Models\Client;
use App\Models\Coin;
use App\Models\Movement;
use App\Models\MethodPayment;
use Auth;
use Mail;
use DB;
use Log;
use App\Mail\buyMail;
use App\Notifications\clients;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class PaymentController extends Controller
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }
    
    public function index()
    {
        return view('client.paypal');
    }

    public function payWithpaypal(Request $req)
    {  
      

       if($req->coin=='' || !isset($req->coin) || !is_numeric($req->coin)){
           return json_encode(['state'=>500, 'msj'=>'Coin Not Found']);
       }

        if($req->price=='' || !isset($req->price) || !is_numeric($req->price)){
            return json_encode(['state'=>500, 'msj'=>'Coin Price Not Found']);
        }

        if($req->unity=='' || !isset($req->unity) || !is_numeric($req->unity)){
            return json_encode(['state'=>500, 'msj'=>'Unity of Coin Not Found']);
        }

       try{
          DB::beginTransaction();  
           $mov = new Movement();
           $mov->method_payment_id = $req->method;
           $mov->client_id = Auth::user()->id;
           $mov->coin_id = $req->coin;
           $mov->quantity = $req->unity;
           $actual_price = Coin::find($req->coin)->actual_price;
           if(is_null($actual_price)){
               return json_encode(['state'=>500, 'msj'=>'Not Found Price Actually Coin']);
           }
           $mov->price_actually = $actual_price;
           $mov->date_movement = Carbon::now();
           if($mov->method_payment_id == 1){
            $mov->state='P'; 
           }else{
            $mov->state='A';
           }
           
           $mov->save();

                 }catch (\Exception $e){
                    Log::info($e->getMessage());
                    DB::rollback();
                    return redirect()->route('client.dashboard')->withErrors('error unknow: 1013');
                
                }   
            
                if($mov->method_payment_id == 2){

                $payer = new Payer();
                $payer->setPaymentMethod('paypal');
                $item_1 = new Item();
                $item_1->setName('Moneda Atrium')->setCurrency('EUR')->setQuantity(1)->setPrice($req->get('price')); 
                $item_list = new ItemList();
                $item_list->setItems(array($item_1));
                $amount = new Amount();
                $amount->setCurrency('EUR')->setTotal($req->get('price'));
                $transaction = new Transaction();
                $transaction->setAmount($amount)->setItemList($item_list)->setDescription('Your transaction description');
                $redirect_urls = new RedirectUrls();
                $redirect_urls->setReturnUrl(Route('status'))->setCancelUrl(Route('status'));
                $payment = new Payment();
                $payment->setIntent('Sale')->setPayer($payer)->setRedirectUrls($redirect_urls)->setTransactions(array($transaction));
            
                try {
                    $payment->create($this->_api_context);
            
                } catch (\PayPal\Exception\PPConnectionException $ex) {
                    if (\Config::get('app.debug')) {
                        return redirect()->Route('client.dashboard')->withErrors('Connection timeout');
                    } else {
                        return redirect()->Route('client.dashboard')->withErrors('Some error occur, sorry for inconvenient');
                    }
                }
                foreach ($payment->getLinks() as $link) {
                    if ($link->getRel() == 'approval_url') {
                        $redirect_url = $link->getHref();
                        break;
                    }
                }
                /** add payment ID to session **/
                Session::put('paypal_payment_id', $payment->getId());
                if (isset($redirect_url)) {
                    /** redirect to paypal **/
                    return Redirect::away($redirect_url);
                }
                DB::rollback();
                return redirect()->route('client.dashboard')->withErrors('Unknown error occurred');

                 
            }elseif($mov->method_payment_id == 5){
                  $atm = Movement::select(\DB::raw('sum(quantity*price_actually) as price'),
        \DB::raw('sum(quantity) as quantity'), 'coin_id','movements.state as state')->
        where('client_id',Auth::user()->id)->where('state','A')->where('coin_id',11)->first();
          
                Log::info(doubleval($req->price) ."_". doubleval($atm->quantity));
                if(doubleval($req->price) > doubleval($atm->quantity) ){
                    return redirect()->route('client.dashboard')->withErrors(trans('mensaje.atm_insuficiente'));
                }

               $mov2 = new Movement();
               $mov2->method_payment_id = $req->method;
               $mov2->client_id = Auth::user()->id;
               $mov2->coin_id = 11;
               $mov2->quantity = ($req->quantity)*-1;
               $actual_price = Coin::find($req->coin)->actual_price;
               if(is_null($actual_price)){
                   return json_encode(['state'=>500, 'msj'=>'Not Found Price Actually Coin']);
               }
               $mov2->price_actually = $actual_price;
               $mov2->date_movement = Carbon::now();
               $mov2->state='A';
               try {
                  $mov2->save(); 
                  DB::commit();
                  NewBuyerEmail::dispatch($mov)->delay(now()->addSeconds(1));
                  
               } catch (Exception $e) {
                  Log::info($e->getMessage());
                  DB::rollback();
                  return redirect()->route('client.dashboard')->withErrors('Error Unknown: 1014');
               }
                
                return redirect()->route('client.dashboard')->withSuccess(trans('buy.result_atm', [ 'valor' => $mov2->valor ]));
            }else{
                DB::commit();
                NewBuyerEmail::dispatch($mov)->delay(now()->addSeconds(30));
                return redirect()->route('client.dashboard')->withSuccess(trans('buy.result_tranfer_bank',['valor'=>$mov->valor]));

            }
            
        
    }
    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return redirect()->Route('client.dashboard');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {
            return redirect()->Route('client.dashboard')->withSuccess(['Payment Success']);
        }
        \Session::put('error', 'Payment failed');
        return redirect()->Route('client.dashboard');
    }
}

