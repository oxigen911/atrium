<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
 
/**
 * WebCrawlerController
 * @author Crawler
 */
class WebCrawlerController extends Controller {
    
    /**
     * Search snapdeal products by given url
     * @param Request $request
     */
    public function searchProductsByUrl(Request $request) {
    	 $abcdario = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        $pageUrl = 'https://en.wikipedia.org/wiki/List_of_towns_and_cities_with_100,000_or_more_inhabitants/cityname:_';
        if(!$pageUrl) {
            return response()->json($this->getActionStatus("ERROR", "Product page url not found."));
        }
        $errors = array();
 	
 		foreach ($abcdario as $key => $value) {
 			$result = $this->makeWebRequest($pageUrl.''.$value, $errors);

 			if(empty($errors)) {
		        $response['content'][] = $this->parseQuickProductsJson($result);
		    }
 		}			

 		
 		echo "<pre>";
 		print_r(array_reduce($response, 'array_merge', [[]]));
 		
        exit(0);
        
        //return response()->json($response);
    }
    
    /**
     * Parse json string into array
     * @param string $result
     * @param string $category
     */
    private function parseQuickProductsJson($result) {
        $crawler = new Crawler($result);
            $filter = $crawler->filter('table');
            $table = $crawler->filter('table')->filter('tr')->each(function ($tr, $i) {
		    return $tr->filter('td')->each(function ($td, $i) {

		    		return trim($td->text());
		    	
		    	
		    });
		});
		
        return $table;
    }
    
    /**
     * Make web request to affiliate server url
     * @param String $url
     */
    private function makeWebRequest($url, &$errors) {
        $client = new Client();
        $response = $client->get($url);
        
        if($response->getStatusCode() == 200) {
            return (string)$response->getBody();
        } else {
            array_push($errors, $response->getReasonPhrase());
            return;
        }
    }
}