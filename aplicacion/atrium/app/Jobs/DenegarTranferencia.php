<?php

namespace App\Jobs;
use App\Models\Client;
use App\Models\Movement;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\buyMail;
use App\Mail\MailPaymentTranferBank;
use App\Mail\MailPaymentAdminTranferBank;
use App\Mail\MailPaymentATM;
use App\Mail\MailDenyTranferBank;
use Mail;
use Log;

class DenegarTranferencia implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $movement = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Movement $mov)
    {
        $this->movement = $mov;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {  
        Mail::to($this->movement->client->Email)->send(new MailDenyTranferBank($this->movement));
    }
}
