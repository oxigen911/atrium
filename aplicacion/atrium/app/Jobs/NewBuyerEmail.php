<?php

namespace App\Jobs;
use App\Models\Movement;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\buyMail;
use App\Mail\MailPaymentTranferBank;
use App\Mail\MailPaymentAdminTranferBank;
use App\Mail\MailPaymentATM;
use Mail;
use Log;

class NewBuyerEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $movement = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Movement $mov)
    {
        $this->movement = $mov;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   Log::info("mensajes NewBuyerEmail".$this->movement->method_payment_id);
       switch ($this->movement->method_payment_id) {
            case 1:
                # Tranfer Bank
                    Mail::to($this->movement->client->Email)->send(new MailPaymentTranferBank($this->movement));
                break;
            case 2:
                # Paypal
                    Mail::to($this->movement->client->Email)->send(new buyMail($this->movement));
                break;   
            case 3:
                # CreditCard
                    Mail::to($this->movement->client->Email)->send(new buyMail($this->movement));
                break;         
            case 4:
                # WireTranfer
                    Mail::to($this->movement->client->Email)->send(new buyMail($this->movement));
                break;    
            case 5:
                # ATM
                    Mail::to($this->movement->client->Email)->send(new MailPaymentATM($this->movement));
                break;        
            default:
                    Log::info("por favor verificar el movimiento".json_encode($this->movement));
                break;
        }
        Mail::to('juan.sierracastro@hotmail.com')->send(new MailPaymentAdminTranferBank($this->movement));
    }
}
