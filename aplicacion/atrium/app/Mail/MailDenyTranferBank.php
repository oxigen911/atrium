<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\Client;
use App\Models\Movement;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailDenyTranferBank extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $movement;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Movement $client)
    {
        $this->movement= $client;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.denyTransaction');
    }
}
