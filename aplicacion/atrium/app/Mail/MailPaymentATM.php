<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\Movement;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailPaymentATM extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $movement;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Movement $demo)
    {
        $this->movement = $demo;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->movement->quantity>0){
            return $this->view('mails.buyATM');
        }

        
    }
}
