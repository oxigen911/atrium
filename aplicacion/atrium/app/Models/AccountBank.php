<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountBank extends Model
{

    protected $table = "account_bank";

    protected $fillable =[
        'client_id',
        'EthereumWallet',
        'IBAN',
        'HeaderAccount',
        'BankAccount',
        'SwiftCode',
        'Prefered'];

}
