<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryCoins extends Model
{
    	protected $table = "category_coins";    

  	protected $fillable =[
  			  'name'
			  	];
}
