<?php

namespace App\Models;
use Illuminate\Auth\Notifications;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Event;
use Log;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client Extends Authenticatable implements MustVerifyEmail{
    use Notifiable;
  
  

  protected $fillable = ['UserName_NDG',    
                        'ManagerId_NDG',            
                        'Email',      
                        'Password',      
                        'SecondPassword',      
                        'FirstName',            
                        'LastName',         
                        'BirthDate',        
                        'CountryBirth',         
                        'DistrictBirth',            
                        'CityBirth',            
                        'CountryResidency',         
                        'Address',  
                        'CertificationResidency',           
                        'DocumentType',         
                        'DocumentIdentification',           
                        'DocumentIssueDate',        
                        'DocumentexpirationDate',       
                        'DocumentImage',
                        'Approved',
                        'ApprovedBy',
                        'Deny','DenyBy','DenyReason','email_verified_at','Confirmed','ConfirmationCode',
                        'remember_token','NumberCard'
                        ];
   protected $dates= ['Approved','BirthDate','DocumentIssueDate','DocumentexpirationDate'];

  public static function boot(){
      parent::boot();

      static::created(function($client){
          Event::fire('createdClient',$client);
          //$client->sendEmailVerificationNotification();
      });
  }

  public function AccountBanks(){
       return $this->hasMany('\App\Models\AccountBank','client_id','id');

  }

  public function ApprobedByYUser(){
       return $this->belongsTo('\App\Models\User','ApprovedBy','id');

  }

  public function Country(){
    return $this->belongsTo('\App\Models\Country','CountryBirth','id');
  }

  public function City(){
    return $this->belongsTo('\App\Models\City','CityBirth','id');
  }

  public function getFullNameAttribute()
  {
      return "{$this->FirstName} {$this->LastName}";
  }


  public function getFormatDateCreatedAttribute()
  {
      return "{$this->created_at->format('d.m.Y H:i')}";
  }

  public function getFormatDateCreAttribute()
  {
      return "{$this->created_at->format('d.m.Y')}";
  }

  public function generateToken()
  {
      $api_token = str_random(60); 
      $this->api_token = $api_token;
      $this->update();
      return $api_token;
  }
    
  public function routeNotificationForMail($notification)
    {
        return $this->Email;
    }

  public function Movements(){
        return $this->hasMany('\App\Models\Movement','client_id','id');
 
  }
    
}
