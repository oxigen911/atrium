<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Coin Extends Model{

  	protected $table = "coins";    

  	protected $fillable =[
  			  'name',
			  'initial',
			  'type_id',
			  'logo',
			  'project_value',
			  'coin_price',
                          'issued_coins',
			  'cash_back_fund',
			  'actual_price',
			  'data_start',
			  'data_end',
			  'white_paper',
			  'category_id',
			  'link'
			  	];

	protected $dates=['data_start','data_end'];


    protected $casts = [
        'data_start'  => 'date:Y-m-d',
        'data_end' => 'date:Y-m-d',
    ];

    public function Movements(){
       return $this->hasMany('\App\Models\Movement','coin_id','id');

	}

	public function MovementsActive(){
       return $this->Movements()->where('state','A');

	}

	public function getSoldAttribute(){
		$tot=0;
       foreach ($this->MovementsActive as $key => $value) {
       		$tot=$tot+($value->price_actually*$value->quantity);
       }

       return $tot;
       }


	public function getCashBackAttribute(){
           return $this->Sold*($this->cash_back_fund/100);	
       }

	public function getPprojectValAttribute(){
    
                 return $this->coin_price*$this->issued_coins;	
       }


	public function getCoinPersonAttribute(){
		$tot=0;
       foreach ($this->MovementsActive->groupBy('client_id') as $key => $value) {
       		$tot=$tot+1;
       }

       return $tot;

	}

	 public function Category(){
       return $this->belongsTo('\App\Models\CategoryCoins','category_id','id');

    }

    public function TypeCoin(){
       return $this->belongsTo('\App\Models\TypeCoin','type_id','id');

    }


}
