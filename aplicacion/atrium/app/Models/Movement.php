<?php

namespace App\Models;
use Event;
use Log;
use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{

    protected $table = "movements";
    protected $dates = ["date_approve","date_movement"];
    protected $fillable =[
        'client_id',
        'coin_id',
        'method_payment_id',
        'quantity',
        'price_actually',
        'date_approve',
        'date_deny',
        'date_movement',
        'state',
        'observation'
    ];

    /*public static function boot(){
        parent::boot();
        static::created(function($movement){
            Log::info("sendMail");
            Event::fire('createdMovement',$movement);
        });
    }*/
    public function coin(){
        return $this->belongsTo('\App\Models\Coin','coin_id','id');

    }

    public function methodPayment(){
        return $this->belongsTo('\App\Models\MethodPayment','method_payment_id','id');

    }

    public function client(){
        return $this->belongsTo('\App\Models\Client','client_id','id');

    }

    public function getStatedAttribute(){
       $txt="";
        switch ($this->state) {
            case 'P':
               $txt='Pending';  
                break;
            case 'A':
               $txt='Authorized';  
                break;    
            case 'I':
		$txt='Declined';
                break; 
            default:
                $txt="Revisar";
                break;
        }

        return $txt;
    }


    public function getValorAttribute(){
       
        
        return number_format($this->quantity*$this->price_actually,2);
    }




}
