<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

         $faker = Faker::create();
    	foreach (range(1,100) as $index) {
	        DB::table('clients')->insert([
'UserName_NDG'=>$faker->numberBetween(2019022001,2019099999),
'ManagerId_NDG'=>$faker->numberBetween(2019022001,2019099999),
'Email'=>$faker->email,
'FirstName'=>$faker->firstName,
'LastName'=>$faker->lastName, 
'BirthDate'=>$faker->date($format = 'Y-m-d', $max = 'now'),
'CountryBirth'=>$faker->numberBetween(1,50),
'DistrictBirth'=>$faker->numberBetween(1,200),
'CityBirth'=>$faker->numberBetween(1,100), 
'CountryResidency'=>$faker->numberBetween(1,200),
'Address'=>$faker->address, 
'CertificationResidency'=>$faker->image(null, 100, 200, 'cats', false),
'DocumentType'=>$faker->numberBetween(1,5), 
'DocumentIdentification'=>$faker->numberBetween(1000000,99999999),
'DocumentIssueDate'=>$faker->date($format = 'Y-m-d', $max = 'now'),
'DocumentexpirationDate'=>$faker->date($format = 'Y-m-d', $max = 'now'), 
'DucumentImage'=>$faker->image(null, 100, 200, 'cats', false)

	        ]);
		}	
    }
}
