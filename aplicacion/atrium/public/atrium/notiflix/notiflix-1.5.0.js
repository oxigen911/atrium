/*! Notiflix ('https://www.notiflix.com') -- Version: 1.5.0 -- Author: Furkan MT ('https://github.com/furcan') -- Copyright 2019 Notiflix, MIT Licence ('https://opensource.org/licenses/MIT') */
"use strict";
var newNotifySettings, newReportSettings, newConfirmSettings, newLoadingSettings, notifySettings = {
        wrapID: "NotiflixNotifyWrap",
        width: "280px",
        position: "right-top",
        distance: "10px",
        opacity: 1,
        borderRadius: "5px",
        rtl: !1,
        timeout: 3e3,
        messageMaxLength: 110,
        backOverlay: !1,
        backOverlayColor: "rgba(0,0,0,0.5)",
        ID: "NotiflixNotify",
        className: "notiflix-notify",
        zindex: 4001,
        useGoogleFont: !0,
        fontFamily: "Quicksand",
        fontSize: "13px",
        cssAnimation: !0,
        cssAnimationDuration: 400,
        cssAnimationStyle: "fade",
        closeButton: !1,
        useIcon: !0,
        useFontAwesome: !1,
        fontAwesomeIconStyle: "basic",
        fontAwesomeIconSize: "34px",
        plainText: !0,
        success: {
            background: "#00b462",
            textColor: "#fff",
            childClassName: "success",
            notiflixIconColor: "rgba(0, 0, 0, 0.25)",
            fontAwesomeClassName: "fas fa-check-circle",
            fontAwesomeIconColor: "rgba(0, 0, 0, 0.2)"
        },
        failure: {
            background: "#f44336",
            textColor: "#fff",
            childClassName: "failure",
            notiflixIconColor: "rgba(0, 0, 0, 0.2)",
            fontAwesomeClassName: "fas fa-times-circle",
            fontAwesomeIconColor: "rgba(0, 0, 0, 0.2)"
        },
        warning: {
            background: "#f2bd1d",
            textColor: "#fff",
            childClassName: "warning",
            notiflixIconColor: "rgba(0, 0, 0, 0.2)",
            fontAwesomeClassName: "fas fa-exclamation-circle",
            fontAwesomeIconColor: "rgba(0, 0, 0, 0.2)"
        },
        info: {
            background: "#00bcd4",
            textColor: "#fff",
            childClassName: "info",
            notiflixIconColor: "rgba(0, 0, 0, 0.2)",
            fontAwesomeClassName: "fas fa-info-circle",
            fontAwesomeIconColor: "rgba(0, 0, 0, 0.2)"
        }
    },
    reportSettings = {
        ID: "NotiflixReportWrap",
        className: "notiflix-report",
        width: "481px",
        backgroundColor: "#ffffff",
        borderRadius: "20px",
        rtl: !1,
        zindex: 4002,
        backOverlay: !0,
        backOverlayColor: "rgba(0, 0, 0, 0.5)",
        useGoogleFont: !0,
        fontFamily: "Quicksand",
        svgSize: "110px",
        plainText: !0,
        titleFontSize: "16px",
        titleMaxLength: 34,
        messageFontSize: "13px",
        messageMaxLength: 400,
        buttonFontSize: "14px",
        buttonMaxLength: 34,
        cssAnimation: !0,
        cssAnimationDuration: 360,
        cssAnimationStyle: "fade",
        success: {
            svgColor: "#00b462",
            titleColor: "#1e1e1e",
            messageColor: "#242424",
            buttonBackground: "#00b462",
            buttonColor: "#fff"
        },
        failure: {
            svgColor: "#f44336",
            titleColor: "#1e1e1e",
            messageColor: "#242424",
            buttonBackground: "#f44336",
            buttonColor: "#fff"
        },
        warning: {
            svgColor: "#f2bd1d",
            titleColor: "#1e1e1e",
            messageColor: "#242424",
            buttonBackground: "#f2bd1d",
            buttonColor: "#fff"
        },
        info: {
            svgColor: "#00bcd4",
            titleColor: "#1e1e1e",
            messageColor: "#242424",
            buttonBackground: "#00bcd4",
            buttonColor: "#fff"
        }
    },
    confirmSettings = {
        ID: "NotiflixConfirmWrap",
        className: "notiflix-confirm",
        width: "280px",
        zindex: 4003,
        position: "center",
        distance: "10px",
        backgroundColor: "#f8f8f8",
        borderRadius: "25px",
        backOverlay: !0,
        backOverlayColor: "rgba(0,0,0,0.5)",
        rtl: !1,
        useGoogleFont: !0,
        fontFamily: "Quicksand",
        cssAnimation: !0,
        cssAnimationStyle: "fade",
        cssAnimationDuration: 300,
        titleColor: "#00b462",
        titleFontSize: "16px",
        titleMaxLength: 34,
        messageColor: "#1e1e1e",
        messageFontSize: "14px",
        messageMaxLength: 110,
        buttonsFontSize: "15px",
        buttonsMaxLength: 34,
        okButtonColor: "#f8f8f8",
        okButtonBackground: "#00b462",
        cancelButtonColor: "#f8f8f8",
        cancelButtonBackground: "#a9a9a9",
        plainText: !0
    },
    loadingSettings = {
        ID: "NotiflixLoadingWrap",
        className: "notiflix-loading",
        zindex: 4e3,
        backgroundColor: "rgba(0,0,0,0.8)",
        rtl: !1,
        useGoogleFont: !0,
        fontFamily: "Quicksand",
        cssAnimation: !0,
        cssAnimationDuration: 400,
        clickToClose: !1,
        customSvgUrl: null,
        svgSize: "80px",
        svgColor: "#00b462",
        messageID: "NotiflixLoadingMessage",
        messageFontSize: "15px",
        messageMaxLength: 34,
        messageColor: "#dcdcdc"
    },
    extendNotiflix = function() {
        var n = {},
            i = !1,
            t = 0;
        "[object Boolean]" === Object.prototype.toString.call(arguments[0]) && (i = arguments[0], t++);
        for (var e = function(t) {
                for (var e in t) t.hasOwnProperty(e) && (i && "[object Object]" === Object.prototype.toString.call(t[e]) ? n[e] = extendNotiflix(n[e], t[e]) : n[e] = t[e])
            }; t < arguments.length; t++) e(arguments[t]);
        return n
    };

function notiflixPlaintext(t) {
    var e = document.createElement("div");
    return e.innerHTML = t, e.textContent || e.innerText || ""
}

function notiflixGoogleFont() {
    if (!document.getElementById("NotiflixQuicksand")) {
        var t = document.createElement("link");
        t.id = "NotiflixGoogleDNS", t.rel = "dns-prefetch", t.href = "//fonts.googleapis.com", document.head.appendChild(t);
        var e = document.createElement("link");
        e.id = "NotiflixQuicksand", e.rel = "stylesheet", e.href = "https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&amp;subset=latin-ext", document.head.appendChild(e)
    }
}
var Notiflix = {
        Notify: {
            Init: function(t) {
                (newNotifySettings = extendNotiflix(!0, notifySettings, t)).useGoogleFont && "Quicksand" === newNotifySettings.fontFamily && notiflixGoogleFont()
            },
            Merge: function(t) {
                newNotifySettings = extendNotiflix(!0, newNotifySettings || notifySettings, t)
            },
            Success: function(t, e) {
                e || (e = void 0), NotiflixNotify(t, e, newNotifySettings.success, "Success")
            },
            Failure: function(t, e) {
                e || (e = void 0), NotiflixNotify(t, e, newNotifySettings.failure, "Failure")
            },
            Warning: function(t, e) {
                e || (e = void 0), NotiflixNotify(t, e, newNotifySettings.warning, "Warning")
            },
            Info: function(t, e) {
                e || (e = void 0), NotiflixNotify(t, e, newNotifySettings.info, "Info")
            }
        },
        Report: {
            Init: function(t) {
                (newReportSettings = extendNotiflix(!0, reportSettings, t)).useGoogleFont && "Quicksand" === newReportSettings.fontFamily && notiflixGoogleFont()
            },
            Merge: function(t) {
                newReportSettings = extendNotiflix(!0, newReportSettings || reportSettings, t)
            },
            Success: function(t, e, n, i) {
                4 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : ((void 0 === t || t.length <= 0) && (t = "Notiflix Success"), (void 0 === e || e.length <= 0) && (e = '"Do not try to become a person of success but try to become a person of value." <br><br>- Albert Einstein'), (void 0 === n || n.length <= 0) && (n = "Okay"), void 0 === i && (i = void 0), NotiflixReport(t, e, n, i, newReportSettings.success, "success"))
            },
            Failure: function(t, e, n, i) {
                4 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : ((void 0 === t || t.length <= 0) && (t = "Notiflix Failure"), (void 0 === e || e.length <= 0) && (e = '"Failure is simply the opportunity to begin again, this time more intelligently." <br><br>- Henry Ford'), (void 0 === n || n.length <= 0) && (n = "Okay"), void 0 === i && (i = void 0), NotiflixReport(t, e, n, i, newReportSettings.failure, "failure"))
            },
            Warning: function(t, e, n, i) {
                4 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : ((void 0 === t || t.length <= 0) && (t = "Notiflix Warning"), (void 0 === e || e.length <= 0) && (e = '"The peoples who want to live comfortably without producing and fatigue; they are doomed to lose their dignity, then liberty, and then independence and destiny." <br><br>- Mustafa Kemal Ataturk'), (void 0 === n || n.length <= 0) && (n = "Okay"), void 0 === i && (i = void 0), NotiflixReport(t, e, n, i, newReportSettings.warning, "warning"))
            },
            Info: function(t, e, n, i) {
                4 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : ((void 0 === t || t.length <= 0) && (t = "Notiflix Info"), (void 0 === e || e.length <= 0) && (e = '"Knowledge rests not upon truth alone, but upon error also." <br><br>- Carl Gustav Jung'), (void 0 === n || n.length <= 0) && (n = "Okay"), void 0 === i && (i = void 0), NotiflixReport(t, e, n, i, newReportSettings.info, "info"))
            }
        },
        Confirm: {
            Init: function(t) {
                (newConfirmSettings = extendNotiflix(!0, confirmSettings, t)).useGoogleFont && "Quicksand" === newConfirmSettings.fontFamily && notiflixGoogleFont()
            },
            Merge: function(t) {
                newConfirmSettings = extendNotiflix(!0, newConfirmSettings || confirmSettings, t)
            },
            Show: function(t, e, n, i, a) {
                5 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : ((void 0 === t || t.length <= 0) && (t = "Notiflix Confirm"), (void 0 === e || e.length <= 0) && (e = "Do you agree with me?"), (void 0 === n || n.length <= 0) && (n = "Yes"), (void 0 === i || i.length <= 0) && (i = "No"), void 0 === a && (a = void 0), NotiflixConfirm(t, e, n, i, a))
            }
        },
        Loading: {
            Init: function(t) {
                (newLoadingSettings = extendNotiflix(!0, loadingSettings, t)).useGoogleFont && "Quicksand" === newLoadingSettings.fontFamily && notiflixGoogleFont()
            },
            Merge: function(t) {
                newLoadingSettings = extendNotiflix(!0, newLoadingSettings || loadingSettings, t)
            },
            Standard: function(t) {
                1 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : (t || (t = ""), NotiflixLoading(t, "standard", !0, 0))
            },
            Hourglass: function(t) {
                1 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : (t || (t = ""), NotiflixLoading(t, "hourglass", !0, 0))
            },
            Circle: function(t) {
                1 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : (t || (t = ""), NotiflixLoading(t, "circle", !0, 0))
            },
            Arrows: function(t) {
                1 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : (t || (t = ""), NotiflixLoading(t, "arrows", !0, 0))
            },
            Dots: function(t) {
                1 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : (t || (t = ""), NotiflixLoading(t, "dots", !0, 0))
            },
            Pulse: function(t) {
                1 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : (t || (t = ""), NotiflixLoading(t, "pulse", !0, 0))
            },
            Custom: function(t) {
                1 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : (t || (t = ""), NotiflixLoading(t, "custom", !0, 0))
            },
            Notiflix: function(t) {
                1 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : (t || (t = ""), NotiflixLoading(t, "notiflix", !0, 0))
            },
            Remove: function(t) {
                1 < arguments.length ? console.error("Notiflix Error: Parameters not allowed.") : (t || (t = 0), NotiflixLoading(!1, !1, !1, t))
            },
            Change: function(t) {
                1 < arguments.length ? console.error("Notiflix Error: More parameters than allowed.") : (t || (t = ""), NotiflixLoadingChange(t))
            }
        }
    },
    notiflixNotifyCount = 0;

function NotiflixNotify(t, e, n, i) {
    if (null != arguments && 4 === arguments.length) {
        t || (t = "Notiflix " + i), newNotifySettings.plainText && (t = notiflixPlaintext(t)), !newNotifySettings.plainText && t.length > newNotifySettings.messageMaxLength && (Notiflix.Notify.Merge({
            closeButton: !0
        }), t = '<b>HTML Tags Error:</b> Your content length is more than "messageMaxLength" option.'), t.length > newNotifySettings.messageMaxLength && (t = t.substring(0, newNotifySettings.messageMaxLength) + "..."), "shadow" === newNotifySettings.fontAwesomeIconStyle && (n.fontAwesomeIconColor = n.background), notiflixNotifyCount++, newNotifySettings.cssAnimation || (newNotifySettings.cssAnimationDuration = 0);
        var a, o = document.getElementsByTagName("body")[0],
            s = document.createElement("div");
        s.id = notifySettings.wrapID, s.style.width = newNotifySettings.width, s.style.zIndex = newNotifySettings.zindex, s.style.opacity = newNotifySettings.opacity, "right-bottom" === newNotifySettings.position ? (s.style.right = newNotifySettings.distance, s.style.bottom = newNotifySettings.distance, s.style.top = "auto", s.style.left = "auto") : "left-top" === newNotifySettings.position ? (s.style.left = newNotifySettings.distance, s.style.top = newNotifySettings.distance, s.style.right = "auto", s.style.bottom = "auto") : "left-bottom" === newNotifySettings.position ? (s.style.left = newNotifySettings.distance, s.style.bottom = newNotifySettings.distance, s.style.top = "auto", s.style.right = "auto") : (s.style.right = newNotifySettings.distance, s.style.top = newNotifySettings.distance, s.style.left = "auto", s.style.bottom = "auto"), newNotifySettings.backOverlay && ((a = document.createElement("div")).id = newNotifySettings.ID + "Overlay", a.style.width = "100%", a.style.height = "100%", a.style.position = "fixed", a.style.zIndex = newNotifySettings.zindex, a.style.left = 0, a.style.top = 0, a.style.right = 0, a.style.bottom = 0, a.style.background = newNotifySettings.backOverlayColor, a.className = newNotifySettings.cssAnimation ? "with-animation" : "", a.style.animationDuration = newNotifySettings.cssAnimation ? newNotifySettings.cssAnimationDuration + "ms" : "", document.getElementById(a.id) || o.appendChild(a)), document.getElementById(s.id) || o.appendChild(s);
        var r = document.createElement("div");
        r.id = newNotifySettings.ID + "-" + notiflixNotifyCount, r.className = newNotifySettings.className + " " + n.childClassName + " " + (newNotifySettings.cssAnimation ? "with-animation" : "") + " " + (newNotifySettings.useIcon ? "with-icon" : "") + " nx-" + newNotifySettings.cssAnimationStyle + " " + (newNotifySettings.closeButton && !e ? "with-close" : "") + " " + (e ? "with-callback" : ""), r.style.fontSize = newNotifySettings.fontSize, r.style.color = n.textColor, r.style.background = n.background, r.style.borderRadius = newNotifySettings.borderRadius, newNotifySettings.rtl && (r.setAttribute("dir", "rtl"), r.classList.add("rtl-on")), r.style.fontFamily = '"' + newNotifySettings.fontFamily + '", sans-serif', newNotifySettings.cssAnimation && (r.style.animationDuration = newNotifySettings.cssAnimationDuration + "ms");
        var l, c = "";
        if (newNotifySettings.closeButton && !e && (c = '<span class="click-to-close"><svg class="clck2cls" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="20px" height="20px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"viewBox="0 0 20 20"xmlns:xlink="http://www.w3.org/1999/xlink"><defs><style type="text/css">.click2close{fill:' + n.notiflixIconColor + '}</style></defs><g><path class="click2close" d="M0.38 2.19l7.8 7.81 -7.8 7.81c-0.51,0.5 -0.51,1.31 -0.01,1.81 0.25,0.25 0.57,0.38 0.91,0.38 0.34,0 0.67,-0.14 0.91,-0.38l7.81 -7.81 7.81 7.81c0.24,0.24 0.57,0.38 0.91,0.38 0.34,0 0.66,-0.14 0.9,-0.38 0.51,-0.5 0.51,-1.31 0,-1.81l-7.81 -7.81 7.81 -7.81c0.51,-0.5 0.51,-1.31 0,-1.82 -0.5,-0.5 -1.31,-0.5 -1.81,0l-7.81 7.81 -7.81 -7.81c-0.5,-0.5 -1.31,-0.5 -1.81,0 -0.51,0.51 -0.51,1.32 0,1.82z"/></g></svg></span>'), newNotifySettings.useIcon)
            if (newNotifySettings.useFontAwesome) r.innerHTML = '<i style="color:' + n.fontAwesomeIconColor + "; font-size:" + newNotifySettings.fontAwesomeIconSize + ';" class="nmi wfa ' + n.fontAwesomeClassName + " " + ("shadow" === newNotifySettings.fontAwesomeIconStyle ? "shadow" : "basic") + '"></i><span class="the-message with-icon">' + t + "</span>" + (newNotifySettings.closeButton ? c : "");
            else l = "Success" === i ? '<svg class="nmi" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="40px" height="40px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"viewBox="0 0 40 40"xmlns:xlink="http://www.w3.org/1999/xlink"><defs><style type="text/css">#Notiflix-Icon-Success{fill:' + n.notiflixIconColor + '}</style></defs><g><path id="Notiflix-Icon-Success" class="fil0" d="M20 0c11.03,0 20,8.97 20,20 0,11.03 -8.97,20 -20,20 -11.03,0 -20,-8.97 -20,-20 0,-11.03 8.97,-20 20,-20zm0 37.98c9.92,0 17.98,-8.06 17.98,-17.98 0,-9.92 -8.06,-17.98 -17.98,-17.98 -9.92,0 -17.98,8.06 -17.98,17.98 0,9.92 8.06,17.98 17.98,17.98zm-2.4 -13.29l11.52 -12.96c0.37,-0.41 1.01,-0.45 1.42,-0.08 0.42,0.37 0.46,1 0.09,1.42l-12.16 13.67c-0.19,0.22 -0.46,0.34 -0.75,0.34 -0.23,0 -0.45,-0.07 -0.63,-0.22l-7.6 -6.07c-0.43,-0.35 -0.5,-0.99 -0.16,-1.42 0.35,-0.43 0.99,-0.5 1.42,-0.16l6.85 5.48z"/></g></svg>' : "Failure" === i ? '<svg class="nmi" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="40px" height="40px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"viewBox="0 0 40 40"xmlns:xlink="http://www.w3.org/1999/xlink"><defs><style type="text/css">#Notiflix-Icon-Failure{fill:' + n.notiflixIconColor + '}</style></defs><g><path id="Notiflix-Icon-Failure" class="fil0" d="M20 0c11.03,0 20,8.97 20,20 0,11.03 -8.97,20 -20,20 -11.03,0 -20,-8.97 -20,-20 0,-11.03 8.97,-20 20,-20zm0 37.98c9.92,0 17.98,-8.06 17.98,-17.98 0,-9.92 -8.06,-17.98 -17.98,-17.98 -9.92,0 -17.98,8.06 -17.98,17.98 0,9.92 8.06,17.98 17.98,17.98zm1.42 -17.98l6.13 6.12c0.39,0.4 0.39,1.04 0,1.43 -0.19,0.19 -0.45,0.29 -0.71,0.29 -0.27,0 -0.53,-0.1 -0.72,-0.29l-6.12 -6.13 -6.13 6.13c-0.19,0.19 -0.44,0.29 -0.71,0.29 -0.27,0 -0.52,-0.1 -0.71,-0.29 -0.39,-0.39 -0.39,-1.03 0,-1.43l6.13 -6.12 -6.13 -6.13c-0.39,-0.39 -0.39,-1.03 0,-1.42 0.39,-0.39 1.03,-0.39 1.42,0l6.13 6.12 6.12 -6.12c0.4,-0.39 1.04,-0.39 1.43,0 0.39,0.39 0.39,1.03 0,1.42l-6.13 6.13z"/></g></svg>' : "Warning" === i ? '<svg class="nmi" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="40px" height="40px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"viewBox="0 0 40 40"xmlns:xlink="http://www.w3.org/1999/xlink"><defs><style type="text/css">#Notiflix-Icon-Warning{fill:' + n.notiflixIconColor + '}</style></defs><g><path id="Notiflix-Icon-Warning" class="fil0" d="M21.91 3.48l17.8 30.89c0.84,1.46 -0.23,3.25 -1.91,3.25l-35.6 0c-1.68,0 -2.75,-1.79 -1.91,-3.25l17.8 -30.89c0.85,-1.47 2.97,-1.47 3.82,0zm16.15 31.84l-17.8 -30.89c-0.11,-0.2 -0.41,-0.2 -0.52,0l-17.8 30.89c-0.12,0.2 0.05,0.4 0.26,0.4l35.6 0c0.21,0 0.38,-0.2 0.26,-0.4zm-19.01 -4.12l0 -1.05c0,-0.53 0.42,-0.95 0.95,-0.95 0.53,0 0.95,0.42 0.95,0.95l0 1.05c0,0.53 -0.42,0.95 -0.95,0.95 -0.53,0 -0.95,-0.42 -0.95,-0.95zm0 -4.66l0 -13.39c0,-0.52 0.42,-0.95 0.95,-0.95 0.53,0 0.95,0.43 0.95,0.95l0 13.39c0,0.53 -0.42,0.96 -0.95,0.96 -0.53,0 -0.95,-0.43 -0.95,-0.96z"/></g></svg>' : "Info" === i ? '<svg class="nmi" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="40px" height="40px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"viewBox="0 0 40 40"xmlns:xlink="http://www.w3.org/1999/xlink"><defs><style type="text/css">#Notiflix-Icon-Info{fill:' + n.notiflixIconColor + '}</style></defs><g><path id="Notiflix-Icon-Info" class="fil0" d="M20 0c11.03,0 20,8.97 20,20 0,11.03 -8.97,20 -20,20 -11.03,0 -20,-8.97 -20,-20 0,-11.03 8.97,-20 20,-20zm0 37.98c9.92,0 17.98,-8.06 17.98,-17.98 0,-9.92 -8.06,-17.98 -17.98,-17.98 -9.92,0 -17.98,8.06 -17.98,17.98 0,9.92 8.06,17.98 17.98,17.98zm-0.99 -23.3c0,-0.54 0.44,-0.98 0.99,-0.98 0.55,0 0.99,0.44 0.99,0.98l0 15.86c0,0.55 -0.44,0.99 -0.99,0.99 -0.55,0 -0.99,-0.44 -0.99,-0.99l0 -15.86zm0 -5.22c0,-0.55 0.44,-0.99 0.99,-0.99 0.55,0 0.99,0.44 0.99,0.99l0 1.09c0,0.54 -0.44,0.99 -0.99,0.99 -0.55,0 -0.99,-0.45 -0.99,-0.99l0 -1.09z"/></g></svg>' : "", r.innerHTML = l + '<span class="the-message with-icon">' + t + "</span>" + (newNotifySettings.closeButton ? c : "");
        else r.innerHTML = '<span class="the-message">' + t + "</span>" + (newNotifySettings.closeButton ? c : "");
        if ("left-bottom" === newNotifySettings.position || "right-bottom" === newNotifySettings.position) {
            var m = document.getElementById(s.id);
            m.insertBefore(r, m.firstChild)
        } else document.getElementById(s.id).appendChild(r);
        if (newNotifySettings.useIcon) {
            var p = document.getElementById(r.id).querySelectorAll(".nmi")[0],
                g = 40;
            if (newNotifySettings.useFontAwesome) g = Math.round(parseInt(p.offsetHeight));
            else {
                var f = p.getBBox();
                g = Math.round(parseInt(f.width))
            }
            var x = document.getElementById(r.id).querySelectorAll("span")[0],
                d = Math.round(x.offsetHeight);
            d <= g && (x.style.paddingTop = (g - d) / 2 + "px", x.style.paddingBottom = (g - d) / 2 + "px")
        }
        if (document.getElementById(r.id)) {
            var u, w, y, b = document.getElementById(r.id),
                h = document.getElementById(s.id);
            if (newNotifySettings.backOverlay && (u = document.getElementById(a.id)), newNotifySettings.closeButton || e || (w = setTimeout(function() {
                    b.classList.add("remove"), newNotifySettings.backOverlay && h.childElementCount <= 0 && u.classList.add("remove")
                }, newNotifySettings.timeout), y = setTimeout(function() {
                    b.parentNode.removeChild(b), h.childElementCount <= 0 && (h.parentNode.removeChild(h), newNotifySettings.backOverlay && u.parentNode.removeChild(u))
                }, newNotifySettings.timeout + newNotifySettings.cssAnimationDuration)), newNotifySettings.closeButton && !e) document.getElementById(r.id).querySelectorAll("span.click-to-close")[0].addEventListener("click", function() {
                b.classList.add("remove"), clearTimeout(w), newNotifySettings.backOverlay && h.childElementCount <= 1 && u.classList.add("remove"), setTimeout(function() {
                    b.parentNode.removeChild(b), clearTimeout(y), h.childElementCount <= 0 && (h.parentNode.removeChild(h), newNotifySettings.backOverlay && u.parentNode.removeChild(u))
                }, newNotifySettings.cssAnimationDuration)
            });
            e && b.addEventListener("click", function(t) {
                e(), b.classList.add("remove"), newNotifySettings.backOverlay && h.childElementCount <= 0 && u.classList.add("remove"), clearTimeout(w), setTimeout(function() {
                    b.parentNode.removeChild(b), h.childElementCount <= 0 && (h.parentNode.removeChild(h), newNotifySettings.backOverlay && u.parentNode.removeChild(u)), clearTimeout(y)
                }, newNotifySettings.cssAnimationDuration)
            })
        }
    } else console.error("Notiflix Error: Where is the arguments?")
}

function NotiflixReport(t, e, n, i, a, o) {
    newReportSettings.plainText && (t = notiflixPlaintext(t), e = notiflixPlaintext(e), n = notiflixPlaintext(n)), newReportSettings.plainText || (t.length > newReportSettings.titleMaxLength && (t = "HTML Tags Error", e = 'Your Title content length is more than "titleMaxLength" option.', n = "Okay"), e.length > newReportSettings.messageMaxLength && (t = "HTML Tags Error", e = 'Your Message content length is more than "messageMaxLength" option.', n = "Okay"), n.length > newReportSettings.buttonMaxLength && (t = "HTML Tags Error", e = 'Your Button content length is more than "buttonMaxLength" option.', n = "Okay")), t.length > newReportSettings.titleMaxLength && (t = t.substring(0, newReportSettings.titleMaxLength) + "..."), e.length > newReportSettings.messageMaxLength && (e = e.substring(0, newReportSettings.messageMaxLength) + "..."), n.length > newReportSettings.buttonMaxLength && (n = n.substring(0, newReportSettings.buttonMaxLength) + "..."), newReportSettings.cssAnimation || (newReportSettings.cssAnimationDuration = 0);
    var s = document.getElementsByTagName("body")[0],
        r = document.createElement("div");
    r.id = reportSettings.ID, r.className = newReportSettings.className, r.style.width = newReportSettings.width, r.style.zIndex = newReportSettings.zindex, r.style.borderRadius = newReportSettings.borderRadius, r.style.fontFamily = '"' + newReportSettings.fontFamily + '", sans-serif', newReportSettings.rtl && (r.setAttribute("dir", "rtl"), r.classList.add("rtl-on"));
    var l = "";
    newReportSettings.backOverlay && (l = '<div class="' + newReportSettings.className + "-overlay" + (newReportSettings.cssAnimation ? " with-animation" : "") + '" style="background:' + newReportSettings.backOverlayColor + ";animation-duration:" + newReportSettings.cssAnimationDuration + 'ms;"></div>');
    var c = "";
    if ("success" === o ? c = notiflixReportSvgSuccess(newReportSettings.svgSize, a.svgColor) : "failure" === o ? c = notiflixReportSvgFailure(newReportSettings.svgSize, a.svgColor) : "warning" === o ? c = notiflixReportSvgWarning(newReportSettings.svgSize, a.svgColor) : "info" === o && (c = notiflixReportSvgInfo(newReportSettings.svgSize, a.svgColor)), r.innerHTML = l + '<div class="' + newReportSettings.className + "-content" + (newReportSettings.cssAnimation ? " with-animation " : "") + " nx-" + newReportSettings.cssAnimationStyle + '" style="background:' + newReportSettings.backgroundColor + "; animation-duration:" + newReportSettings.cssAnimationDuration + 'ms;border-top: 5px solid #f61816;"><div style="width:' + newReportSettings.svgSize + "; height:" + newReportSettings.svgSize + ';" class="' + newReportSettings.className + '-icon">' + c + '</div><h5 class="' + newReportSettings.className + '-title" style="font-weight:500; font-size:' + newReportSettings.titleFontSize + "; color:" + a.titleColor + ';">' + t + '</h5><p class="' + newReportSettings.className + '-message" style="font-size:' + newReportSettings.messageFontSize + "; color:" + a.messageColor + ';">' + e + '</p><a id="NXReportButton" class="' + newReportSettings.className + '-button" style="font-weight:500; font-size:' + newReportSettings.buttonFontSize + "; background:" + a.buttonBackground + "; color:" + a.buttonColor + ';">' + n + "</a></div>", !document.getElementById(r.id)) {
        s.appendChild(r);
        var m = Math.round(window.innerHeight),
            p = Math.round(document.getElementById(r.id).offsetHeight);
        r.style.top = (m - p) / 2 + "px";
        var g = document.getElementById(r.id);
        document.getElementById("NXReportButton").addEventListener("click", function() {
            i && i(), g.classList.add("remove"), setTimeout(function() {
                g.parentNode.removeChild(g)
            }, newReportSettings.cssAnimationDuration)
        })
    }
}

function NotiflixConfirm(t, e, n, i, a) {
    newConfirmSettings.plainText && (t = notiflixPlaintext(t), e = notiflixPlaintext(e), n = notiflixPlaintext(n), i = notiflixPlaintext(i)), newConfirmSettings.plainText || (t.length > newConfirmSettings.titleMaxLength && (t = "HTML Tags Error", e = 'Your Title content length is more than "titleMaxLength" option.', n = "Okay", i = "..."), e.length > newConfirmSettings.messageMaxLength && (t = "HTML Tags Error", e = 'Your Message content length is more than "messageMaxLength" option.', n = "Okay", i = "..."), (n.length || i.length) > newConfirmSettings.buttonsMaxLength && (t = "HTML Tags Error", e = 'Your Buttons contents length is more than "buttonsMaxLength" option.', n = "Okay", i = "...")), t.length > newConfirmSettings.titleMaxLength && (t = t.substring(0, newConfirmSettings.titleMaxLength) + "..."), e.length > newConfirmSettings.messageMaxLength && (e = e.substring(0, newConfirmSettings.messageMaxLength) + "..."), n.length > newConfirmSettings.buttonsMaxLength && (n = n.substring(0, newConfirmSettings.buttonsMaxLength) + "..."), i.length > newConfirmSettings.buttonsMaxLength && (i = i.substring(0, newConfirmSettings.buttonsMaxLength) + "..."), newConfirmSettings.cssAnimation || (newConfirmSettings.cssAnimationDuration = 0);
    var o = document.getElementsByTagName("body")[0],
        s = document.createElement("div");
    s.id = confirmSettings.ID, s.className = newConfirmSettings.className + (newConfirmSettings.cssAnimation ? " with-animation nx-" + newConfirmSettings.cssAnimationStyle : ""), s.style.width = newConfirmSettings.width, s.style.zIndex = newConfirmSettings.zindex, newConfirmSettings.rtl && (s.setAttribute("dir", "rtl"), s.classList.add("rtl-on")), s.style.fontFamily = '"' + newConfirmSettings.fontFamily + '", sans-serif';
    var r = "";
    newConfirmSettings.backOverlay && (r = '<div class="' + newConfirmSettings.className + "-overlay" + (newConfirmSettings.cssAnimation ? " with-animation" : "") + '" style="background:' + newConfirmSettings.backOverlayColor + ";animation-duration:" + newConfirmSettings.cssAnimationDuration + 'ms;"></div>');
    var l = "";
    if (a && (l = '<a id="NXConfirmButtonCancel" class="confirm-button-cancel" style="color:' + newConfirmSettings.cancelButtonColor + ";background:" + newConfirmSettings.cancelButtonBackground + ";font-size:" + newConfirmSettings.buttonsFontSize + ';">' + i + "</a>"), s.innerHTML = r + '<div class="' + newConfirmSettings.className + '-content" style="background:' + newConfirmSettings.backgroundColor + "; animation-duration:" + newConfirmSettings.cssAnimationDuration + "ms; border-radius: " + newConfirmSettings.borderRadius + ';"><div class="' + newConfirmSettings.className + '-head"><h5 style="color:' + newConfirmSettings.titleColor + ";font-size:" + newConfirmSettings.titleFontSize + ';">' + t + '</h5><p style="color:' + newConfirmSettings.messageColor + ";font-size:" + newConfirmSettings.messageFontSize + ';">' + e + '</p></div><div class="' + newConfirmSettings.className + '-buttons"><a id="NXConfirmButtonOk" class="confirm-button-ok' + (a ? "" : " full") + '" style="color:' + newConfirmSettings.okButtonColor + ";background:" + newConfirmSettings.okButtonBackground + ";font-size:" + newConfirmSettings.buttonsFontSize + ';">' + n + "</a>" + l + "</div></div>", !document.getElementById(s.id)) {
        if (o.appendChild(s), "center" === newConfirmSettings.position) {
            var c = Math.round(window.innerHeight),
                m = Math.round(document.getElementById(s.id).offsetHeight);
            s.style.top = (c - m) / 2 + "px", s.style.left = newConfirmSettings.distance, s.style.right = newConfirmSettings.distance, s.style.bottom = "auto", s.style.margin = "auto"
        } else "right-top" === newConfirmSettings.position ? (s.style.right = newConfirmSettings.distance, s.style.top = newConfirmSettings.distance, s.style.bottom = "auto", s.style.left = "auto") : "right-bottom" === newConfirmSettings.position ? (s.style.right = newConfirmSettings.distance, s.style.bottom = newConfirmSettings.distance, s.style.top = "auto", s.style.left = "auto") : "left-top" === newConfirmSettings.position ? (s.style.left = newConfirmSettings.distance, s.style.top = newConfirmSettings.distance, s.style.right = "auto", s.style.bottom = "auto") : "left-bottom" === newConfirmSettings.position ? (s.style.left = newConfirmSettings.distance, s.style.bottom = newConfirmSettings.distance, s.style.top = "auto", s.style.right = "auto") : (s.style.top = newConfirmSettings.distance, s.style.left = 0, s.style.right = 0, s.style.bottom = "auto"), s.style.margin = "auto";
        var p = document.getElementById(s.id),
            g = document.getElementById("NXConfirmButtonOk");
        if (a) g.addEventListener("click", function() {
            a(), p.classList.add("remove"), setTimeout(function() {
                p.parentNode.removeChild(p)
            }, newConfirmSettings.cssAnimationDuration)
        }), document.getElementById("NXConfirmButtonCancel").addEventListener("click", function() {
            p.classList.add("remove"), setTimeout(function() {
                p.parentNode.removeChild(p)
            }, newConfirmSettings.cssAnimationDuration)
        });
        else g.addEventListener("click", function() {
            p.classList.add("remove"), setTimeout(function() {
                p.parentNode.removeChild(p)
            }, newConfirmSettings.cssAnimationDuration)
        })
    }
}

function NotiflixLoading(t, e, n, i) {
    if (n) {
        t = t.toString().length > newLoadingSettings.messageMaxLength ? notiflixPlaintext(t).toString().substring(0, newLoadingSettings.messageMaxLength) + "..." : notiflixPlaintext(t).toString();
        var a = parseInt(newLoadingSettings.svgSize.slice(0, -2)),
            o = "";
        if (0 < t.length) {
            var s = Math.round(a - a / 3).toString() + "px",
                r = (1.2 * parseInt(newLoadingSettings.messageFontSize.slice(0, 2))).toString() + "px";
            o = '<p id="' + newLoadingSettings.messageID + '" class="loading-message" style="color:' + newLoadingSettings.messageColor + ";font-size:" + newLoadingSettings.messageFontSize + ";height:" + r + "; top:" + s + ';">' + t + "</p>"
        }
        newLoadingSettings.cssAnimation || (newLoadingSettings.cssAnimationDuration = 0);
        var l = "";
        if ("standard" === e) l = notiflixLoadingSvgStandard(newLoadingSettings.svgSize, newLoadingSettings.svgColor);
        else if ("hourglass" === e) l = notiflixLoadingSvgHourglass(newLoadingSettings.svgSize, newLoadingSettings.svgColor);
        else if ("circle" === e) l = notiflixLoadingSvgCircle(newLoadingSettings.svgSize, newLoadingSettings.svgColor);
        else if ("arrows" === e) l = notiflixLoadingSvgArrows(newLoadingSettings.svgSize, newLoadingSettings.svgColor);
        else if ("dots" === e) l = notiflixLoadingSvgDots(newLoadingSettings.svgSize, newLoadingSettings.svgColor);
        else if ("pulse" === e) l = notiflixLoadingSvgPulse(newLoadingSettings.svgSize, newLoadingSettings.svgColor);
        else if ("custom" === e && null !== newLoadingSettings.customSvgUrl) l = '<img class="custom-loading-icon" width="' + newLoadingSettings.svgSize + '" height="' + newLoadingSettings.svgSize + '" src="' + newLoadingSettings.customSvgUrl + '" alt="Notiflix">';
        else {
            if ("custom" === e && null == newLoadingSettings.customSvgUrl) return console.error('Notiflix Error: You have to set a static SVG url to "customSvgUrl" option to use Loading Custom.'), !1;
            "notiflix" === e && (l = notiflixLoadingSvgNotiflix(newLoadingSettings.svgSize, "#f8f8f8", "#00b462"))
        }
        var c = 0;
        0 < t.length && (c = "-" + Math.round(a - a / 3).toString() + "px");
        var m = '<div style="top:' + c + "; width:" + newLoadingSettings.svgSize + "; height:" + newLoadingSettings.svgSize + ';" class="' + newLoadingSettings.className + "-icon" + (0 < t.length ? " with-message" : "") + '">' + l + "</div>",
            p = document.getElementsByTagName("body")[0],
            g = document.createElement("div");
        if (g.id = loadingSettings.ID, g.className = newLoadingSettings.className + (newLoadingSettings.cssAnimation ? " with-animation" : "") + (newLoadingSettings.clickToClose ? " click-to-close" : ""), g.style.zIndex = newLoadingSettings.zindex, g.style.background = newLoadingSettings.backgroundColor, g.style.animationDuration = newLoadingSettings.cssAnimationDuration + "ms", g.style.fontFamily = '"' + newLoadingSettings.fontFamily + '", sans-serif', newLoadingSettings.rtl && (g.setAttribute("dir", "rtl"), g.classList.add("rtl-on")), g.innerHTML = m + o, !document.getElementById(g.id))
            if (p.appendChild(g), newLoadingSettings.clickToClose) document.getElementById(g.id).addEventListener("click", function() {
                g.classList.add("remove"), setTimeout(function() {
                    g.parentNode.removeChild(g)
                }, newLoadingSettings.cssAnimationDuration)
            })
    } else if (document.getElementById(loadingSettings.ID)) {
        var f = document.getElementById(loadingSettings.ID);
        setTimeout(function() {
            f.classList.add("remove"), setTimeout(function() {
                f.parentNode.removeChild(f)
            }, newLoadingSettings.cssAnimationDuration)
        }, i)
    }
}

function NotiflixLoadingChange(t) {
    if (document.getElementById(loadingSettings.ID))
        if (0 < (t = t.toString().length > newLoadingSettings.messageMaxLength ? notiflixPlaintext(t).toString().substring(0, newLoadingSettings.messageMaxLength) + "..." : notiflixPlaintext(t).toString()).length) {
            var e = document.getElementById(loadingSettings.ID).getElementsByTagName("p")[0];
            if (void 0 !== e) e.innerHTML = t;
            else {
                var n = document.createElement("p");
                n.id = newLoadingSettings.messageID, n.className = "loading-message new", n.style.color = newLoadingSettings.messageColor, n.style.fontSize = newLoadingSettings.messageFontSize;
                var i = parseInt(newLoadingSettings.svgSize.slice(0, -2)),
                    a = Math.round(i - i / 3).toString() + "px";
                n.style.top = a;
                var o = (1.2 * parseInt(newLoadingSettings.messageFontSize.slice(0, 2))).toString() + "px";
                n.style.height = o, n.innerHTML = t, document.getElementById(loadingSettings.ID).appendChild(n);
                var s = document.getElementById(loadingSettings.ID).getElementsByTagName("div")[0],
                    r = "-" + Math.round(i - i / 3).toString() + "px";
                s.style.top = r
            }
        } else console.error("Notiflix Error: Where is the new message?")
}

function notiflixReportSvgSuccess(t, e) {
    return t || (t = "110px"), e || (e = "#00b462"), '<svg id="NXReportSuccess" fill="' + e + '" width="' + t + '" height="' + t + '" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 124.69 117.93" style="enable-background:new 0 0 124.69 117.93;" xml:space="preserve"><style type="text/css"> .st0{fill:#070308;} .st1{fill:#C23029;} .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#B1B1B1;} .st3{fill-rule:evenodd;clip-rule:evenodd;fill:#C23029;} input[type="submit"]:disabled { opacity: 0.65; cursor: not-allowed; } </style><filter id="dropshadow2" height="130%"><feGaussianBlur in="SourceAlpha" stdDeviation="5"/><feOffset dx="0" dy="0" result="offsetblur"/><feFlood flood-color="red"/><feComposite in2="offsetblur" operator="in"/><feMerge><feMergeNode/><feMergeNode in="SourceGraphic"/></feMerge></filter><g><g><path class="st0 path2"  d="M22.59,98.23c-0.55-0.03-1.05-0.23-1.49-0.59c-0.45-0.36-0.78-0.8-1-1.29c-1.3-3.02-2.54-6.11-3.67-9.17'+
            'C15.29,84.1,14.1,81,12.9,77.94l-0.04-0.07c-0.1-0.1-0.23-0.16-0.4-0.19c-0.2-0.03-0.38,0.06-0.46,0.23'
            +'c-0.25,0.47-0.46,0.97-0.63,1.51c-0.16,0.51-0.34,1.02-0.53,1.51l-2.44,6.28c-0.6,1.54-1.2,3.08-1.81,4.61'
            +'c-0.59,1.47-1.19,2.95-1.79,4.44L4.72,96.4c-0.23,0.48-0.57,0.9-1,1.26c-0.42,0.35-0.9,0.54-1.44,0.57l-0.05,0L1.8,98.52'
            +'l0.23,0.46l0.18,0.12l0.06,0c1.55-0.09,3.1-0.14,4.62-0.16l0.43-0.21v-0.51L7.2,98.17C6.99,98.08,6.79,98,6.6,97.94'
            +'c-0.09-0.03-0.14-0.16-0.14-0.38c0-0.02,0.01-0.12,0.17-0.57c0.12-0.35,0.28-0.77,0.47-1.27c0.19-0.5,0.41-1.03,0.64-1.6'
            +'C7.98,93.56,8.2,93,8.41,92.47c0.21-0.53,0.4-1,0.56-1.4c0.13-0.32,0.23-0.57,0.31-0.75h5.28c0.06,0.15,0.15,0.38,0.27,0.69'
            +'c0.16,0.4,0.35,0.87,0.57,1.41c0.22,0.53,0.45,1.1,0.68,1.69c0.24,0.59,0.45,1.14,0.64,1.64l0.49,1.26'
            +'c0.11,0.29,0.14,0.41,0.16,0.45l0,0l0,0.05c0.01,0.14-0.03,0.25-0.12,0.35c-0.1,0.11-0.22,0.16-0.36,0.18l-0.45,0.25l0.21,0.42'
            +'l0.21,0.18l0.06,0c0.95,0.04,1.9,0.06,2.85,0.08c0.95,0.02,1.9,0.05,2.84,0.1l0.06,0l0.49-0.27l-0.5-0.59L22.59,98.23z'
            + 'M11.92,88.5c-0.64,0.01-1.28,0.01-1.93-0.01c0.36-0.85,0.7-1.73,1.02-2.61c0.29-0.81,0.6-1.62,0.92-2.42l1.93,5.02'
            +'C13.21,88.49,12.57,88.49,11.92,88.5z"/>'
            + '          <path class="st0 path2" d="M38.6,78.11c-0.04-0.07-0.14-0.16-0.34-0.16c-0.87,0.02-1.75,0.05-2.61,0.09c-0.84,0.04-1.73,0.07-2.62,0.09'
            +'c-3.64,0.09-7.33,0.06-10.94-0.08l-0.03,0l-0.29,0.07l-0.04,0.04c-0.33,0.35-0.66,0.7-1,1.04c-0.33,0.34-0.67,0.69-1.02,1.04'
            +'l-0.05,0.05l-0.05,0.45l0.44,0.26l0.34-0.08l0.04-0.04c0.38-0.36,0.82-0.61,1.31-0.74c0.51-0.14,1.02-0.2,1.52-0.2'
            +'c1.78,0.05,3.58,0.08,5.36,0.08c0.02,2.75,0.03,5.5,0.03,8.24v8.43c0,0.42-0.15,0.78-0.46,1.1c-0.31,0.31-0.66,0.47-1.08,0.47'
            +'l-0.5,0.25l0.08,0.17c0.02,0.03,0.04,0.09,0.07,0.18c0.09,0.25,0.28,0.26,0.32,0.27c0.23,0.02,0.46,0.04,0.7,0.04'
            +'c0.11,0,0.23,0,0.35-0.01c0.33-0.02,0.67-0.02,1,0h4.12l0.39-0.22l-0.05-0.51l-0.36-0.14l-0.04,0c-0.44,0.02-0.81-0.13-1.1-0.45'
            +'c-0.3-0.33-0.46-0.71-0.49-1.15c-0.04-0.87-0.05-1.78-0.05-2.7v-2.73c-0.02-1.9-0.02-3.82-0.01-5.71'
            +'c0.01-1.88,0.03-3.74,0.06-5.55c0.86,0,1.72-0.02,2.56-0.07c0.91-0.04,1.83-0.06,2.72-0.04c0.15,0.02,0.27,0.04,0.35,0.09'
            +'c0.04,0.02,0.08,0.11,0.11,0.23l0.06-0.01l-0.05,0.02c0.06,0.16,0.11,0.33,0.13,0.52c0.03,0.21,0.06,0.4,0.09,0.58l0.02,0.08'
            +'l0.38,0.25l0.09-0.03c0.06-0.02,0.13-0.05,0.22-0.09c0.12-0.05,0.21-0.14,0.27-0.25l0.02-0.08c0.02-0.47,0.04-0.94,0.07-1.41'
            +'c0.03-0.47,0.06-0.94,0.09-1.42l0.01-0.07l-0.04-0.06C38.65,78.19,38.62,78.15,38.6,78.11z"/>'
            +'           <path class="st0 path2" d="M62.51,97.5c-0.53-0.41-1.04-0.83-1.51-1.24c-0.47-0.41-0.95-0.86-1.44-1.33c-0.48-0.47-1.01-1.01-1.58-1.62'
            +'c-0.57-0.6-1.22-1.32-1.94-2.14c-0.67-0.76-1.43-1.63-2.3-2.62c0.42-0.14,0.82-0.33,1.19-0.56c0.46-0.29,0.88-0.65,1.24-1.06'
            +'c0.42-0.49,0.74-1.05,0.95-1.66c0.22-0.61,0.33-1.25,0.33-1.88c0-0.76-0.14-1.47-0.42-2.13c-0.28-0.65-0.68-1.23-1.17-1.72'
            +'c-0.5-0.49-1.09-0.88-1.76-1.16c-0.67-0.28-1.38-0.42-2.12-0.42c-1.37-0.02-2.73-0.04-4.08-0.08c-1.36-0.04-2.72-0.08-4.12-0.13'
            +'c-0.09,0.01-0.33,0.04-0.4,0.23c-0.05,0.14-0.06,0.26-0.01,0.35c0.03,0.07,0.1,0.13,0.17,0.15c0.07,0.02,0.14,0.05,0.2,0.08'
            +'l0.08,0.02c0.46,0.02,0.82,0.19,1.12,0.52c0.31,0.35,0.47,0.74,0.5,1.19c0.09,1.3,0.13,2.62,0.14,3.93'
            +'c0.01,1.31,0.01,2.63,0.01,3.94c0,1.4,0,2.8-0.01,4.2c-0.01,1.4-0.06,2.81-0.14,4.19c-0.03,0.45-0.19,0.83-0.48,1.16'
            +'c-0.28,0.32-0.64,0.49-1.1,0.52l-0.51,0.24l0.09,0.17c0.02,0.03,0.04,0.09,0.07,0.16c0.05,0.11,0.12,0.2,0.2,0.25l0.05,0.04'
            +'l0.06,0c1.02-0.05,2.04-0.09,3.03-0.12c1.01-0.03,2.02-0.05,3.03-0.07l0.48-0.22l-0.11-0.48l-0.32-0.14l-0.04,0'
            +'c-0.42,0.02-0.78-0.13-1.05-0.46c-0.29-0.33-0.43-0.7-0.43-1.11c0-0.91,0.01-1.83,0.04-2.73c0.03-0.91,0.05-1.82,0.07-2.74'
            +'c0.02-1.94,0.01-3.88-0.01-5.76c-0.03-1.83-0.06-3.7-0.11-5.56c0.41,0,0.82-0.01,1.24-0.03c0.47-0.02,0.94,0,1.4,0.06'
            +'c0.46,0.06,0.9,0.17,1.31,0.33c0.4,0.16,0.77,0.41,1.1,0.75c0.37,0.38,0.64,0.82,0.81,1.31c0.17,0.49,0.25,0.99,0.22,1.49'
            +'c-0.02,0.5-0.14,0.99-0.35,1.47c-0.21,0.47-0.51,0.88-0.91,1.23c-0.5,0.43-1.01,0.73-1.51,0.87c-0.52,0.15-1.12,0.25-1.75,0.32'
            +'c-0.06,0-0.12,0.01-0.19,0.04c-0.06,0.02-0.12,0.04-0.16,0.06l-0.1,0.05l-0.05,0.46l0.07,0.07c1.37,1.37,2.72,2.74,4.07,4.11'
            +'c1.35,1.38,2.71,2.76,4.07,4.14l0.57,0.56c0.23,0.23,0.46,0.44,0.69,0.64c0.24,0.21,0.49,0.4,0.74,0.58'
            +'c0.26,0.18,0.5,0.32,0.72,0.41c0.5,0.21,1.1,0.39,1.77,0.52c0.68,0.13,1.26,0.27,1.77,0.41l0.15,0.04l0.36-0.68l-0.14-0.1'
            +'C63.66,98.37,63.05,97.91,62.51,97.5z"/>'
            +'           <path class="st0 path2" d="M73.16,98.21c-0.41-0.06-0.76-0.26-1.05-0.6c-0.29-0.34-0.44-0.71-0.44-1.12V80.46c0-0.43,0.14-0.8,0.44-1.13'
            +'c0.3-0.34,0.64-0.54,1.06-0.62l0.45-0.32l-0.07-0.14c-0.02-0.04-0.05-0.1-0.1-0.18c-0.07-0.11-0.15-0.18-0.25-0.21l-0.04-0.01'
            +'l-0.05,0.01c-1.86,0.28-3.8,0.27-5.67,0.03l-0.05-0.01l-0.5,0.22l0.41,0.64l0.09,0.01c0.41,0.05,0.75,0.24,1.04,0.59'
            +'c0.3,0.35,0.44,0.72,0.44,1.14v16.03c0,0.41-0.14,0.78-0.44,1.12c-0.3,0.34-0.64,0.54-1.05,0.6l-0.45,0.32l0.07,0.14'
            +'c0.02,0.04,0.05,0.1,0.1,0.17c0.07,0.11,0.15,0.18,0.25,0.21l0.04,0.01l0.04-0.01c1.88-0.26,3.79-0.26,5.67,0l0.06,0.01l0.48-0.27'
            +'l-0.41-0.59L73.16,98.21z"/>'
            + '          <path class="st0 path2" d="M98.19,78.54l0.05-0.86l-0.21,0.01c-0.86,0.04-1.71,0.07-2.56,0.09c-0.85,0.03-1.71,0.06-2.56,0.09l-0.05,0'
            +'l-0.4,0.27l0.15,0.49l0.26,0.1l0.04,0c0.22-0.01,0.4,0.02,0.57,0.1c0.18,0.08,0.33,0.2,0.46,0.35c0.13,0.15,0.23,0.32,0.3,0.5'
            +'c0.07,0.19,0.11,0.39,0.11,0.59c0,0.59-0.02,1.19-0.05,1.79c-0.04,0.61-0.06,1.21-0.08,1.8c-0.04,1.1-0.05,2.25-0.05,3.42'
            +'c0,1.17,0.01,2.31,0.03,3.42c0,0.85-0.14,1.65-0.42,2.39c-0.28,0.73-0.68,1.38-1.2,1.93c-0.51,0.55-1.13,0.99-1.84,1.31'
            +'c-0.71,0.32-1.5,0.48-2.36,0.48c-0.86,0-1.66-0.16-2.38-0.48c-0.72-0.32-1.35-0.76-1.88-1.31c-0.53-0.55-0.95-1.2-1.25-1.93'
            +'c-0.3-0.73-0.45-1.54-0.45-2.38c0-1.05,0-2.13,0.01-3.24c0.01-1.11,0-2.19-0.01-3.22c-0.02-0.67-0.04-1.33-0.08-1.98'
            +'c-0.04-0.65-0.04-1.29-0.03-1.93c0.02-0.41,0.16-0.79,0.42-1.12c0.25-0.32,0.59-0.46,1.04-0.45l0.05,0l0.45-0.25l-0.08-0.17'
            +'c-0.02-0.03-0.04-0.09-0.07-0.18c-0.09-0.24-0.26-0.27-0.32-0.27c0,0,0,0,0,0c-0.34-0.04-0.69-0.05-1.04-0.04'
            +'c-0.33,0.01-0.68,0-1.03-0.01l-2.04-0.05c-0.67-0.02-1.35-0.04-2.03-0.08l-0.05,0l-0.46,0.21l0.09,0.18'
            +'c0.02,0.04,0.05,0.1,0.09,0.19c0.09,0.19,0.21,0.26,0.31,0.28c0.49,0.1,0.89,0.28,1.18,0.53c0.28,0.25,0.45,0.63,0.5,1.16'
            +'c0.07,0.78,0.12,1.62,0.14,2.51c0.03,0.89,0.04,1.8,0.04,2.7c0,0.91-0.01,1.81-0.03,2.7c-0.02,0.89-0.03,1.75-0.03,2.55'
            +'c0,1.12,0.19,2.21,0.58,3.23c0.38,1.03,0.98,1.96,1.76,2.76c0.79,0.8,1.72,1.42,2.78,1.82c1.05,0.4,2.14,0.6,3.25,0.6'
            +'c1.12,0,2.22-0.21,3.25-0.62c1.04-0.41,1.96-1.03,2.75-1.83c0.79-0.8,1.39-1.73,1.79-2.76c0.4-1.02,0.6-2.11,0.58-3.24'
            +'c0-0.81,0-1.66-0.01-2.55c-0.01-0.89-0.01-1.79-0.01-2.7c0-0.9,0.02-1.81,0.05-2.69c0.03-0.88,0.09-1.73,0.16-2.53'
            +'c0.03-0.45,0.2-0.83,0.51-1.15c0.31-0.33,0.68-0.5,1.15-0.53L98.19,78.54z"/>'
             +          '<path class="st0 path2" d="M122.7,98.24c-0.12-0.05-0.23-0.06-0.32-0.05c-0.01,0-0.05,0-0.14-0.03c-0.33-0.09-0.59-0.27-0.79-0.52'
            +'c-0.21-0.27-0.36-0.58-0.43-0.93c-0.19-0.97-0.34-1.96-0.44-2.95c-0.11-1-0.22-1.99-0.34-2.97c-0.24-2.09-0.45-4.29-0.62-6.53'
            +'c-0.16-2.23-0.29-4.44-0.38-6.56l0-0.08l-0.31-0.31l-0.1,0.01c-0.39,0.05-0.51,0.1-0.57,0.21c-0.45,0.83-0.89,1.7-1.29,2.59'
            +'l-1.21,2.63c-0.42,0.93-0.84,1.89-1.25,2.85c-0.41,0.97-0.82,1.96-1.22,2.96c-0.4,1-0.81,1.99-1.21,2.97'
            +'c-0.35,0.85-0.69,1.67-1.04,2.48c-1.32-2.62-2.63-5.33-3.91-8.07c-1.35-2.89-2.78-5.71-4.26-8.38l-0.05-0.06'
            +'c-0.1-0.07-0.23-0.12-0.41-0.15c-0.25-0.04-0.44,0.08-0.5,0.32c-0.06,0.26-0.08,0.58-0.07,0.94c0.01,0.34,0,0.64-0.04,0.91'
            +'c-0.05,1.22-0.13,2.42-0.24,3.57c-0.12,1.59-0.27,3.29-0.43,5.07c-0.16,1.74-0.34,3.44-0.54,5.03c-0.09,0.59-0.16,1.19-0.22,1.81'
            +'c-0.06,0.59-0.15,1.18-0.27,1.76c-0.08,0.42-0.26,0.76-0.55,1.04c-0.29,0.28-0.62,0.43-1.04,0.46l-0.49,0.23l0.12,0.36'
            +'c0.05,0.14,0.15,0.24,0.28,0.27l0.03,0.01l0.03,0c0.86-0.04,1.71-0.06,2.57-0.08c0.86-0.02,1.72-0.04,2.58-0.08l0.5-0.27l-0.2-0.5'
            +'l-0.25-0.1l-0.04,0c-0.43,0.03-0.76-0.09-1.04-0.38c-0.29-0.29-0.44-0.63-0.47-1.02c0.03-0.05,0.04-0.1,0.04-0.14'
            +'c0.3-1.98,0.55-4.07,0.75-6.21c0.18-1.91,0.35-3.77,0.51-5.57c0.73,1.45,1.46,3,2.18,4.6c0.82,1.83,1.66,3.59,2.5,5.25l0.45,0.89'
            +'c0.19,0.38,0.39,0.79,0.59,1.21c0.2,0.42,0.4,0.82,0.59,1.19c0.19,0.37,0.35,0.64,0.47,0.79c0.11,0.14,0.27,0.2,0.46,0.19'
            +'c0.17-0.01,0.31-0.07,0.41-0.17l0.04-0.06c0.99-2.22,1.98-4.56,2.95-6.96c0.86-2.13,1.75-4.24,2.67-6.27'
            +'c0.1,1.03,0.2,2.11,0.31,3.25c0.14,1.48,0.27,2.78,0.41,3.99c0.09,0.67,0.17,1.32,0.26,1.96c0.09,0.64,0.18,1.29,0.26,1.93v0.13'
            +'c0,0.4-0.14,0.73-0.44,1.01c-0.3,0.28-0.63,0.42-1.01,0.39l-0.06,0l-0.41,0.26l0.09,0.16c0.18,0.33,0.26,0.44,0.41,0.44'
            +'c0.09,0,0.3,0,0.64,0.01c0.34,0.01,0.73,0.02,1.18,0.04c0.45,0.02,0.92,0.04,1.42,0.05c0.5,0.02,0.96,0.03,1.4,0.04'
            +'c0.43,0.01,0.79,0.01,1.07,0.01h0.47l0.05-0.01c0.2-0.06,0.32-0.19,0.35-0.38C123.1,98.6,123.09,98.35,122.7,98.24z"/>'
                   +'</g>'
               +    '<g>'
               +         '<g id="XMLID_738_">'
               +            '<path id="XMLID_744_" class="st2 path2" d="M64.03,15.5V4.92h12.37l-6.94,12.02C67.77,16.18,65.95,15.68,64.03,15.5z"/>'
               +            '<path id="XMLID_743_" class="st2 path2" d="M77.74,25.23l9.15-5.28l6.18,10.71H79.19C79.01,28.74,78.51,26.91,77.74,25.23z"/>'
               +            '<path id="XMLID_742_" class="st2 path2" d="M76.16,41.98l9.15,5.29l-6.18,10.71l-6.94-12.02C73.72,44.86,75.06,43.52,76.16,41.98z"/>'
               +            '<path id="XMLID_741_" class="st2 path2" d="M60.86,48.98v10.57H48.5l6.94-12.02C57.12,48.3,58.94,48.8,60.86,48.98z"/>'
               +            '<path id="XMLID_740_" class="st2 path2" d="M47.15,39.24L38,44.53l-6.18-10.71H45.7C45.88,35.74,46.38,37.57,47.15,39.24z"/>'
               +           '<path id="XMLID_739_" class="st2 path2" d="M48.73,22.5l-9.15-5.29l6.18-10.71l6.94,12.02C51.17,19.62,49.83,20.96,48.73,22.5z"/>'
               +        '</g>'
               +        '<path id="XMLID_737_" class="st3 path2"  d="M72.19,18.53l5.29-9.16l10.71,6.18L76.16,22.5C75.09,20.99,73.76,19.64,72.19,18.53z"/>'
               +        '<path id="XMLID_736_" class="st3 path2" d="M79.19,33.82h10.57v12.37l-12.02-6.94C78.54,37.49,79.02,35.66,79.19,33.82z"/>'
               +        '<path id="XMLID_735_" class="st3 path2" d="M69.45,47.53l5.29,9.15l-10.71,6.18V48.98C65.9,48.81,67.74,48.31,69.45,47.53z"/>'
               +        '<path id="XMLID_734_" class="st3 path2" d="M52.7,45.95l-5.29,9.16l-10.71-6.18l12.02-6.94C49.8,43.49,51.13,44.83,52.7,45.95z"/>'
               +        '<path id="XMLID_733_" class="st3 path2" d="M45.7,30.66H35.13V18.29l12.02,6.94C46.35,26.99,45.87,28.82,45.7,30.66z"/>'
               +        '<path id="XMLID_732_" class="st3 path2" d="M55.44,16.95l-5.29-9.15l10.71-6.18V15.5C58.99,15.67,57.15,16.17,55.44,16.95z"/>'
               +    '</g>'
               +'</g>'
          +'</svg>'
}

function notiflixReportSvgFailure(t, e) {
    return t || (t = "110px"), e || (e = "#f44336"), '<svg id="NXReportFailure" fill=" ' + e + '" width="' + t + '" height="' + t + '" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 120 120" xmlns:xlink="http://www.w3.org/1999/xlink"><style>@-webkit-keyframes NXReportFailure4-animation{0%{opacity: 0;}40%{opacity: 1;}100%{opacity: 1;}}@keyframes NXReportFailure4-animation{0%{opacity: 0;}40%{opacity: 1;}100%{opacity: 1;}}@-webkit-keyframes NXReportFailure3-animation{0%{-webkit-transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);}40%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}60%{-webkit-transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);}100%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}}@keyframes NXReportFailure3-animation{0%{-webkit-transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);}40%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}60%{-webkit-transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);}100%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}}@-webkit-keyframes NXReportFailure5-animation{0%{-webkit-transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);}50%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}60%{-webkit-transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);}100%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}}@keyframes NXReportFailure5-animation{0%{-webkit-transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);}50%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}60%{-webkit-transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);}100%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}}@-webkit-keyframes NXReportFailure6-animation{0%{opacity: 0;}50%{opacity: 1;}100%{opacity: 1;}}@keyframes NXReportFailure6-animation{0%{opacity: 0;}50%{opacity: 1;}100%{opacity: 1;}}#NXReportFailure *{-webkit-animation-duration: 1.2s;animation-duration: 1.2s;-webkit-animation-timing-function: cubic-bezier(0, 0, 1, 1);animation-timing-function: cubic-bezier(0, 0, 1, 1);}#NXReportFailure6{fill:inherit;-webkit-animation-name: NXReportFailure6-animation;animation-name: NXReportFailure6-animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);opacity: 1;}#NXReportFailure5{-webkit-animation-name: NXReportFailure5-animation;animation-name: NXReportFailure5-animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}#NXReportFailure3{-webkit-animation-name: NXReportFailure3-animation;animation-name: NXReportFailure3-animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}#NXReportFailure4{fill:inherit;-webkit-animation-name: NXReportFailure4-animation;animation-name: NXReportFailure4-animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);opacity: 1;}</style><g id="NXReportFailure1"><g id="NXReportFailure2"><g id="NXReportFailure3" data-animator-group="true" data-animator-type="2"><path d="M4.35 34.95c0,-16.82 13.78,-30.6 30.6,-30.6l50.1 0c16.82,0 30.6,13.78 30.6,30.6l0 50.1c0,16.82 -13.78,30.6 -30.6,30.6l-50.1 0c-16.82,0 -30.6,-13.78 -30.6,-30.6l0 -50.1zm30.6 85.05l50.1 0c19.22,0 34.95,-15.73 34.95,-34.95l0 -50.1c0,-19.22 -15.73,-34.95 -34.95,-34.95l-50.1 0c-19.22,0 -34.95,15.73 -34.95,34.95l0 50.1c0,19.22 15.73,34.95 34.95,34.95z" id="NXReportFailure4"/></g><g id="NXReportFailure5" data-animator-group="true" data-animator-type="2"><path d="M82.4 37.6c-0.9,-0.9 -2.37,-0.9 -3.27,0l-19.13 19.13 -19.14 -19.13c-0.9,-0.9 -2.36,-0.9 -3.26,0 -0.9,0.9 -0.9,2.35 0,3.26l19.13 19.14 -19.13 19.13c-0.9,0.9 -0.9,2.37 0,3.27 0.45,0.45 1.04,0.68 1.63,0.68 0.59,0 1.18,-0.23 1.63,-0.68l19.14 -19.14 19.13 19.14c0.45,0.45 1.05,0.68 1.64,0.68 0.58,0 1.18,-0.23 1.63,-0.68 0.9,-0.9 0.9,-2.37 0,-3.27l-19.14 -19.13 19.14 -19.14c0.9,-0.91 0.9,-2.36 0,-3.26z" id="NXReportFailure6"/></g></g></g></svg>'
}

function notiflixReportSvgWarning(t, e) {
    return t || (t = "110px"), e || (e = "#f2bd1d"), '<svg id="NXReportWarning" fill="' + e + '" width="' + t + '" height="' + t + '" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 120 120" xmlns:xlink="http://www.w3.org/1999/xlink"><style>@-webkit-keyframes NXReportWarning3-animation{0%{opacity: 0;}40%{opacity: 1;}100%{opacity: 1;}}@keyframes NXReportWarning3-animation{0%{opacity: 0;}40%{opacity: 1;}100%{opacity: 1;}}@-webkit-keyframes NXReportWarning2-animation{0%{-webkit-transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);}40%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}60%{-webkit-transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);}100%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}}@keyframes NXReportWarning2-animation{0%{-webkit-transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);}40%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}60%{-webkit-transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);}100%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}}@-webkit-keyframes NXReportWarning4-animation{0%{-webkit-transform: translate(60px, 66.6px) scale(0.5, 0.5) translate(-60px, -66.6px);transform: translate(60px, 66.6px) scale(0.5, 0.5) translate(-60px, -66.6px);}50%{-webkit-transform: translate(60px, 66.6px) scale(1, 1) translate(-60px, -66.6px);transform: translate(60px, 66.6px) scale(1, 1) translate(-60px, -66.6px);}60%{-webkit-transform: translate(60px, 66.6px) scale(0.95, 0.95) translate(-60px, -66.6px);transform: translate(60px, 66.6px) scale(0.95, 0.95) translate(-60px, -66.6px);}100%{-webkit-transform: translate(60px, 66.6px) scale(1, 1) translate(-60px, -66.6px);transform: translate(60px, 66.6px) scale(1, 1) translate(-60px, -66.6px);}}@keyframes NXReportWarning4-animation{0%{-webkit-transform: translate(60px, 66.6px) scale(0.5, 0.5) translate(-60px, -66.6px);transform: translate(60px, 66.6px) scale(0.5, 0.5) translate(-60px, -66.6px);}50%{-webkit-transform: translate(60px, 66.6px) scale(1, 1) translate(-60px, -66.6px);transform: translate(60px, 66.6px) scale(1, 1) translate(-60px, -66.6px);}60%{-webkit-transform: translate(60px, 66.6px) scale(0.95, 0.95) translate(-60px, -66.6px);transform: translate(60px, 66.6px) scale(0.95, 0.95) translate(-60px, -66.6px);}100%{-webkit-transform: translate(60px, 66.6px) scale(1, 1) translate(-60px, -66.6px);transform: translate(60px, 66.6px) scale(1, 1) translate(-60px, -66.6px);}}@-webkit-keyframes NXReportWarning5-animation{0%{opacity: 0;}50%{opacity: 1;}100%{opacity: 1;}}@keyframes NXReportWarning5-animation{0%{opacity: 0;}50%{opacity: 1;}100%{opacity: 1;}}#NXReportWarning *{-webkit-animation-duration: 1.2s;animation-duration: 1.2s;-webkit-animation-timing-function: cubic-bezier(0, 0, 1, 1);animation-timing-function: cubic-bezier(0, 0, 1, 1);}#NXReportWarning3{fill: inherit;-webkit-animation-name: NXReportWarning3-animation;animation-name: NXReportWarning3-animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);opacity: 1;}#NXReportWarning5{fill: inherit;-webkit-animation-name: NXReportWarning5-animation;animation-name: NXReportWarning5-animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);opacity: 1;}#NXReportWarning4{-webkit-animation-name: NXReportWarning4-animation;animation-name: NXReportWarning4-animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform: translate(60px, 66.6px) scale(1, 1) translate(-60px, -66.6px);transform: translate(60px, 66.6px) scale(1, 1) translate(-60px, -66.6px);}#NXReportWarning2{-webkit-animation-name: NXReportWarning2-animation;animation-name: NXReportWarning2-animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}</style><g id="NXReportWarning1"><g id="NXReportWarning2" data-animator-group="true" data-animator-type="2"><path d="M115.46 106.15l-54.04 -93.8c-0.61,-1.06 -2.23,-1.06 -2.84,0l-54.04 93.8c-0.62,1.07 0.21,2.29 1.42,2.29l108.08 0c1.21,0 2.04,-1.22 1.42,-2.29zm-50.29 -95.95l54.04 93.8c2.28,3.96 -0.65,8.78 -5.17,8.78l-108.08 0c-4.52,0 -7.45,-4.82 -5.17,-8.78l54.04 -93.8c2.28,-3.95 8.03,-4 10.34,0z" id="NXReportWarning3"/></g><g id="NXReportWarning4" data-animator-group="true" data-animator-type="2"><path d="M57.83 94.01c0,1.2 0.97,2.17 2.17,2.17 1.2,0 2.17,-0.97 2.17,-2.17l0 -3.2c0,-1.2 -0.97,-2.17 -2.17,-2.17 -1.2,0 -2.17,0.97 -2.17,2.17l0 3.2zm0 -14.15c0,1.2 0.97,2.17 2.17,2.17 1.2,0 2.17,-0.97 2.17,-2.17l0 -40.65c0,-1.2 -0.97,-2.17 -2.17,-2.17 -1.2,0 -2.17,0.97 -2.17,2.17l0 40.65z" id="NXReportWarning5"/></g></g></svg>'
}

function notiflixReportSvgInfo(t, e) {
    return t || (t = "110px"), e || (e = "#00bcd4"), '<svg id="NXReportInfo" fill="' + e + '" width="' + t + '" height="' + t + '" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 120 120" xmlns:xlink="http://www.w3.org/1999/xlink"><style>@-webkit-keyframes NXReportInfo5-animation{0%{opacity: 0;}50%{opacity: 1;}100%{opacity: 1;}}@keyframes NXReportInfo5-animation{0%{opacity: 0;}50%{opacity: 1;}100%{opacity: 1;}}@-webkit-keyframes NXReportInfo4-animation{0%{-webkit-transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);}50%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}60%{-webkit-transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);}100%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}}@keyframes NXReportInfo4-animation{0%{-webkit-transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);}50%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}60%{-webkit-transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);}100%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}}@-webkit-keyframes NXReportInfo3-animation{0%{opacity: 0;}40%{opacity: 1;}100%{opacity: 1;}}@keyframes NXReportInfo3-animation{0%{opacity: 0;}40%{opacity: 1;}100%{opacity: 1;}}@-webkit-keyframes NXReportInfo2-animation{0%{-webkit-transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);}40%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}60%{-webkit-transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);}100%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}}@keyframes NXReportInfo2-animation{0%{-webkit-transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.5, 0.5) translate(-60px, -60px);}40%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}60%{-webkit-transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);transform: translate(60px, 60px) scale(0.95, 0.95) translate(-60px, -60px);}100%{-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}}#NXReportInfo *{-webkit-animation-duration: 1.2s;animation-duration: 1.2s;-webkit-animation-timing-function: cubic-bezier(0, 0, 1, 1);animation-timing-function: cubic-bezier(0, 0, 1, 1);}#NXReportInfo3{fill:inherit;-webkit-animation-name: NXReportInfo3-animation;animation-name: NXReportInfo3-animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);opacity: 1;}#NXReportInfo5{fill:inherit;-webkit-animation-name: NXReportInfo5-animation;animation-name: NXReportInfo5-animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);opacity: 1;}#NXReportInfo2{-webkit-animation-name: NXReportInfo2-animation;animation-name: NXReportInfo2-animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}#NXReportInfo4{-webkit-animation-name: NXReportInfo4-animation;animation-name: NXReportInfo4-animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);transform: translate(60px, 60px) scale(1, 1) translate(-60px, -60px);}</style><g id="NXReportInfo1"><g id="NXReportInfo2" data-animator-group="true" data-animator-type="2"><path d="M60 115.38c-30.54,0 -55.38,-24.84 -55.38,-55.38 0,-30.54 24.84,-55.38 55.38,-55.38 30.54,0 55.38,24.84 55.38,55.38 0,30.54 -24.84,55.38 -55.38,55.38zm0 -115.38c-33.08,0 -60,26.92 -60,60 0,33.08 26.92,60 60,60 33.08,0 60,-26.92 60,-60 0,-33.08 -26.92,-60 -60,-60z" id="NXReportInfo3"/></g><g id="NXReportInfo4" data-animator-group="true" data-animator-type="2"><path d="M57.75 43.85c0,-1.24 1.01,-2.25 2.25,-2.25 1.24,0 2.25,1.01 2.25,2.25l0 48.18c0,1.24 -1.01,2.25 -2.25,2.25 -1.24,0 -2.25,-1.01 -2.25,-2.25l0 -48.18zm0 -15.88c0,-1.24 1.01,-2.25 2.25,-2.25 1.24,0 2.25,1.01 2.25,2.25l0 3.32c0,1.25 -1.01,2.25 -2.25,2.25 -1.24,0 -2.25,-1 -2.25,-2.25l0 -3.32z" id="NXReportInfo5"/></g></g></svg>'
}

function notiflixLoadingSvgStandard(t, e) {
    return t || (t = "60px"), e || (e = "#00b462"), '<svg stroke="' + e + '" width="' + t + '" height="' + t + '" viewBox="0 0 38 38" style="transform:scale(0.8);" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><g transform="translate(1 1)" stroke-width="2"><circle stroke-opacity=".25" cx="18" cy="18" r="18"/><path d="M36 18c0-9.94-8.06-18-18-18"><animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s" repeatCount="indefinite"/></path></g></g></svg>'
}

function notiflixLoadingSvgHourglass(t, e) {
    return t || (t = "60px"), e || (e = "#00b462"), '<svg id="NXLoadingHourglass" fill="' + e + '" width="' + t + '" height="' + t + '" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 200 200"><style>@-webkit-keyframes NXhourglass5-animation{0%{-webkit-transform: scale(1, 1);transform: scale(1, 1);}16.67%{-webkit-transform: scale(1, 0.8);transform: scale(1, 0.8);}33.33%{-webkit-transform: scale(0.88, 0.6);transform: scale(0.88, 0.6);}37.50%{-webkit-transform: scale(0.85, 0.55);transform: scale(0.85, 0.55);}41.67%{-webkit-transform: scale(0.8, 0.5);transform: scale(0.8, 0.5);}45.83%{-webkit-transform: scale(0.75, 0.45);transform: scale(0.75, 0.45);}50%{-webkit-transform: scale(0.7, 0.4);transform: scale(0.7, 0.4);}54.17%{-webkit-transform: scale(0.6, 0.35);transform: scale(0.6, 0.35);}58.33%{-webkit-transform: scale(0.5, 0.3);transform: scale(0.5, 0.3);}83.33%{-webkit-transform: scale(0.2, 0);transform: scale(0.2, 0);}100%{-webkit-transform: scale(0.2, 0);transform: scale(0.2, 0);}}@keyframes NXhourglass5-animation{0%{-webkit-transform: scale(1, 1);transform: scale(1, 1);}16.67%{-webkit-transform: scale(1, 0.8);transform: scale(1, 0.8);}33.33%{-webkit-transform: scale(0.88, 0.6);transform: scale(0.88, 0.6);}37.50%{-webkit-transform: scale(0.85, 0.55);transform: scale(0.85, 0.55);}41.67%{-webkit-transform: scale(0.8, 0.5);transform: scale(0.8, 0.5);}45.83%{-webkit-transform: scale(0.75, 0.45);transform: scale(0.75, 0.45);}50%{-webkit-transform: scale(0.7, 0.4);transform: scale(0.7, 0.4);}54.17%{-webkit-transform: scale(0.6, 0.35);transform: scale(0.6, 0.35);}58.33%{-webkit-transform: scale(0.5, 0.3);transform: scale(0.5, 0.3);}83.33%{-webkit-transform: scale(0.2, 0);transform: scale(0.2, 0);}100%{-webkit-transform: scale(0.2, 0);transform: scale(0.2, 0);}}@-webkit-keyframes NXhourglass3-animation{0%{-webkit-transform: scale(1, 0.02);transform: scale(1, 0.02);}79.17%{-webkit-transform: scale(1, 1);transform: scale(1, 1);}100%{-webkit-transform: scale(1, 1);transform: scale(1, 1);}}@keyframes NXhourglass3-animation{0%{-webkit-transform: scale(1, 0.02);transform: scale(1, 0.02);}79.17%{-webkit-transform: scale(1, 1);transform: scale(1, 1);}100%{-webkit-transform: scale(1, 1);transform: scale(1, 1);}}@-webkit-keyframes NXhourglass1-animation{0%{-webkit-transform: rotate(0deg);transform: rotate(0deg);}83.33%{-webkit-transform: rotate(0deg);transform: rotate(0deg);}100%{-webkit-transform: rotate(180deg);transform: rotate(180deg);}}@keyframes NXhourglass1-animation{0%{-webkit-transform: rotate(0deg);transform: rotate(0deg);}83.33%{-webkit-transform: rotate(0deg);transform: rotate(0deg);}100%{-webkit-transform: rotate(180deg);transform: rotate(180deg);}}#NXLoadingHourglass *{-webkit-animation-duration: 1.2s;animation-duration: 1.2s;-webkit-animation-iteration-count: infinite;animation-iteration-count: infinite;-webkit-animation-timing-function: cubic-bezier(0, 0, 1, 1);animation-timing-function: cubic-bezier(0, 0, 1, 1);}#NXhourglass7{fill: inherit;}#NXhourglass1{-webkit-animation-name: NXhourglass1-animation;animation-name: NXhourglass1-animation;-webkit-transform-origin: 50% 50%;transform-origin: 50% 50%;transform-box: fill-box;}#NXhourglass3{-webkit-animation-name: NXhourglass3-animation;animation-name: NXhourglass3-animation;-webkit-animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);-webkit-transform-origin: 50% 100%;transform-origin: 50% 100%;transform-box: fill-box;}#NXhourglass5{-webkit-animation-name: NXhourglass5-animation;animation-name: NXhourglass5-animation;-webkit-transform-origin: 50% 100%;transform-origin: 50% 100%;transform-box: fill-box;}g#NXhourglass5,#NXhourglass3{fill: inherit;opacity: .4;}</style><g id="NXhourglass1" data-animator-group="true" data-animator-type="1"><g id="NXhourglass2"><g id="NXhourglass3" data-animator-group="true" data-animator-type="2"><polygon points="100,100 65.62,132.08 65.62,163.22 134.38,163.22 134.38,132.08 " id="NXhourglass4"/></g><g id="NXhourglass5" data-animator-group="true" data-animator-type="2"><polygon points="100,100 65.62,67.92 65.62,36.78 134.38,36.78 134.38,67.92" id="NXhourglass6"/></g> <path d="M51.14 38.89l8.33 0 0 14.93c0,15.1 8.29,28.99 23.34,39.1 1.88,1.25 3.04,3.97 3.04,7.08 0,3.11 -1.16,5.83 -3.04,7.09 -15.05,10.1 -23.34,23.99 -23.34,39.09l0 14.93 -8.33 0c-2.68,0 -4.86,2.18 -4.86,4.86 0,2.69 2.18,4.86 4.86,4.86l97.72 0c2.68,0 4.86,-2.17 4.86,-4.86 0,-2.68 -2.18,-4.86 -4.86,-4.86l-8.33 0 0 -14.93c0,-15.1 -8.29,-28.99 -23.34,-39.09 -1.88,-1.26 -3.04,-3.98 -3.04,-7.09 0,-3.11 1.16,-5.83 3.04,-7.08 15.05,-10.11 23.34,-24 23.34,-39.1l0 -14.93 8.33 0c2.68,0 4.86,-2.18 4.86,-4.86 0,-2.69 -2.18,-4.86 -4.86,-4.86l-97.72 0c-2.68,0 -4.86,2.17 -4.86,4.86 0,2.68 2.18,4.86 4.86,4.86zm79.67 14.93c0,15.87 -11.93,26.25 -19.04,31.03 -4.6,3.08 -7.34,8.75 -7.34,15.15 0,6.41 2.74,12.07 7.34,15.15 7.11,4.78 19.04,15.16 19.04,31.03l0 14.93 -61.62 0 0 -14.93c0,-15.87 11.93,-26.25 19.04,-31.02 4.6,-3.09 7.34,-8.75 7.34,-15.16 0,-6.4 -2.74,-12.07 -7.34,-15.15 -7.11,-4.78 -19.04,-15.16 -19.04,-31.03l0 -14.93 61.62 0 0 14.93z" id="NXhourglass7"/></g></g></svg>'
}

function notiflixLoadingSvgCircle(t, e) {
    return t || (t = "60px"), e || (e = "#00b462"), '<svg id="NXLoadingCircle" width="' + t + '" height="' + t + '" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="25 25 50 50" xml:space="preserve" version="1.1"><style>#NXLoadingCircle{-webkit-animation: rotate 2s linear infinite; animation: rotate 2s linear infinite; height: ' + t + "; -webkit-transform-origin: center center; -ms-transform-origin: center center; transform-origin: center center; width: " + t + '; position: absolute; top: 0; left: 0; margin: auto;}.notiflix-loader-circle-path{stroke-dasharray: 150,200; stroke-dashoffset: -10; -webkit-animation: dash 1.5s ease-in-out infinite, color 1.5s ease-in-out infinite; animation: dash 1.5s ease-in-out infinite, color 1.5s ease-in-out infinite; stroke-linecap: round;}@-webkit-keyframes rotate{100%{-webkit-transform: rotate(360deg); transform: rotate(360deg);}}@keyframes rotate{100%{-webkit-transform: rotate(360deg); transform: rotate(360deg);}}@-webkit-keyframes dash{0%{stroke-dasharray: 1,200; stroke-dashoffset: 0;}50%{stroke-dasharray: 89,200; stroke-dashoffset: -35;}100%{stroke-dasharray: 89,200; stroke-dashoffset: -124;}}@keyframes dash{0%{stroke-dasharray: 1,200; stroke-dashoffset: 0;}50%{stroke-dasharray: 89,200; stroke-dashoffset: -35;}100%{stroke-dasharray: 89,200; stroke-dashoffset: -124;}}</style><circle class="notiflix-loader-circle-path" cx="50" cy="50" r="20" fill="none" stroke="' + e + '" stroke-width="2"/></svg>'
}

function notiflixLoadingSvgArrows(t, e) {
    return t || (t = "60px"), e || (e = "#00b462"), '<svg id="NXLoadingArrows" fill="' + e + '" width="' + t + '" height="' + t + '" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 128 128" xml:space="preserve"><g><path fill="inherit" fill-opacity="1" d="M109.25 55.5h-36l12-12a29.54 29.54 0 0 0-49.53 12H18.75A46.04 46.04 0 0 1 96.9 31.84l12.35-12.34v36zm-90.5 17h36l-12 12a29.54 29.54 0 0 0 49.53-12h16.97A46.04 46.04 0 0 1 31.1 96.16L18.74 108.5v-36z" /><animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64" dur="1.5s" repeatCount="indefinite"></animateTransform></g></svg>'
}

function notiflixLoadingSvgDots(t, e) {
    return t || (t = "60px"), e || (e = "#00b462"), '<svg id="NXLoadingDots" fill="' + e + '" width="' + t + '" height="' + t + '" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><g transform="translate(25 50)"><circle cx="0" cy="0" r="9" fill="inherit" transform="scale(0.239 0.239)"><animateTransform attributeName="transform" type="scale" begin="-0.266s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="0.8s" repeatCount="indefinite"/></circle></g><g transform="translate(50 50)"> <circle cx="0" cy="0" r="9" fill="inherit" transform="scale(0.00152 0.00152)"><animateTransform attributeName="transform" type="scale" begin="-0.133s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="0.8s" repeatCount="indefinite"/></circle></g><g transform="translate(75 50)"><circle cx="0" cy="0" r="9" fill="inherit" transform="scale(0.299 0.299)"><animateTransform attributeName="transform" type="scale" begin="0s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="0.8s" repeatCount="indefinite"/></circle></g></svg>'
}

function notiflixLoadingSvgPulse(t, e) {
    return t || (t = "60px"), e || (e = "#00b462"), '<svg stroke="' + e + '" width="' + t + '" height="' + t + '" viewBox="0 0 44 44" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd" stroke-width="2"><circle cx="22" cy="22" r="1"><animate attributeName="r" begin="0s" dur="1.8s" values="1; 20" calcMode="spline" keyTimes="0; 1" keySplines="0.165, 0.84, 0.44, 1" repeatCount="indefinite"/><animate attributeName="stroke-opacity" begin="0s" dur="1.8s" values="1; 0" calcMode="spline" keyTimes="0; 1" keySplines="0.3, 0.61, 0.355, 1" repeatCount="indefinite"/></circle><circle cx="22" cy="22" r="1"><animate attributeName="r" begin="-0.9s" dur="1.8s" values="1; 20" calcMode="spline" keyTimes="0; 1" keySplines="0.165, 0.84, 0.44, 1" repeatCount="indefinite"/><animate attributeName="stroke-opacity" begin="-0.9s" dur="1.8s" values="1; 0" calcMode="spline" keyTimes="0; 1" keySplines="0.3, 0.61, 0.355, 1" repeatCount="indefinite"/></circle></g></svg>'
}

function notiflixLoadingSvgNotiflix(t, e, n) {
    return t || (t = "60px"), e || (e = "#f8f8f8"), n || (n = "#00b462"), '<svg id="NXLoadingNotiflixLib" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="' + t + '" height="' + t + '" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 200 200" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><style type="text/css">.line{stroke:' + e + ";stroke-width:12;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:22;}.line{fill:none;}.dot{fill:" + n + ";stroke:" + n + ';stroke-width:12;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:22;}.n{stroke-dasharray: 500;stroke-dashoffset: 0;animation-name: notiflix-n;animation-timing-function: linear;animation-duration: 2.5s;animation-delay:0s;animation-iteration-count: infinite;animation-direction: normal;}@keyframes notiflix-n{0%{stroke-dashoffset: 1000;}100%{stroke-dashoffset: 0;}}.x2,.x1{stroke-dasharray: 500;stroke-dashoffset: 0;animation-name: notiflix-x;animation-timing-function: linear;animation-duration: 2.5s;animation-delay:.2s;animation-iteration-count: infinite;animation-direction: normal;}@keyframes notiflix-x{0%{stroke-dashoffset: 1000;}100%{stroke-dashoffset: 0;}}.dot{animation-name: notiflix-dot;animation-timing-function: ease-in-out;animation-duration: 1.25s;animation-iteration-count: infinite;animation-direction: normal;}@keyframes notiflix-dot{0%{stroke-width: 0;}50%{stroke-width: 12;}100%{stroke-width: 0;}}</style></defs><g><path class="dot" d="M47.97 135.05c3.59,0 6.5,2.91 6.5,6.5 0,3.59 -2.91,6.5 -6.5,6.5 -3.59,0 -6.5,-2.91 -6.5,-6.5 0,-3.59 2.91,-6.5 6.5,-6.5z"/><path class="line n" d="M10.14 144.76l0 -0.22 0 -0.96 0 -56.03c0,-5.68 -4.54,-41.36 37.83,-41.36 42.36,0 37.82,35.68 37.82,41.36l0 57.21"/><path class="line x1" d="M115.06 144.49c24.98,-32.68 49.96,-65.35 74.94,-98.03"/><path class="line x2" d="M114.89 46.6c25.09,32.58 50.19,65.17 75.29,97.75"/></g></svg>'
}