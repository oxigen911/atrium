<?php 

	return[
		'title'=>'Login Client',
		'login'=>'Log in',
		'keep'=>'Remember me',
		'dontaccount'=>'Don\'t have an account?',
		'enter'=>'CLIENT AREA',
		'access'=>'OPEN AN ACCOUNT',
		'client'=>'TO CUSTOMER',
		'email'=>'Email',
		'msj_password'=>'Password (at least 6 characters)',
		'submit'=>'Submit',
		'sign'=>'Enter',
		'forgot'=>'Forgot Password?',
		'required_email'=>'Enter Email',

			];


?>