<?php 

	return[
		'dear'=>'Dear',
		'body_msj'=>'Confirm your e-mail address by clicking on the following link.',
		'confirm'=>'Click Confirm Email',
		'user_approval'=>'User Approval',
		'hey'=>'Hey There,',
		'msj1'=>'Thanks for signing up for Atrium! we are happy that you are with us',
		'msj2'=>'Here you can find a link so you can enter our platform',
		'buy_coins'=>'Coins Purchase',
		'buy_thank'=>'Thanks for using Atrium',
		'buy_msj'=>"invoice is now available. The balance was automatically charged to your credit card, so you don't need to do anything.",
		'your'=>'Your',
		'summary'=>'Summary for',
		'commision'=>'Commision',
		'msj_error_atm_suficiente'=>'Does not have enough ATM to transfer',
		'method_payment'=>'Payment method',
		'total'=>'Total',
		'subject'=>'New Client Confirm'
	];


?>
