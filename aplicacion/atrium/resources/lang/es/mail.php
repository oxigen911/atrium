<?php 

	return[
		'dear'=>'Hola',
		'body_msj'=>'Confirme su dirección de correo electrónico haciendo clic en el siguiente enlace.',
		'confirm'=>'Haga clic en Confirmar correo electrónico',
		'user_approval'=>'Aprobación del usuario',
		'hey'=>'Hola,',
		'msj1'=>'¡Gracias por registrarte en Atrium! estamos felices de que estés con nosotros',
		'msj2'=>'Aquí puedes encontrar un enlace para que puedas ingresar a nuestra plataforma',
		'buy_coins'=>'Compra de monedas',
		'buy_thank'=>'Gracias por usar Atrium',
		'buy_msj'=>"La factura ya está disponible. El saldo se cargó automáticamente a su tarjeta de crédito, por lo que no necesita hacer nada.",
		'your'=>'Tu',
		'summary'=>'Resumen de',
		'msj_error_atm_suficiente'=>'No posee los suficientes ATM para realizar la tranferencia.',
		'commision'=>'Comisión',
		'method_payment'=>'Metodo de pago',
        'total'=>'Total',
        'subject'=>'Confirmar Nuevo Cliente'
	];


?>