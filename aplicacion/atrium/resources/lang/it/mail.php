<?php 

	return[
		'dear'=>'Caro',
		'body_msj'=>'Conferma il tuo indirizzo e-mail facendo clic sul seguente link.',
		'confirm'=>'Fai clic su Conferma email',
		'user_approval'=>"Approvazione dell'utente",
		'hey'=>'Ehi,',
		'msj1'=>'Grazie per esserti registrato ad Atrium! siamo felici che tu sia con noi',
		'msj2'=>'Qui puoi trovare un link in modo da poter accedere alla nostra piattaforma',
		'buy_coins'=>'Acquisto di monete',
		'buy_thank'=>"Grazie per aver usato Atrium",
		'buy_msj'=>"la fattura è ora disponibile. Il saldo è stato addebitato automaticamente sulla tua carta di credito, quindi non devi fare nulla.",
		'your'=>'il tuo',
		'msj_error_atm_suficiente'=>'Non hai abbastanza ATM per il trasferimento.',
		'summary'=>'Riepilogo per',
		'commision'=>'Commissione',
		'total'=>'Totale',
		'method_payment'=>'metodo di pagamento',
		'subject'=>'nuovo client di conferma'

	];


?>
