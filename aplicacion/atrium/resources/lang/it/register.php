<?php 

	return[
		'welcome'=>'Benvenuti in Atrium, iniziamo',
		'success_validation'=>'Ok!',
		'welcome_subt'=>'Completa i seguenti passaggi per creare il tuo account',
		'atrium_code'=>'Promo Code',
		'required_promo_code'=>'Inserisci il tuo codice promo',
		'first_name'=>'Nome',
		'error_first_name'=>'Per favore, inserisci il tuo nome',
		'last_name'=>'Cognome',
		'error_last_name'=>'Perfavore includa il suo cognome.',
		'what_email'=>' la tua mail migliore',
		'required_email'=>'Si prega di includere il proprio indirizzo e-mail.',
		'error_email'=>'Si prega di includere il proprio indirizzo e-mail.',
		'password'=>'Pin accesso',
		'error_password'=>'Si prega di includere la password.',
		'confirm_password'=>'Conferma Pin accesso',
		'error_confirm_password'=>'Conferma la tua password',
		'second_password'=>'Pin dispositivo',
		'required_second_password'=>'Questa password verrà utilizzata per le transazioni',
		'confirm_second_password'=>'Conferma pin dispositivo ',
		'condition_1'=>'Certifico di aver almeno 18 anni',
		'condition_2'=>'Accetto termini e condizioni e la privacy Policy di Atrium ',
		'condition_3'=>'Acconsento a ricevere suggerimenti commerciali',
		'condition_3_1'=>'Accetta i termini e le condizioni',
		'next'=>'Prossimo',
		'previous'=>'precedente',
		'birth_date'=>'Data di nascita',
		'error_birth_date'=>'Si prega di includere la data del compleanno',
		'birth_country'=>'Stato di nascita',
		'error_birth_country'=>'Si prega di includere la nascita del paese',
		'email_not_available'=>'Email non disponibile',
		'email_available'=>'Email disponibile',
		'select_country'=>'Seleziona un Paese',
		'select_city'=>'Seleziona una città',
		'congrulation'=>'Congratulazioni!',
		'your_atrium_id'=>'Il tuo ID Atrium è',
		'msj_number'=>'Conserva questo numero perché sarà il tuo ID personale',
		'msj_confirmation'=>'Fai clic sul link ricevuto nella tua e-mail per confermare la registrazione',
		'city_birth'=>'Città di nascita',
		'error_city_birth'=>'Si prega di includere la città di nascita',
		'current_city'=>'Paese attuale',
		'error_current_city'=>'Includi il Paese corrente',
		'select_type_document'=>'Seleziona il tipo di documento',
		'select_number'=>'Numero del documento',
		'rtrelease_data'=>'Data di rilascio',
		'expiry_data'=>'Data di scadenza',
		'msj_front'=>'Davanti',
		'msj_back'=>'Indietro',
		'complete_later'=>'Completa più tardi',
		'select_method_pay'=>'Seleziona il tuo metodo di pagamento preferito',
		'complete_your_profile'=>'Completa il tuo profilo ora',
		'recommended'=>'Consigliato',
		'msj_credit_card'=>'Ultimamente puoi aggiungere metodi di pagamento alternativi: fino a 5 conti bancari e fino a 5 carte di credito',
		'add_account_bank'=>'Aggiungi conto bancario',
		'prefered'=>'preferito',
		'cancel'=>'Annulla',
		'save'=>'Salvare',
		'credit_card_connect'=>'Carta di credito connessa',

			];


?>
