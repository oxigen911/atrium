<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('atrium/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/css/bootstrap-select.min.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/css/animate.min.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/css/main.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/css/login.css')}}" />

    <title>Atrium</title>
  </head>
  <body id="login">
    <main>
      <section class="content-center my-3">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-6">
              <div class="card w-100 card-login" style="width: 18rem;">
                <button type="button" class="close" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
             
                <div class="card-body">
                  <h4>Login - Admin</h4>
                  <form method="POST" action="{{ route('login') }}" autocomplete="OFF">
                        @csrf
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input id="email" type="email" class="form-control btn-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Enter email" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1"
                        >Password (At least 6 characters)</label
                      >
                      <input id="password" type="password" class="form-control btn-lg{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required 
                        id="exampleInputPassword1" placeholder="Password">
                    </div>

                    <button
                      type="submit"
                      class="btn btn-success btn-lg btn-block mt-5"
                    >
                      Submit
                    </button>
                  </form>
                </div>
                <div class="card-footer-login">
                  <p>Date <span>07.02.2019 14:58</span></p>
                  <p><a href="{{route('register.withOutLogin')}}">Sign</a></p>
                  <p><a href="">Forgot Password?</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>

    <footer>
      <div class="container">
        <div class="row align-items-center">
          <div class="col">Todos los derechos reservados</div>
        </div>
      </div>
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script src="{{asset('atrium/js/bootstrap-material-design.min.js')}}"></script>
    <script src="{{asset('atrium/js/bootstrap-select.min.js')}}"></script>
  </body>
</html>
