<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
   
    <meta name="description" content="" />
	<!-- CSS Files -->
		
	<link rel="stylesheet" href="{{asset('atrium/login/css/reset.css')}}" />
	<link rel="stylesheet" href="{{asset('atrium/login/css/animate.min.css')}}" />
	<link rel="stylesheet" href="{{asset('atrium/login/css/bootstrap.css')}}" />
	<link rel="stylesheet" href="{{asset('atrium/login/css/style.css')}}" />
	<link rel="stylesheet" href="{{asset('atrium/login/css/flexslider.css')}}" />
	<link rel="stylesheet" href="{{asset('atrium/login/css/font-awesome.css')}}" />
	<link rel="stylesheet" href="{{asset('atrium/login/css/owl.carousel.css')}}" />
	<link rel="stylesheet" href="{{asset('atrium/login/css/prettyPhoto.css')}}" />
	<link rel="stylesheet" href="{{asset('atrium/login/css/responsive.css')}}" />
	<link rel="stylesheet" href="{{asset('atrium/login/css/player/YTPlayer.css')}}" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!-- End CSS Files -->
    
    <link rel="stylesheet" href="{{asset('atrium/login/css/login.css')}}">


    <title>Atrium Login</title>
</head>

<body>
    <!-- Navigation Section -->
	<section id="navigation" class="shadow">
	
		<div class="inner navigation">
			
			<!-- Logo Img -->
			<div class="logo">
				<a class="scroll" href="#home"><img src="{{asset('atrium/login/images/logo_black.png')}}" alt="Logo" width="auto" height="43"/></a>
			</div>
			
			
			<!-- Nav Menu -->
			<div class="nav-menu" style="float:left;margin-left: 220px;">			
				<ul class="nav main-nav" >
				
					<li class="active"><a class="scroll" href="#home">home</a></li>
					<li><a class="scroll" href="#features">MOBILE</a></li>
					<li><a class="scroll" href="#services">POINT 2019/2020</a></li>							
				</ul>
			</div>	
			<div class="nav-menu">	
				<ul class="nav main-nav">
                    <li><a style="padding-top: 24px;" href="https://atriumwallet.com/login" target="_blank"><button type="submit" class="subscribe-btn subs">@Lang('login.enter')</button></button></a></li>
					<!--<li><a style="padding-top: 24px;" href="https://atrium/login/wallet.com/sign" target="_blank"><button type="submit" class="subscribe-btn subs">HAZTE CLIENTE</button></a></li>-->					
					<li><a style="padding-top: 24px;" data-animation="flipInY" data-animation-delay="300" data-toggle="modal" data-target="#myModal3"><button type="submit" class="subscribe-btn subs">@Lang('login.access')</button></a></li>	
					<li><a class="scroll" href="#prices">NEWS</a></li>
					<li class="dropdown-toggle nav-toggle"><a class="scroll" href="#">{{App::getLocale()}} <b data-toggle="dropdown" class="caret"></b></a>

						<ul class="dropdown-menu">
                             @foreach(['en','es','it'] as $lan)
							<li><a href="#" class="btn-lang" data-lang="{{$lan}}">{{ $lan }}</a></li>
							@endforeach
						</ul>
					</li>
					<li><a class="scroll" href="#prices">TELEGRAM</a></li>
					
				
				</ul>
				
			</div>
				
			<div style="float: left;margin-left: 120px; margin-top: 10px;" class="visible-xs visible-sm">
				<a style="padding-top: 24px;" href="https://atrium/login/wallet.com/login" target="_blank"><button type="submit" class="subscribe-btn subs" style="width:30%; font-size: 10px; padding: 4px 7px;">ACCESO CLIENTES</button></a>
				<a style="padding-top: 24px;" data-animation="flipInY" data-animation-delay="300" data-toggle="modal" data-target="#myModal4"><button type="submit" class="subscribe-btn subs" style="width:30%; font-size: 10px; padding: 4px 7px;">HAZTE CLIENTE</button></a>
			</div>
			
				<!-- Dropdown Menu For Mobile Devices-->
				<div class="visible-xs visible-sm">
				<div class="dropdown mobile-drop" style="display: contents!important;">
				   <a data-toggle="dropdown" class="mobile-menu" href="#"><i class="fa fa-bars"></i></a>
				  <ul class="nav dropdown-menu fullwidth" role="menu" >
				  <li class="active"><a class="scroll" href="#home">HOME</a></li>
				  <li><a class="scroll" href="#features">MOBILE</a></li>
				  <li><a class="scroll" href="#services">POINT 2019/2020</a></li>
				  <li class="dropdown-toggle mobile-toggle">
                      <a class="scroll" href="#">{{ App::getLocale()}} <b data-toggle="dropdown" class="caret"></b></a>

						<ul class="dropdown-menu dr-mobile">

						<li><a href="index-en.html">EN</a></li>
							<li><a href="indeX.html">IT</a></li>

						</ul>

				  </li>	
				
				  <li><a class="scroll" href="#prices">NEWS</a></li>
					<li><a class="scroll" href="#prices">TELEGRAM</a></li>
				  
				    
					
				  </ul>
				</div>
				</div>
		
			<div class="clear"></div>
		</div>
	
	</section>
    <!-- End Navigation Section -->
    

    <section id="cuerpo">

        <div class="container cont-df">
            
            <div class="row row-df bsp">
                <div class="content">
                <div class="col-md-4 p-3">
                        <div class="logo" style="margin-bottom: 2em">
                                <img src="{{asset('atrium/login/images/logo_black.png')}}" alt="Logo" width="auto" height="70">
                        </div>

                        <div class="title">
                            <h1>@Lang('login.login')</h1>
                            <br>
                        </div>
                        <form method="POST" action="{{ route('client.login.submit') }}" autocomplete="OFF">
                                @csrf
                        <div class="inputs">
                                <div class="group">      
                                    <input type="email" name="email" class="btn-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" required placeholder="@Lang('login.required_email')">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label>@Lang('login.email')</label>
                                </div>
                                <div class="group">      
                                    <input type="password" name="password" required placeholder="@Lang('login.msj_password')" class="btn-lg{{ $errors->has('password') ? ' is-invalid' : '' }}">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label>@Lang('login.msj_password')</label>
                                </div>
                                <p class="text-right"><a href="{{route('password.request')}}">@Lang('login.forgot')</a></p>
                                <input type="checkbox" name="vehicle1" value="Bike"> @Lang('login.keep')<br>

                                <button type="submit" class="btn btn-default btn-lg subscribe-btn btn-log">@Lang('login.submit')</button>
                                <p>@Lang('login.dontaccount') <a href="{{route('register.withOutLogin')}}"> @lang('login.sign')</a></p>
                        </div>
                        </form>

                </div>
                <div class="col-md-8 bg-img hidden-xs hidden-sm">
                    
                </div>
            </div>
        </div>
        </div>

    </section>







    <!-- Footer Section -->
	<section id="footer">
		
	
	
		<div class="inner footer">
			
		
			<!-- Phone -->
			<div class="col-xs-4 footer-box animated" data-animation="flipInY" data-animation-delay="0">
				
      
	<div class="logoFooter"></div>


				<div align="justify">
				<p class="footer-text">
					<span>atrium/login/ Prívate Banker es una marca de HomeChain s.l. Patrimonio digital coin 12.000.000 ATM en wallet (cartera). Fondo prudencial 2.500.00 ATM en Wallet atrium/login/. Provisión cash back (devolución de dinero) &gt; 30%%</span>
				</p>
					</div>
			</div>
			
			<!-- Socials and Mail -->
			<div class="col-xs-4 footer-box animated" data-animation="flipInY" data-animation-delay="0">
				
				<!-- Social 1 -->
				
					<div class="iconFacebook"></div>
				
				
				<!-- Social 2 -->
				<div class="iconInstagram"></div>
				
				<!-- Social 3 -->
				<div class="iconTelegramIta"></div>
				<!-- Social 4 -->
				<div class="iconTelegramEs"></div>
				<!-- Social 5 -->
				<div class="iconTelegramEn"></div>
				<!-- Social 6 
				<div class="iconTelegramRu"></div>-->
					<div align="center">
				<br>
				<p class="footer-text">
					<a href="./documents/Term & Privacy.pdf" target="_blank"> Términos y condiciones <br> info@atrium/login/privatebanker.com </a>
				</p>
					</div>
				
			</div>
			
			<!-- Adress -->
			<div class="col-xs-4 footer-box animated" data-animation="flipInY" data-animation-delay="0">
			
				<!-- Icon -->
				<h3 style="margin-top: 0px;padding-top: 0px;"><b><font color="white">info@atrium/login/privatebanker.com</font></b></h3>
				<div align="justify">
				<br>
				<p class="footer-text">
					<span>Domicilio social: Calle Boreas, 6 - Arona Santa Cruz de Tenerife - Isole Canarie <br> www.atrium/login/privatebanker.com 
					</span>
				</p>
					</div>
				</p>
			</div>
			
			<div class="clear"></div>
		</div> <!-- End Footer inner -->
		
	</section><!-- End Footer Section -->
	

    <!-- JS Files -->
	
	<script type="text/javascript" src="{{asset('atrium/login/js/jquery-1.10.2.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/bootstrap.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/jquery.appear.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/waypoints.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/jquery.prettyPhoto.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/modernizr-latest.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/SmoothScroll.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/jquery.parallax-1.1.3.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/jquery.easing.1.3.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/jquery.superslides.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/jquery.flexslider.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/jquery.sticky.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/owl.carousel.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/jquery.isotope.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/rev-slider/jquery.themepunch.plugins.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('atrium/login/js/rev-slider/jquery.themepunch.revolution.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/jquery.mb.YTPlayer.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/jquery.fitvids.js')}}"></script>
	<script type="text/javascript" src="{{asset('atrium/login/js/plugins.js')}}"></script>
	<script>
		$('.btn-lang').click(function(event){
			event.preventDefault();
			var route = "{{route('locale',':param')}}";
			url =route.replace(':param', $(this).data('lang'));
			window.location.href = url;
		});
	</script>
    @include('error.error')

	 <!-- End JS Files -->
</body>

</html>
