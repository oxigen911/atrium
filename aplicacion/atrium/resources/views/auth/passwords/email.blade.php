<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('atrium/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/css/bootstrap-select.min.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/css/animate.min.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/css/main.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/css/login.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/notiflix/notiflix-1.5.0.min.css')}}"/>
    <title>Atrium</title>
</head>
<body id="login">
<main>
    <section class="content-center my-3">
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    </section>
</main>

<footer>
    <div class="container">
        <div class="row align-items-center">
            <div class="col">Todos los derechos reservados</div>
        </div>
    </div>
</footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script
        src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"
></script>
<script
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"
></script>
<script src="{{asset('atrium/js/bootstrap-material-design.min.js')}}"></script>
<script src="{{asset('atrium/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('atrium/notiflix/notiflix-1.5.0.min.js')}}"></script>
<script src="{{asset('atrium/notiflix/notiflix-globals.js')}}"></script>
@include('error.error')

</body>
</html>

