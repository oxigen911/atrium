@extends('layout.atrium-client')
@section('css-atrium')
    <link rel="stylesheet" href="{{asset('atrium/css/animate.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/main.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/dashboard.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/register-w.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/preload.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/notiflix/notiflix-1.5.0.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/select2.min.css')}}"/>
    
   
@endsection
@section('content-client')
    <div class="container-fluid" style="margin:0px 17%">
            <div class="row no-gutters">

                <div class="col-md-7 col-xl-8 pr-3">
                    <div class="card w-100 card-login" style="width: 18rem;">
                        <form id="storeUser" class="steps needs-validation" accept-charset="UTF-8" enctype="multipart/form-data"
                              novalidate="" autocomplete="off" action="{{route('store.user')}}" method="POST">

                            <div class="card-body">
                                <img src="{{asset('atrium/images/logo-c.png')}}" alt="logo" class="logo-app"/>
                                <div class="info">
                                    <h4>Welcome to Atrium, lets's start</h4>
                                    <p>
                                        Complete the following steps to create your account
                                    </p>
                                </div>


                                {{ csrf_field() }}
                                <ul id="progressbar">
                                    <li></li>
                                    <li></li>
                                    <li class="active"></li>
                                    <li></li>

                                </ul>

                                <!-- 3 FIELD SET -->
                                <fieldset id="page3">


                                    <!-- Begin type-doc -->

                                    <div class="form-row">
                                        <div class="col-12 col-sm-12">
                                            <label for="type-doc">Select type of document <span
                                                        class="ast">*</span></label>
                                            <select id="type-doc" class="form-control"
                                                    title="Choose one of the following..." name="DocumentType"
                                                    value="{{ old('DocumentType') }}">
                                                <option value="null" >Select type of document</option>
                                                <option value="DNI" selected >DNI</option>
                                                <option value="PASSPORT">PASSPORT</option>
                                                <option value="ID">ID</option>
                                            </select>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please include type document
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End current-city -->
                                    <input type="hidden" name="identy" value="{{Auth::user()->id}}">

                                    <!-- Begin number doc -->
                                    <div class="form-row">
                                        <div class="col-12 col-sm-12">
                                            <label for="num-doc2">Document Number</label>
                                            <input id="num-doc2" class="form-control" type="text"
                                                   placeholder="Document number" data-msg-required=""
                                                   name="DocumentIdentification"
                                                   value="{{ old('DocumentIdentification') }}">

                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please include your document number.
                                            </div>
                                        </div>
                                    </div>

                                    <!-- End number doc -->

                                    <!-- Begin release date -->

                                    <div class="form-row">
                                        <div class="col-12 col-sm-12">
                                            <label for="re-date">Release Date <span class="ast">*</span></label>
                                            <input id="re-date" class="form-control" type="date" data-msg-required=""
                                                   name="DocumentIssueDate" value="{{ old('DocumentIssueDate') }}">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please include release date
                                            </div>
                                        </div>
                                    </div>

                                    <!-- End release date -->


                                    <!-- Begin expiry date -->

                                    <div class="form-row">
                                        <div class="col-12 col-sm-12">
                                            <label for="ex-date">Expiry Date <span class="ast">*</span></label>
                                            <input id="ex-date" class="form-control" type="date" placeholder=""
                                                   name="DocumentexpirationDate"
                                                   value="{{ old('DocumentexpirationDate') }}">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please include expiry date
                                            </div>
                                        </div>
                                    </div>

                                    <!-- End expiry date -->

                                    <div class="container mt-4">
                                        <div class="row">
                                            <div class="col-md-6">

                                                <div class="form-group files">
                                                    <input type="file" class="form-control" multiple=""
                                                           name="CertificationResidency">
                                                </div>
                                                <span style="color: #999999; font-style: italic;">Front</span>
                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group files color">
                                                    <input type="file" class="form-control" multiple=""
                                                           name="DocumentImage">
                                                </div>
                                                <span style="color: #999999; font-style: italic;">Back</span>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="button" data-page="3" name="next" style="width: 214px !important" class="next action-button" value="Next"/>

                                </fieldset>


                                <!-- 3 FIELD SET -->
                                <fieldset>
                                    <h6>Select your preferred payment method</h6>
                                    <!-- Begin icons -->
                                    <div class="row align-items-center">

                                        <div class="col">
                                            <p><b>Recommended</b></p>
                                            <button type="button" class="btn btn-light" data-toggle="modal" id="bn"
                                                    data-target="#abc"><img src="atrium/images/icon-blank.svg" alt=""
                                                                            class="img-bank">
                                                +Add bank account
                                            </button>
                                            <p>No commissions</p>
                                        </div>
                                        <div class="col">
                                            <button type="button" class="btn btn-light" data-toggle="modal" data-target="#acc"><img src="atrium/images/icon-blank.svg"
                                                                                             alt=""
                                                                                             class="img-credit-card">
                                                +Add credit card
                                            </button>
                                        </div>
                                    </div>
                                    <!-- End icons-->

                                    <!-- Begin text -->
                                    <div class="row">
                                        <div class="col">
                                            <p>Lately you can add alternative payment methods: Up to 5 bank accounts and
                                                up to 5 credit cards</p>
                                        </div>
                                    </div>

                                    <input type="hidden" name="NumberCard" id="NumberCard">
                                    
                                    <input type="hidden" name="EthereumWallet" >
                                    <input type="hidden" name="IBAN" >
                                    <input type="hidden" name="HeaderAccount">
                                    <input type="hidden" name="BankAccount">
                                    <input type="hidden" name="SwiftCode">
                                    <input type="hidden" name="Prefered">

                                    
                                    <div class="row">
                                          <input type="button" data-page="4" name="previous" class="previous action-button"

                                           value="Previous"/>
                                    <!--input type="button" data-page="4" name="next" class="next action-button" value="Next" /-->
                                    <input id="submit"  class="hs-button primary large action-button next" 
                                           value="Complete" type="submit">
                                        
                                    </div>

                                </fieldset>


                            </div>

                        </form>
                    </div>
                </div>

                <div class="modal fade" id="abc" tabindex="-1" role="dialog" aria-labelledby="abc" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6 class="modal-title" id="abc">Add bank account</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label for="e">Ethereum wallet</label>
                                        <input id="e" class="form-control" required="required" type="text"
                                               placeholder="Document number" data-rule-required="true"
                                               data-msg-required="">

                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include ethereum wallet
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label for="i">IBAN of connected current account</label>
                                        <input class="form-control" required="required" type="text"
                                               placeholder="IBAN of connected current account" data-rule-required="true"
                                               data-msg-required="" name="IBAN" id="i">

                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include credit card connected
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label for="h">Header account</label>
                                        <input class="form-control" required="required" type="text"
                                               placeholder="Header account" data-rule-required="true"
                                               data-msg-required="" name="HeaderAccount" id="h">

                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include credit card connected
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label for="num-doc4">Bank account</label>
                                        <input class="form-control" required="required" type="text"
                                               placeholder="Bank account" data-rule-required="true"
                                               data-msg-required="" name="BankAccount" id="b">

                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include bank account
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label for="num-doc4">SWIFT code</label>
                                        <input class="form-control" required="required" type="text"
                                               placeholder="SWIFT Code" data-rule-required="true"
                                               data-msg-required="" name="SWIFTCode" id="s">

                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include SWIFT code
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="p" name="Prefered"
                                               required="required" >
                                        <label class="form-check-label" for="prefered">
                                            Prefered
                                        </label>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cancel</button>
                                <button type="button" class="btn btn-success" onclick="saveModal();">Save</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!--End Modal add bank -->

                <!-- Modal add credit card -->
                <div class="modal fade" id="acc" tabindex="-1" role="dialog" aria-labelledby="acc" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6 class="modal-title" id="acc">Add credit card</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">


                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label for="num-doc4">Credit card connected</label>
                                        <input class="form-control" required="required" type="text"
                                               placeholder="Document number" data-rule-required="true"
                                               data-msg-required="" name="NumCard" id="numberCard">

                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include credit card connected
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-success" onclick="$('#NumberCard').val($('#num-doc').val())" data-dismiss="modal">Save</button>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
   
@endsection
@section('js-atrium')
<script src="{{asset('atrium/js/jquery-3.3.1.slim.min.js')}}"></script>
<script src="{{asset('atrium/js/popper.min.js')}}"></script>
<script src="{{asset('atrium/js/jquery-2.1.3.min.js')}}"></script>
<script src="{{asset('atrium/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('atrium/js/jquery.validate.js')}}"></script>
<script src="{{asset('atrium/js/bootstrap.min.js')}}"></script>

<script src="{{asset('atrium/js/bootstrap-material-design.min.js')}}"></script>
<script src="{{asset('atrium/js/bootstrap-select.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{asset('atrium/notiflix/notiflix-1.5.0.min.js')}}"></script>
<script src="{{asset('atrium/notiflix/notiflix-globals.js')}}"></script>
@include('error.error')

<script type="text/javascript">

    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

    $(document).ready(function () {

        var current_fs, next_fs, previous_fs;
        var left, opacity, scale;
        var animating;
        $(".steps").validate({
            rules: {
                password: "required",
                password_confirmation: {
                    equalTo: "#password"
                },
                password_2: "required",
                password_confirmation_2: {
                    equalTo: "#password_2"
                }
            },
            messages: {
                password: " Enter Password",
                password_confirmation: "Enter Confirm Password Same as Password",
                password_2: " Enter Password",
                password_confirmation_2: "Enter Confirm Password Same as Password"
            },
            errorClass: 'invalid', errorElement: 'span', errorPlacement: function (error, element) {
                error.insertAfter(element.next('span').children());
                console.log("validate steps");
                cargarSelect();
            }, highlight: function (element) {
                $(element).next('span').show();
                console.log("higlight steps");
            }, unhighlight: function (element) {
                console.log("unhighlighn steps");
                $(element).next('span').hide();
            }
        });
        $(".next").click(function () {
            console.log("entro en next click");
            $(".steps").validate({
                errorClass: 'invalid',
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.insertAfter(element.next('span').children());
                    console.log("steps next error placement");
                },
                highlight: function (element) {
                    console.log("next highlight steps");
                    $(element).next('span').show();
                },
                unhighlight: function (element) {
                    console.log("next steps unhighligh");
                    $(element).next('span').hide();
                }
            });
            if ((!$('.steps').valid())) {
                console.log("steps valid");
                return true;
            }
            if (animating) return false;
            animating = true;
            current_fs = $(this).parent();
            console.log("current_fs: ");
            console.log(current_fs);
            next_fs = $(this).parent().next();
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            next_fs.show();
            current_fs.animate({opacity: 0}, {

                step: function (now, mx) {
                    scale = 1 - (1 - now) * 0.2;
                    left = (now * 50) + "%";
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale(' + scale + ')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                    console.log("fqwfqd658"+now+"mx:"+mx);
                }, duration: 800, complete: function () {
                    current_fs.hide();
                    animating = false;
                }, easing: 'easeInOutExpo'
            });
        });
        $(".submit").click(function () {
            $(".steps").validate({
                errorClass: 'invalid',
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.insertAfter(element.next('span').children());
                    console.log("submit steps error placemment");
                    },
                highlight: function (element) {
                    $(element).next('span').show();
                    console.log("submit steps highlight");

                },
                unhighlight: function (element) {
                    $(element).next('span').hide();
                    console.log("submit steps unhighlight");
                }
            });
            if ((!$('.steps').valid())) {
                return false;
            }
            if (animating) return false;
            animating = true;
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            next_fs.show();
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    scale = 1 - (1 - now) * 0.2;
                    left = (now * 50) + "%";
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale(' + scale + ')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                }, duration: 800, complete: function () {
                    current_fs.hide();
                    animating = false;
                }, easing: 'easeInOutExpo'
            });
        });

        $(".previous").click(function () {
            console.log(animating);
            if (animating) return false;
            animating = true;
            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
            previous_fs.show();
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    scale = 0.8 + (1 - now) * 0.2;
                    left = ((1 - now) * 50) + "%";
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
                }, duration: 800, complete: function () {
                    current_fs.hide();
                    animating = false;
                }, easing: 'easeInOutExpo'
            });
        });
    });
    jQuery(document).ready(function () {
        jQuery("#edit-submitted-acquisition-amount-1,#edit-submitted-acquisition-amount-2,#edit-submitted-cultivation-amount-1,#edit-submitted-cultivation-amount-2,#edit-submitted-cultivation-amount-3,#edit-submitted-cultivation-amount-4,#edit-submitted-retention-amount-1,#edit-submitted-retention-amount-2,#edit-submitted-constituent-base-total-constituents").keyup(function () {
            console.log("line 761");
        });
    });

    var modules = {
        $window: $(window), $html: $('html'), $body: $('body'), $container: $('.container'), init: function () {
            $(function () {
                modules.modals.init();
            });
        }, modals: {
            trigger: $('.explanation'), modal: $('.modal'), scrollTopPosition: null, init: function () {
                var self = this;
                if (self.trigger.length > 0 && self.modal.length > 0) {
                    modules.$body.append('<div class="modal-overlay"></div>');
                    self.triggers();
                }
            }, triggers: function () {
                var self = this;
                self.trigger.on('click', function (e) {
                    e.preventDefault();
                    var $trigger = $(this);
                    self.openModal($trigger, $trigger.data('modalId'));
                });
                $('.modal-overlay').on('click', function (e) {
                    e.preventDefault();
                    self.closeModal();
                });
                modules.$body.on('keydown', function (e) {
                    if (e.keyCode === 27) {
                        self.closeModal();
                    }
                });
                $('.modal-close').on('click', function (e) {
                    e.preventDefault();
                    self.closeModal();
                });
            }, openModal: function (_trigger, _modalId) {
                var self = this, scrollTopPosition = modules.$window.scrollTop(), $targetModal = $('#' + _modalId);
                self.scrollTopPosition = scrollTopPosition;
                modules.$html.addClass('modal-show').attr('data-modal-effect', $targetModal.data('modal-effect'));
                $targetModal.addClass('modal-show');
                modules.$container.scrollTop(scrollTopPosition);
            }, closeModal: function () {
                var self = this;
                $('.modal-show').removeClass('modal-show');
                modules.$html.removeClass('modal-show').removeAttr('data-modal-effect');
                modules.$window.scrollTop(self.scrollTopPosition);
            }
        }
    }
    modules.init();

    function saveModal(){

        $('input[name="EthereumWallet"]').val($('#e').val());
        $('input[name="IBAN"]').val($('#i').val());
        $('input[name="HeaderAccount"]').val($('#h').val());
        $('input[name="BankAccount"]').val($('#b').val());
        $('input[name="SwiftCode"]').val($('#s').val());
        $('input[name="Prefered"]').val($('#p').val());

        $('#abc').modal('hide');
    }

    $('.country').select2({
        placeholder: 'Select an Country',
        ajax: {
            url: "{{route('autocomplete.fetch')}}",
            dataType: 'json',
            delay: 250,
            processResults: function (data) {

                return {
                    results: data
                };
            },
            cache: true
        }
    });
 

    $(window).load(function(){
        $('#enter').fadeOut(200);
    });

    function sendForm(){
        $('#storeUser').submit();
        //$('#enter').fadeIn(200);   
        
    }


 

    function cargarSelect() {
        if($('.country').val()==null){
        $('.country').select2({
            placeholder: 'Select an Country',
            ajax: {
                url: "{{route('autocomplete.fetch')}}",
                dataType: 'json',
                delay: 250,
                processResults: function (data) {

                    return {
                        results: data
                    };
                },
                cache: true
                }
            });
        }
        if($('.countryCurrent').val()==null) {
            $('.countryCurrent').select2({
                placeholder: 'Select an Country',
                ajax: {
                    url: "{{route('autocomplete.fetch')}}",
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {

                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });

        }
    }

    function compl() {
        animating = true;
        current_fs = $('#page2');
        console.log(current_fs);
        next_fs = $('#page3');
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        next_fs.show();
        current_fs.animate({opacity: 0}, {
            step: function (now, mx) {
                scale = 1 - (1 - now) * 0.2;
                left = (now * 50) + "%";
                opacity = 1 - now;
                current_fs.css({'transform': 'scale(' + scale + ')'});
                next_fs.css({'left': left, 'opacity': opacity});
            }, duration: 800, complete: function () {
                current_fs.hide();
                animating = false;
            }, easing: 'easeInOutExpo'
        });
    }

    function move(){
           $('#enter').fadeIn(200);   

    }
</script>
 @endsection