@extends('layout.atrium-client')
@section('css-atrium')
    <link rel="stylesheet" href="{{asset('atrium/css/animate.min.css')}}"/>

    <style>
        #second-section{
            opacity:0;
            transition:opacity 1s linear;
        }

    </style>

@endsection
@section('content-client')
    <div class="container">
        <!--------ROW 2 COLUMNS------------>

        <div class="row justify-content-between pb-3">
            <div class="col-md-6 col-xl-6 pb-3">
                <div class="tab_container">
                    <input id="tab1" type="radio" name="tabs" checked />
                    <label class="label-tab" for="tab1"
                    ><span class="text-danger">Buy</span></label
                    >

                    <input id="tab2" type="radio" name="tabs" />
                    <label class="label-tab" for="tab2"><span>Send</span></label>

                    <section id="content1" class="tab-content">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <h4 class="h-black">Currency</h4>
                                <input type="hidden" name="mon">
                                <input type="hidden" name="val_mon">
                                <input type="hidden" name="coinTwo">
                                <div class="dropdown">
                                    <a
                                            class="btn bw dropdown-toggle dropdown-menu-card container"
                                            href=""
                                            id="dropdownMenuButton1"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                    >
                                        @if(!is_null($coin))
                                            <div class="row align-items-center">
                                                <div class="col-2 p-0">
                                                    <img
                                                            class="img-thumbnail rounded-circle img-account-xs"
                                                            src="{{asset('/storage/'.$coin->logo)}}"
                                                    />
                                                </div>
                                                <div class="col-5 p-0 fs-14">
                                                    <span>{{$coin->name}}</span> <br />
                                                    <span class="blockquote-footer">{{$coin->initial}}</span>
                                                </div>
                                                <div class="col-3 p-0 blockquote-footer p-0 fs-14 price">
                                                    {{'€'. number_format($coin->actual_price,5, ',', '.')}}
                                                </div>
                                                <input type="hidden" name="coin" value="{{$coin->id}}">

                                            </div>
                                        @else
                                            @if(!is_null($coins))
                                                <div class="row align-items-center">
                                                <div class="col-2 p-0">
                                                    <img
                                                            class="img-thumbnail rounded-circle img-account-xs"
                                                            src="{{asset('/storage/'.$coins[0]->logo)}}"
                                                    />
                                                </div>
                                                <div class="col-5 p-0 fs-14">
                                                    <span>{{$coins[0]->name}}</span> <br />
                                                    <span class="blockquote-footer">{{$coins[0]->initial}}</span>
                                                </div>
                                                <div class="col-3 p-0 blockquote-footer p-0 fs-14 price">
                                                    {{'€'. number_format($coins[0]->actual_price,5, ',', '.')}}
                                                </div>

                                                    <input type="hidden" name="coin" value="{{$coins[0]->id}}">

                                                </div>
                                              @else
                                                <div class="row align-items-center">
                                                    <label>Not Coin Available</label>
                                                </div>
                                              @endif
                                        @endif
                                    </a>
                                    <ul
                                            class="dropdown-menu"
                                            aria-labelledby="dropdownMenuButton"
                                            style="width: 100%;"
                                    >
                                        @if(isset($coins) && !is_null($coins))

                                        @forelse($coins as $money)
                                            <li class="dropdown-item container" >
                                                <a
                                                        class="row align-items-center"
                                                        href="#"
                                                        title="Select this card"
                                                        onclick="replace($(this).prop('outerHTML'),1,{{$money->id}});"
                                                >
                                                    <div class="col-2 p-0">
                                                        <img
                                                                class="img-thumbnail rounded-circle img-account-xs"
                                                                src="{{asset('/storage/'.$money->logo)}}"
                                                        />
                                                    </div>
                                                    <div class="col-5 p-0 fs-14">
                                                        <span>{{$money->name}}</span> <br />
                                                        <span class="blockquote-footer">{{$money->initial}}</span>
                                                    </div>
                                                    <div class="col-3 blockquote-footer p-0 fs-14 price">
                                                        {{'€'. number_format($money->actual_price,5, ',', '.')}}
                                                    </div>
                                                </a>
                                            </li>
                                        @empty
                                            <li>Coin Emptity</li>
                                        @endforelse
                                        @else
                                            <li>Not Coin Available</li>
                                        @endif


                                    </ul>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <h4 class="h-black">Payment method</h4>
                                <div class="dropdown">
                                    <a class="btn bw dropdown-toggle dropdown-menu-card container" href="" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true"aria-expanded="false">
                                        <div class="row align-items-center">
                                            <div class="col-2 p-0">
                                                <img
                                                        class="img-thumbnail rounded-circle img-account-xs"
                                                        src="{{asset('atrium/images/cards.ico')}}"
                                                />
                                            </div>
                                            <div class="col-5 p-0 fs-14">
                                                <span>Tranfer Bank</span> <br />
                                                <span class="blockquote-footer">
                                                <span>TR-BANK</span>

                                            </div>

                                        </div>
                                    </a>
                                    <ul
                                            class="dropdown-menu"
                                            aria-labelledby="dropdownMenuButton"
                                            style="width: 100%;"
                                    >
                                    
                                    @if(count(Auth::user()->AccountBanks)>0)
                                        <li class="dropdown-item container">
                                            <a class="row align-items-center" onclick="replace($(this).prop('outerHTML'),2);">
                                                <div class="col-2 p-0">
                                                    <img
                                                            class="img-thumbnail rounded-circle img-account-xs"
                                                            src="{{asset('atrium/images/ccard.svg')}}"
                                                    />
                                                </div>
                                                <div class="col-5 p-0 fs-14">
                                                    <span>Credit Card</span> <br />
                                                    <span class="blockquote-footer">
                                                    
                                                            @foreach(Auth::user()->AccountBanks as $account)
                                                                <span>{{str_pad(substr($account->BankAccount,-3),10,'*',STR_PAD_LEFT)}}</span>
                                                            @endforeach

                                                </div>

                                            </a>

                                        </li>
                                        @endif
                                        @forelse($metodo_pagos as $mpago)
                                        <li class="dropdown-item container">
                                            <a
                                                    class="row align-items-center"
                                                    href="#"
                                                    title="Select this card"
                                                    onclick="replace($(this).prop('outerHTML'),2);"
                                            >
                                                <div class="col-2 p-0">
                                                    <img
                                                            class="img-thumbnail rounded-circle img-account-xs"
                                                            src="{{asset($mpago->logo)}}"
                                                    />
                                                </div>
                                                <div class="col-5 p-0 fs-14">
                                                    <input type="hidden" name="mpago" value="{{$mpago->id}}" id="mPago">
                                                    <span>{{$mpago->name}}</span> <br />
                                                    <span class="blockquote-footer">{{$mpago->initial}}</span>
                                                </div>
                                                
                                                @if($mpago->id==5)
                                                <input type="hidden" name="atmMethod" value="{{is_null($atm->quantity)? 0 :  $atm->quantity}}" id="availableATM">
                                                <div class="col-5 p-0 fs-14">
                                                    <span class="blockquote-footer">Available {{is_null($atm->quantity)? 0 :  $atm->quantity}}</span>
                                                </div>
                                                @endif
                                            </a>
                                        </li>
                                        @empty
                                            <li>Not Register Method Pay</li>
                                        @endforelse
                                       
                                    </ul>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <h4 class="h-black">
                                    Amount
                                </h4>
                                <hr />
                                <div class="container">
                                    <div class="row justify-content-between">
                                        <div class="col-6">
                                            <i class="fa fa-credit-card-alt"></i>
                                            <span class="fs-14">Card limit: monthly</span>
                                        </div>
                                        <div class="col-6 text-right">
                                            <p>€5.000</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="progress">
                                                <div
                                                        class="progress-bar bg-success"
                                                        role="progressbar"
                                                        style="width: 98%"
                                                        aria-valuenow="25"
                                                        aria-valuemin="0"
                                                        aria-valuemax="100"
                                                ></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <input type="number" class="form-control" placeholder="EUR" min="1" max="5000" id="off" />
                            </div>
                            <div class="form-group col-md-2 text-center mt-2">
                                <i class="fa fa-exchange d-none d-sm-block"></i>
                                <span class="d-block d-sm-none"
                                ><i class="fa fa-long-arrow-up"></i
                                    ><i class="fa fa-long-arrow-down"></i
                                    ></span>
                            </div>
                            <div class="form-group col-md-5">
                                <input type="number" class="form-control" placeholder="Coins" id="to" />
                            </div>
                        </div>

                        <button type="button" class="btn btn-success btn-lg btn-block" id="showSecond">

                            @if(isset($coin) && !is_null($coin))
                              {{'Buy '. $coin->name.' '.$coin->initial }}
                            @else
                                @if(isset($coins[0]) && !is_null($coins[0]))
                                    {{'Buy '. $coins[0]->name.' '.$coins[0]->initial}}
                                @else
                                    {{'Not Coin Available'}}
                                @endif

                            @endif
                            <span class="tot"></span>
                        </button>
                    </section>

                    <section id="content2" class="tab-content">
                        <h3>Headline 2</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                            do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo.
                        </p>
                    </section>
                </div>
            </div>
            <div class="col-md-6 col-xl-6 pb-3 animated bounce delay-2s" id="second-section" >
                <div class="card">
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <p class="blockquote-footer fs-12">YOU ARE BUYING</p>
                                    <h3><span id="buy"></span></h3>
                                    <span class="text-danger buye"></span>
                                    <hr />

                                    <div class="col-md-12">
                                        <div class="stepwizard">
                                            <div class="stepwizard-row">
                                                <div class="stepwizard-step">
                                                    <img src="{{asset('atrium/images/ccard.svg')}}" alt="" class="miimg"/>
                                                    <p class="nam">
                                                    </p>
                                                </div>
                                                <div class="stepwizard-step">
                                                    <img src="{{asset('atrium/images/portaf.svg')}}" alt="" />
                                                    <p>Deposit to<br /><span>Portafolio </span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                         <div class="col-md-6">
                                             <span class="float-left detalle" id="detalle1"></span>
                                         </div>

                                        <div class="col-md-6">
                                            <span class="float-right valor" id="detalle2"></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div class="col-md-6">
                                        <span class="float-left detalle">Atrium commissions </span>
                                    </div>
                                        <div class="col-md-6">
                                            <span class="float-right valor">0,00 €</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-6">
                                    <span class="float-left detalle">Total </span>

                                    </div>
                                        <div class="col-md-6">
                                            <span class="float-right valor" id="fin"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-3">
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <div class="mt-3">
                                        <button
                                                type="button"
                                                class="btn btn-success btn-lg btn-block"
                                                onclick="confirm()"
                                        >
                                            Confirm
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--------END ROW 2 COLUMNS------------>

    </div>
    
    <div class="modal fade" id="modalbuy" tabindex="-1" role="dialog" aria-labelledby="acc" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="acc">@Lang('buy.title_confirm')</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('client.paypal')}}" method="POST" id="payForm">
                        <input type="hidden" name="unity">
                        <input type="hidden" name="coin" id="coinCo">
                        <input type="hidden" name="price">
                        <input type="hidden" name="method">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        
                    </form>

                    <div class="form-row">
                        <label>@Lang('buy.confirmation') <span id="msjConfirm"></span></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" onclick="sendF();" class="btn btn-success" >Yes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="showConfirmTranferBank" tabindex="-1" role="dialog" aria-labelledby="acc" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="acc">@Lang('buy.title_confirm')</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('client.paypal')}}" method="POST" id="payForm">
                        <input type="hidden" name="unity">
                        <input type="hidden" name="coin" id="coinCo">
                        <input type="hidden" name="price">
                        <input type="hidden" name="method">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        
                    </form>

                    <div class="form-row">
                        <label>@Lang('buy.confirmation') <span id="msjConfirm"></span></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" onclick="sendF();" class="btn btn-success" >Yes</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js-atrium')
    <script>
    function sendF(){
        var co = 0;
        if($("input[name='coin']").val()=='' || typeof $("input[name='coin']").val()==="undefined" ){
            co = $("input[name='coinTwo']").val();
        }else{
            co =$("input[name='coin']").val();
        }
        $("#coinCo").val(co);
        $("input[name='second_password']").val($('#second_password').val());
        $("input[name='unity']").val($('#to').val());
        $("input[name='price']").val($('#off').val());
        $("input[name='method']").val($('#mPago').val());
        $("#payForm").submit();
    }

    function replace(elem, num,c){
        console.log("replace"+$('#mPago').val()+" : "+$('#off').val())
        if($('#mPago').val()==5 && $('#off').val() > $('#availableATM').val()){
            alertify.error('You do\'nt have enough ATM to make this transfer.');
            $('#showSecond').prop('disabled',true);
            return false;
        }

        var r = elem.replace('onclick=="replace($(this).prop(\'outerHTML\'));"','');
        $('#dropdownMenuButton'+num).html();
        $('#dropdownMenuButton'+num).html(r);

        if(num==1){
            $("input[name='coinTwo']").val(c);
            $('#showSecond').html('Buy '+$('#dropdownMenuButton1 .col-5 span').html()+' '+$.trim($('#dropdownMenuButton1 .blockquote-footer').html()));
        }
        if(num==2){

            $(".miimg").attr('src',$('#dropdownMenuButton2 .col-2 img').attr('src'));
            $(".nam").html($('#dropdownMenuButton2 .col-5 span').html()+'</br><span>'+$.trim($('#dropdownMenuButton2 .blockquote-footer').html()) +'</span>');

        }
    }

    $('input#off').keyup(function(){
        if($(this).val()!=''){
        console.log("input off keyup");
        pri =  $("#dropdownMenuButton1 .row.align-items-center .price").html();
        initial =$("#dropdownMenuButton1 .blockquote-footer").html();
        unit_  = parseFloat($.trim(pri).substring(1,pri.length).replace(',', '.'));
        price = $(this).val();
        price = parseFloat(price);

        if($('#mPago').val()==5 && price > $('#availableATM').val()){
            alertify.error('You do not have enough ATM to make this transfer.');
            $('#showSecond').prop('disabled',true);
            return false;
        }    

        if(price>5000){
            alertify.error('the amount can not be higher than €5000');
            $('#showSecond').prop('disabled',true);
            return false;
        }else{
            $('#showSecond').prop('disabled',false);
        }

        if(isNaN(unit_) || isNaN(price)){
            console.log("entro "+unit_+!isNaN(unit_)+':'+price+!isNaN(price));
            var calc=0;
        }else{
            
            if(unit_>0){
            var calc = (price/unit_);
            }else{
                calc=0;
            }

        }
            
            console.log("calc1:"+calc);
            calc = parseFloat(calc.toFixed(2));
            console.log("calc2:"+calc);
            $("#to").val(calc);
            $(".tot").html(' '+calc.toLocaleString('de-DE'));

            $("#buy").html(calc.toLocaleString('de-DE')+' '+initial);
            $(".buye").html('€'+unit_.toLocaleString('de-DE')+' per '+initial);
            $("#detalle1").html(calc.toLocaleString('de-DE')+' '+initial);
            $("#detalle2").html(price.toLocaleString('de-DE')+' €');

                var commision=0.00000;
                var fi = commision + price;

            $("#fin").html(fi.toLocaleString('de-DE')+' €');

        }
    });

    $('input#to').keyup(function(){
        if($(this).val()!=''){
        console.log("input to keyup");
        pri =  $("#dropdownMenuButton1 .row.align-items-center .price").html();
        initial =$("#dropdownMenuButton1 .blockquote-footer").html();
        unit_ = parseFloat($.trim(pri).substring(1,pri.length).replace(',', '.'));
        qt = parseFloat($(this).val());

        if(isNaN(unit_) || isNaN(qt)){
            console.log("entro"+unit_+':'+qt);
            var calc = 0;

        }else{
            var calc = (unit_*qt);

        }
        if($('#mPago').val()==5 && price > $('#availableATM').val() ){
            alertify.error('You do not have enough ATM to make this transfer.');
        }    
        console.log("calc:"+calc);
        $("#off").val(calc.toFixed(2));
        $(".tot").html(' '+qt.toLocaleString('de-DE'));

        $("#buy").html('€'+qt.toLocaleString('de-DE')+' '+initial);
        $(".buye").html('€'+unit_.toLocaleString('de-DE')+' per '+initial);
        $("#detalle1").html(qt.toLocaleString('de-DE')+' '+initial);
        $("#detalle2").html(calc.toLocaleString('de-DE')+' €');
        var commision=7.90;
        var fi = commision+calc;
        $("#fin").html(fi.toLocaleString('de-DE')+' €');

            }
    });

    $("#showSecond").click(function(){
        val = parseFloat($('input#to').val());
        unit = parseFloat($('input#off').val());
        if( unit>=20 ){
            $(".nam").html($('#dropdownMenuButton2 .col-5 span').html()+'</br><span>'+$.trim($('#dropdownMenuButton2 .blockquote-footer').html()) +'</span>');
            $('#second-section').css('opacity',1);

        }else{
            alertify.error('The minimum value is €20');
            $('#off').focus();
            return false;    
        }

    });

    function confirm() {
        var vl = $('#off').val();
        if(parseFloat(vl)<20){
            alertify.error('The minimum value is €20');
            return false;
        }
        $("#msjConfirm").html($('#buy').html()+' ?');
        $("#modalbuy").modal('show');

    }

    $('#tab2').on('click', function() {
         @if(count(Auth::user()->AccountBanks)==0)
            Notiflix.Confirm.Show('Credit Card',
                'remember that you do not have a registered credit card to make sales, ¿Do you want to register it?',
                'Yes','No',
                function(){
                    location.href = "{{route('client.account')}}";
                }
            );
            /*Notiflix.Report.Warning('Credit Card','remember that you do not have a registered credit card to make sales, please register it in the following link.','<a href="#"> Register Credit Card </a>', function(){
                console.log("uno");
            }, function(){
                console.log("otro");
            });*/
            return false;
         @endif
    });

    $("input#off").bind('keyup mouseup', function () {
         console.log("off");    
    });


</script>
@endsection

