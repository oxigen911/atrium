@extends('layout.atrium-client')
@section('css-atrium')
<style>
    .test{
        border-top: 1px solid #f5f0f0;
        border-bottom: 1px solid #f5f0f0;
        margin: 5px;
        }
    .test3{
        text-align: right;
        font-weight: bold;
        font-size: 16px;
    }
    .test2{
        text-align: center;
        font-weight: bold;
        font-size: 16px;
    }
    .test4{
      font-size:35px;
    }
    #showEye{
      display: none;
    }
</style>
@stop
@section('content-client')

    <div class="container">
        <!--------------ROW GRAPH----------------->
        <div class="row">
          <div class="col-md-12 col-xl-12 p-0 py-3">
            <div class="card">
              <div class="card-body">
                <div class="row">
                   <div class="col-sm-6" style="border-right: 1px solid #e8e8e8;">
                    <div class="col-sm-12 text-center"> 
                      <label id="eye">VALUE OF YOU WALLET <i class="fa fa-eye fa-2x"></i></label>
                    </div>
                      <?php $tot=0; ?>
                          @forelse($movement_coin as $mov)
                          <?php $tot+=$mov->price;?>
                              
                          @empty
                          
                          @endforelse
                    <div class="col-sm-12 text-center" id="showEye" style="border-top: 1px solid #e8e8e8fe;border-bottom: 1px solid #e8e8e8fe;"> 
                      <span class="test4"> {{ number_format($tot, 5, ',', '.') }} € </span> <i class="fa fa-exchange fa-2x"></i> <span class="test4">{{ number_format((1.1131)*$tot, 5, ',', '.')}} $</span>
                    </div>
                   </div>
                   <div class="col-sm-6">
                    <div class="row"> 
                        <div class="col-sm-6"> 
                          <label>From: <select class=""><option>Options</option></select></label>
                          <label>To: <select class=""><option>Options</option></select></label>
                        </div>  
                        <div class="col-sm-6"> 
                          <label>1D 7D 1M 3M 6M ALL</label>
                        </div>
                    </div>
                    <div class="col-sm-12 text-center"> 
                      <h3>%</h3><span>Yield of your wallet</span>
                    </div>
                   </div> 
                </div>
                <img class="" src="atrium/images/graph-Home.png" alt="" style="height: 124px;width: 100%;margin-top: 10px;" />
              </div>
            </div>
          </div>
        </div>
        <!--------------END ROW GRAPH----------------->
        <!--------------ROW TABS----------------->
        <div class="row">
          <div class="col-md-12 col-xl-12 p-0 py-3">
            <div class="card">
              <div class="card-body">
                <div class="cp">
                  <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                      <a
                        class="nav-link active"
                        id="home-tab"
                        data-toggle="tab"
                        href="#new-users"
                        role="tab"
                        aria-controls="home"
                        aria-selected="true"
                        >NEW PROJECT (RCO)   /   SENIOR COMPANY (VAD)</a
                      >
                    </li>
                   
                    <li class="align-a">
                      <div class="form-row">
                        <div class="form-group pr-3">
                          <select id="inputState" class="form-control">
                            <option selected>USD</option>
                            <option>EUR</option>
                          </select>
                        </div>
                        <div class="form-group pr-3 mt-2">
                          <a href="#">View all</a>
                        </div>
                      </div>
                    </li>
                  </ul>

                  <div class="tab-content" id="myTabContent">
                    <div
                      class="tab-pane fade show active"
                      id="new-users"
                      role="tabpanel"
                      aria-labelledby="home-tab"
                    >
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th scope="col" colspan="3">Name</th>
                              <th scope="col">Type</th>
                              <th scope="col">Actual price</th>
                              <th scope="col" colspan="2">Coin sold</th>
                              <th scope="col">Clients</th>
                              <th scope="col">Start</th>
                              <th scope="col">End</th>
                              <th scope="col">Category</th>
                              <th scope="col">Info / whitepaper</th>
                              <!--<th scope="col">Book</th>-->
                              <th scope="col">Buy</th>
                              <th scope="col">Favorites</th>
                            </tr>
                          </thead>
                          <tbody>
			                   @foreach($coins as $k=>$coin)
				                      <tr>	
                              <td scope="row" colspan="3">
                                <a href="{{URL::asset($coin->link)}}">{{$k+1}}<img src="{{asset('/storage/'.$coin->logo)}}" width="20"  alt="" />{{$coin->name}}</a>
                              </td>
                              <td>{{$coin->TypeCoin->name}}</td>
                              <td>{{'$ '.number_format($coin->actual_price,5, ',', '.')}}</td>
                              <td colspan="2">{{$coin->Sold}}</td>
                              <td>{{ $coin->CoinPerson }}</td>
                              <td>{{$coin->data_start->format('d/m/Y')}}</td>
                              <td>{{$coin->data_end->format('d/m/Y')}}</td>
                              <td>{{$coin->category->name}}</td>
                              <td class="text-center">
                                <div class="btn-group">
                                  
                                  
                                  <a href="{{asset('/storage/'.$coin->info)}}" class="btn btn-primary btn-sm" style="display: table-cell"><small>Info</small></a>
                                  <a href="{{asset('/storage/'.$coin->white_paper)}}" class="btn btn-info btn-sm" style="display: table-cell"><small> Whitepaper </small></a>
                                  
                                </div>
                              </td>
                            <!--<td>Book</td>-->
                              <td class="text-center">
                                <div class="btn-group">
                                <a href="{{route('client.buyer',$coin->id)}}" class="btn btn-success" style="font-size:0.8rem">Buy</a>
                                </div>
                              </td>
                              <td class="text-center">
                                <span class="star fa fa-star-o"></span>
                              </td></tr>
                      			       @endforeach
                      				@if(count($coins)==0)
                      				   <tr><td colspan="15" style="text-align:center;"> Not Information Project </td></tr>	
                      				@endif
                          </tbody>
                        </table>
                          {{$coins->links()}}

                       
                      </div>
                    </div>
                    
                    <!--Fin de tab-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-------------END-ROW TABS----------------->

        <!--------ROW 2 COLUMNS------------>
        <div class="row justify-content-between">
          <div class="col-md-6 col-xl-6 pl-0 py-3">
            <div class="card">
              <div class="card-body">
                  <div class="form-row">
                      <div class="form-group col-md-12 test1">
                          <h4 class="h-black">Recent Activity</h4>

                      </div>
                      @forelse($movements as $mov)
                      <div class="form-group col-md-12">
                          <div class="row align-items-center test">
                              <div class="col-2 p-3 test2">{{$mov->date_movement->format('M')}}<br>
                                  <span class="blockquote-footer">{{$mov->date_movement->format('d')}}</span>
                              </div>
                              <div class="col-2 p-3">
                                  <i class="fa fa-refresh fa-2x" style="color:#3975d0;border: 3px solid #2c81d4;padding: 9px;border-radius: 31px;"></i>
                              </div>
                              <div class="col-5 p-3 fs-14">
                                  <span>{{$mov->coin->name}} </span> <br>
                                  <span class="blockquote-footer">{{$mov->coin->initial}}</span>
                                  <span class="blockquote-footer">{{$mov->Stated}}</span>
                              </div>
                              <div class="col-3 p-3 blockquote-footer p-0 fs-14 test3">
                                  <span style="color:#28c428;">+{{$mov->quantity .' '.$mov->coin->initial}}</span><br>
                                  <span class="blockquote-footer" style="color:#aba8a8;">+{{number_format($mov->quantity*$mov->price_actually, 5, ',', '.')}} €</span>

                              </div>
                          </div>
                      </div>
                          @empty
                          <div class="form-group col-md-12">
                              <label>Not Register Information</label>
                          </div>

                          @endforelse
                          {{$movements->links()}}
                  </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-xl-6 pr-0 py-3">
            <div class="card">
              <div class="card-body">
                <div class="form-row">
                      <div class="form-group col-md-12 test1">
                          <h4 class="h-black">Your Portfolio</h4>

                      </div>
                      <?php $tot=0; ?>
                      @forelse($movement_coin as $mov)
                      <div class="form-group col-md-12">
                          <div class="row align-items-center test">
                              <div class="col-2 p-3 test2">
                                  <img src="{{asset('storage/'.$mov->coin->logo)}}" width="40px">
                              </div>
                              <div class="col-5 p-3 fs-14">
                                  <span>{{$mov->coin->name}} {{$mov->coin->initial}}</span><br>
                                  <span> <strong><small>State: {{$mov->Stated}}</small></strong></span>
                              </div>
                              <div class="col-3 p-3 blockquote-footer p-0 fs-14 test3">
                                  <span style="color:#28c428;">{{number_format($mov->quantity, 5, ',', '.')}} {{$mov->coin->initial}}</span><br>
                                  <span class="blockquote-footer" style="color:#aba8a8;">{{number_format($mov->price, 5, ',', '.')}} €</span>
                                  <?php $tot+=$mov->price;?>
                              </div>
                          </div>
                      </div>
                          @empty
                          <div class="form-group col-md-12">
                              <label>Not Register Information</label>
                          </div>

                          @endforelse
                        <div class="form-group col-md-12 test1 text-center">
                          <h4 class="h-black" style="color: #aca9bc !important">Total Balance = {{number_format($tot, 5, ',', '.')}} €</h4>
                        </div>    
                       {{$movement_coin->links()}} 
                  </div>
              </div>
            </div>

            
          </div>
          
        </div>


        <div class="row justify-content-between">
          <div class="col-md-6 col-xl-6 pl-0 py-3">
            <div class="card">
              <div class="card-body">
                  <div class="form-row">
                      <div class="form-group col-md-12 test1">
                          <h4 class="h-black">Pending</h4>

                      </div>
                      @forelse($movement_pendi as $mov)
                      <div class="form-group col-md-12">
                          <div class="row align-items-center test">
                              <div class="col-2 p-3 test2">
                                  <img src="{{asset('storage/'.$mov->coin->logo)}}" width="40px">
                              </div>
                              <div class="col-5 p-3 fs-14">
                                  <span>{{$mov->coin->name}} {{$mov->coin->initial}}</span><br>
                                  <span> <strong><small>State: {{$mov->Stated}}</small></strong></span>
                              </div>
                              <div class="col-3 p-3 blockquote-footer p-0 fs-14 test3">
                                  <span style="color:#28c428;">{{number_format($mov->quantity, 5, ',', '.')}} {{$mov->coin->initial}}</span><br>
                                  <span class="blockquote-footer" style="color:#aba8a8;">{{number_format($mov->price, 5, ',', '.')}} €</span>
                                  <?php $tot+=$mov->price;?>
                              </div>
                          </div>
                      </div>
                          @empty
                          <div class="form-group col-md-12">
                              <label>Not Register Information</label>
                          </div>

                          @endforelse
                        <div class="form-group col-md-12 test1 text-center">
                          <h4 class="h-black" style="color: #aca9bc !important">Total Balance = {{number_format($tot, 5, ',', '.')}} €</h4>
                        </div>    
                          {{$movement_pendi->links()}}
                  </div>
              </div>
            </div>
          </div>
          
          
        </div>


        <!--------END ROW 2 COLUMNS------------>
        
      </div>
@stop

@section('js-atrium')
    @include('error.error')
    <script type="text/javascript">

    $("#eye").click(function(){
      $("#showEye").toggle("slow");
    });

</script>
@endsection