@extends('layout.atrium-client')
@section('css-atrium')
<style>
    .test{
        border-top: 1px solid #f5f0f0;
        border-bottom: 1px solid #f5f0f0;
        margin: 5px;
        }
    .test3{
        text-align: right;
        font-weight: bold;
        font-size: 16px;
    }
    .test2{
        text-align: center;
        font-weight: bold;
        font-size: 16px;
    }
    .test4{
      font-size:35px;
    }
    #showEye{
      display: none;
    }
</style>
@stop
@section('content-client')

    <div class="container">
        <!--------------ROW GRAPH----------------->
        <div class="row">
          <div class="col-md-12 col-xl-12 p-0 py-3">
            <div class="card">
              <div class="card-body">
                    <button class="btn btn-primary" onclick="recoverySecondPassword()">Recover Second Password</button>
                    <button class="btn btn-primary">Change Avatar</button>
                    <button class="btn btn-primary">Change First Password</button>        
              </div>
            </div>
          </div>
        </div>
        
      </div>


      <div class="modal fade" id="secondPassword" tabindex="-1" role="dialog" aria-labelledby="secondPassword" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6 class="modal-title">Reset Second Password</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="{{route('sendRecoverySecondPassword')}}" method="POST">
                            @csrf

                            <div class="modal-body">
                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label >Password</label>
                                        <input class="form-control" required="required" type="password"
                                               placeholder="@Lang('passwords.pass')" data-rule-required="true"
                                               data-msg-required="" name="password">

                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include ethereum wallet
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label >Repeat Password</label>
                                        <input class="form-control" required="required" type="password"
                                               placeholder="@Lang('passwords.confirm_password')" data-rule-required="true"
                                               data-msg-required="" name="confirmation_password">

                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include ethereum wallet
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" >Cancel</button>
                                <button class="btn btn-success" type="submit">Change Password</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>

@stop

@section('js-atrium')
    @include('error.error')
    <script type="text/javascript">

    $("#eye").click(function(){
      $("#showEye").toggle("slow");
    });

    
    function recoverySecondPassword(){
                  Notiflix.Confirm.Show('Recovery Second Passwor',
                 'Do you want to recovey second password?',
                  'Yes','No',
                  function(){
                    $('#secondPassword').modal('show');
 
                  }
                );
              }

</script>
@endsection