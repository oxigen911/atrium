@extends('layout.atrium')
@section('css-atrium')
<link rel="stylesheet" href="{{asset('atrium/DataTables/datatables.css')}}" />
<link rel="stylesheet" href="{{asset('atrium/css/register-w.css')}}" />

@stop
@section('container')
 <div class="col-md-7 col-xl-11 pr-3">
              <div class="card">
                <div class="card-body">
                  <h4>To Approve</h4>
                  <div class="cp">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a
                          class="nav-link active"
                          id="home-tab"
                          data-toggle="tab"
                          href="#new-users"
                          role="tab"
                          aria-controls="home"
                          aria-selected="true"
                          >ICO</a
                        >
                      </li>
                      <li class="nav-item">
                        <a
                          class="nav-link"
                          id="profile-tab"
                          data-toggle="tab"
                          href="#request"
                          role="tab"
                          aria-controls="profile"
                          aria-selected="false"
                          >Add Coin</a
                        >
                      </li>
                         <li class="nav-item">
                        <a
                          class="nav-link"
                          id="edit-tab"
                          data-toggle="tab"
                          href="#edit"
                          role="tab"
                          aria-controls="profile"
                          aria-selected="false"
                          >Edit Coin</a
                        >
                      </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                      <div
                        class="tab-pane fade show active"
                        id="new-users"
                        role="tabpanel"
                        aria-labelledby="home-tab"
                      >

                        <div class="tab-pane active" id="home3" role="tabpanel" aria-expanded="true">
                          <div class="pad">
                            
                        <div class="table-responsive">
                          <table class="table" id="coinTable">
                            <thead>
                              <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Project Value</th>
                                <th scope="col">Coin Price</th>
                                <th scope="col">Actual Price</th>
                                <th scope="col">Coin Sold</th>
                                <th scope="col">Start</th>
                                <th scope="col">End</th>

                                <th scope="col">Cash Back Fund</th>
                              <!--  <th scope="col">Atrium wallet available</th>-->
                                <th scope="col">Category</th>
                                <th scope="col">Options</th>
                              </tr>
                            </thead>
                            <tbody>
                           
                            @foreach($coins as $k=>$coin)
                              <tr>
                                <td><span><img src="{{asset('storage/'.$coin->logo) }}" width="20px" />{{$coin->name}}</span></td>
                                <td><strong>€{{number_format($coin->PprojectVal,5, ',', '.')}}</strong></td>
                                <td>€{{number_format($coin->coin_price,5, ',', '.')}}</td>
                                <td>€{{number_format($coin->actual_price,5, ',', '.')}}</td>
                                <td>€{{number_format($coin->Sold,5,',', '.')}}</td>

                                <td>{{$coin->data_start->format('d/m/Y')}}</td>
                                <td>{{$coin->data_end->format('d/m/Y')}}</td>
                                <td>€{{number_format($coin->CashBack,5, ',', '.')}}</td>
                               <!-- <td>${{number_format($coin->project_value/8,5, ',', '.')}}</td>-->
                                <td>{{$coin->Category->name}}</td>
                                <td><button onclick="edit({{$coin->id}})" class="btn btn-small">Edit</button></td>
                              </tr>
                            @endforeach
                            </tbody>
                          </table>
                          <nav aria-label="Page navigation example">
                               
                          </nav>
                        </div>
                          </div>
                        </div>

                      </div>
                      <div class="tab-pane fade" id="request" role="tabpanel" aria-labelledby="profile-tab">
                         
                          <form class="steps needs-validation" accept-charset="UTF-8" enctype="multipart/form-data" autocomplete="off" action="{{route('store.coin')}}" method="POST" id="coin">

                         {{ csrf_field() }}     
                         <fieldset>
                          

                           <div class="form-row">
                                <div class="form-group col-md-6">
                                <label>Coin Name</label>
                                <input type="text" class="form-control" id="inputNameProject" placeholder="Name of the project"  name="name" value="{{ old('name') }}" required="required" >
                                </div>
                                <div class="form-group col-md-6">
                                <label>Initial</label>
                                <input type="text" class="form-control" id="inputInitial" placeholder="Initial of the coin" name="initial" value="{{ old('initial') }}">
                                </div>
                          </div>
                          <div class="form-group col-md-6">
                            <label>ICO Logo</label>
                            <div class="files">
                                <input type="file" class="form-control" multiple="" name="logo" accept="image/*" >
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Project value</label>
                            <input type="number" class="form-control" readonly id="projectValue" placeholder="1.020.443" name="project_value" value="{{ old('project_value') }}">
                          </div>
                          
			<div class="form-row">
                                <div class="form-group col-md-6">
                                <label>Coin Price</label>
                                <input type="number" class="form-control" id="inputCoinPrice" placeholder="200.000" name="coin_price" value="{{ old('coin_price') }}">
                                </div>
                                <div class="form-group col-md-6">
                                <label>Actual Price</label>
                                <input type="number" class="form-control" id="inputActualPrice" placeholder="123.000" name="actual_price" value="{{ old('actual_price') }}">
                                </div>
                          </div>
                           
			<div class="form-row">
                                <div class="form-group col-md-6">
                                <label>Issued Coins</label>
                                <input type="number" class="form-control" id="inputCoinPrice5" placeholder="200.000" name="issued_coins" value="{{ old('issued_coins') }}">
                                </div>
                                <div class="form-group col-md-6">
                                <label>Cash Back Fund</label>
                                <input type="number" class="form-control" id="inputActualPrice6" placeholder="123.000" name="cash_back_fund" value="{{ old('cash_back_fund') }}">
                                </div>
                          </div>

			  <div class="form-row">
                            <div class="form-group col-md-6 col-lg-4">
                                <label>Data Start</label>
                                <input id="datepicker"  name="data_start" value="{{old('data_start')}}" type="date" />
                            </div>
                            <div class="form-group col-md-6 col-lg-4">
                                    <label>Data Ending</label>
                                    <input id="datepicker1"  name="data_end" value="{{ old('data_end') }}" type="date" />
                            </div>
                            
                          </div>
                          <div class="form-row">
                            <div class="form-group col-md-6">
                            <label>Type</label>
                            <select id="inputType" class="form-control" name="type_id" value="{{ old('type_id') }}">
                                <option selected>Choose type...</option>
                                @foreach($type as $t)
                                  <option value="{{$t->id}}"> {{ $t->name }}</option> 
                                @endforeach
                                
                            </select>
                            </div>
                          </div>
                          <div class="form-row">
                          <div class="form-group col-md-6">
                                <label>Whitepaper</label>
                                <div class="files">
                                    <input type="file" class="form-control" multiple="" name="white_paper"
                                    value="{{ old('white_paper') }}" accept="application/pdf">
                                </div>
                              </div>

                               <div class="form-group col-md-6">
                                <label>Info</label>
                                <div class="files">
                                    <input type="file" class="form-control" multiple="" name="info"
                                    value="{{ old('info') }}" accept="application/pdf">
                                </div>
                              </div>

                           </div>   
                          <div class="form-row">
                            <div class="form-group col-md-6">
                            <label>Category</label>
                            <select id="inputCategory" class="form-control" name="category" value="{{ old('category_id') }}">
                                <option selected>Choose category...</option>
                                @foreach($category as $cat)
                                  <option value="{{$cat->id}}"> {{ $cat->name }}</option> 
                                @endforeach
                                
                            </select>
                            </div>
                            <div class="form-group col-md-6">
                              <label>Link</label>
                              <input type="text" class="form-control" id="inputWebsite" placeholder="Website" name="link" value="{{ old('link') }}">
                            </div>
                          </div>
                          
                          <div class="row">
                                <div class="col-md-12 mt-3">
                                  <p class="text-center">
                                    <a
                                      href="#"
                                      class="btn btn-success btn-lg float-left"
                                      data-toggle="modal"
                                      data-target="#Modal1"
                                      >Save and Add</a
                                    >

                                    <a
                                      href="#"
                                      class="btn btn-danger btn-lg float-right"
                                      data-toggle="modal"
                                      data-target="#Modal1"
                                      >Clean</a
                                    >
                                  </p>
                                </div>
                              </div>
                        
                      </fieldset>
                    </form>

                      </div>
                      <div class="tab-pane fade" id="edit" role="tabpanel" aria-labelledby="edit-tab" >
                          <form class="steps needs-validation" accept-charset="UTF-8" enctype="multipart/form-data" autocomplete="off" action="{{route('update.coin')}}" method="POST" id="coinUpdate">

                         {{ csrf_field() }}
                              <input type="hidden"  name="coin" />
                         <fieldset>
                          

                           <div class="form-row">
                                <div class="form-group col-md-6">
                                <label>Coin Name</label>
                                <input type="text" class="form-control" id="inputNameProject" placeholder="Name of the project"  name="name" required="required" >
                                </div>
                                <div class="form-group col-md-6">
                                <label>Initial</label>
                                <input type="text" class="form-control" id="inputInitial" placeholder="Initial of the coin" name="initial">
                                </div>
                          </div>
                          <div class="form-group col-md-6">
                            <label>ICO Logo</label>
                            <div class="files">
                                <input type="file" class="form-control" multiple="" name="logo" accept="image/*" >
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Project value</label>
                            <input type="number" class="form-control" id="projectValue" placeholder="1.020.443" name="project_value" readonly>
                          </div>
                          <div class="form-row">
                                <div class="form-group col-md-6">
                                <label>Coin Price</label>
                                <input type="number" class="form-control" id="inputCoinPrice" placeholder="200.000" name="coin_price">
                                </div>
                                <div class="form-group col-md-6">
                                <label>Actual Price</label>
                                <input type="number" class="form-control" id="inputActualPrice" placeholder="123.000" name="actual_price">
                                </div>
                          </div>

			 	  
			<div class="form-row">
                                <div class="form-group col-md-6">
                                <label>Issued Coins</label>
                                <input type="number" class="form-control" id="inputCoinPrice5" placeholder="200.000" name="issued_coins">
                                </div>
                                <div class="form-group col-md-6">
                                <label>Cash Back Fund</label>
                                <input type="number" class="form-control" id="inputActualPrice6" placeholder="123.000" name="cash_back_fund">
                                </div>
                          </div>






                          <div class="form-row">
                            <div class="form-group col-md-6 col-lg-4">
                                <label>Data Start</label>
                                <input id="datepicker"  name="data_start" type="date" />
                            </div>
                            <div class="form-group col-md-6 col-lg-4">
                                    <label>Data Ending</label>
                                    <input id="datepicker1"  name="data_end" type="date" />
                                </div>
                          </div>
                          <div class="form-group col-md-6">
                                <label>Whitepaper</label>
                                <div class="files">
                                    <input type="file" class="form-control" multiple="" name="white_paper" accept="application/pdf">
                                </div>
                          </div>
                          <div class="form-group col-md-6">
                                <label>Info</label>
                                <div class="files">
                                    <input type="file" class="form-control" multiple="" name="info" accept="application/pdf">
                                </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group col-md-6">
                            <label>Category</label>
                            <select id="inputCategory" class="form-control" name="category">
                                <option selected>Choose category...</option>
                                 @foreach($category as $cat)
                                  <option value="{{$cat->id}}"> {{ $cat->name }}</option> 
                                @endforeach
                                
                            </select>
                            </div>

                             <div class="form-group col-md-6">
                            <label>Type</label>
                            <select id="inputType" class="form-control" name="type_id">
                                <option selected>Choose Type...</option>
                                 @foreach($type as $t)
                                  <option value="{{$t->id}}"> {{ $t->name }}</option> 
                                @endforeach
                                
                            </select>
                            </div>
                            
                          </div>
                          <div class="row">
                            <div class="form-group col-md-6">
                              <label>Link</label>
                              <input type="text" class="form-control" id="inputWebsite" placeholder="Website" name="link" >
                            </div>
                          </div>
                          <div class="row">
                                <div class="col-md-12 mt-3">
                                  <p class="text-center">
                                    <a
                                      href="#"
                                      class="btn btn-success btn-lg float-left"
                                      data-toggle="modal"
                                      data-target="#ModalEdit"
                                      >Save and Add</a
                                    >

                                    <a
                                      href="#"
                                      class="btn btn-danger btn-lg float-right"
                                      data-toggle="modal"
                                      data-target="#Modal1"
                                      >Clean</a
                                    >
                                  </p>
                                </div>
                              </div>
                        
                      </fieldset>
                    </form>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>



      <!----------Modals------------>
      <div class="modal fade in" id="ModalColaborator" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Modal Colaborator</h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Modal body text goes here.</p>
              <h1 id="modal-colaborator"></h1>
            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" class="btn btn-primary">
                Aprobar
              </button>
            </div>
          </div>
        </div>
      </div>
      <!----------Modals-end----------->

      <!----------Modals------------>
      <div class="modal fade in" id="Modal2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Denegar</h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Desea Rechazar este Cliente?</p>
              <h3 id="modal-decline"></h3>
               <label>Motivo del Rechazo:</label>
               <textarea class="form-control btn-sm" required="required" id="motivoRechazo"></textarea>
                           
              <input type="hidden" name="client_id_deny">

            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" class="btn btn-primary" id="btnDeny">
                Denegar
              </button>
            </div>
          </div>
        </div>
      </div>
      <!----------Modals------------>
      <div class="modal fade in" id="Modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Aprobar</h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Desea aprobar el cliente?.</p>
              <h3 id="modal-approve"></h3>
              <input type="hidden" name="client_id_approved">
            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Cerrar
              </button>
              <button type="button" class="btn btn-primary" id="btnApprove">
                Aprobar
              </button>
            </div>
          </div>
        </div>
      </div>

       <div class="modal fade in" id="Modal1" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Modal title 1</h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Do you want to send this information?</p>
            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" class="btn btn-primary" onclick="$('#coin').submit();">
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>

 <div class="modal fade in" id="ModalEdit" tabindex="-1" role="dialog">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title">Update Coin</h5>
                 <button
                         type="button"
                         class="close"
                         data-dismiss="modal"
                         aria-label="Close"
                 >
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <p>Do you want to send this information?</p>
             </div>
             <div class="modal-footer">
                 <button
                         type="button"
                         class="btn btn-secondary"
                         data-dismiss="modal"
                 >
                     Close
                 </button>
                 <button type="button" class="btn btn-primary" onclick="$('#coinUpdate').submit();">
                     Update changes
                 </button>
             </div>
         </div>
     </div>
 </div>

      <!----------Modals-end----------->
            @endsection	

            @section('js-atrium')
            @include('error.error')

                <script src="{{asset('atrium/DataTables/datatables.js')}}"></script>
             <script type="text/javascript">


              $(document).ready( function () {

                $('#coinTable').DataTable( {
                "ordering": true,
                "bLengthChange": false, 
                "language": {
                      search: "_INPUT_",
                      searchPlaceholder: "Search..."
                  }
              } );


              $('#coinTable_filter input').addClass('form-control btn-sm');
              $('#coinTable_filter').css('text-align', 'right');
              $('#coinTable_paginate').removeClass('dataTables_paginate paging_simple_numbers').addClass('pagination pagination-sm');

              } );


              $("#btnApprove").click(function () {
                $('#btnApprove').attr('disabled','disabled');

                   $.post("{{route('approve')}}", { clientId : $('input[name="client_id_approved"]').val(), _token: '{{csrf_token()}}' }, function(data){
                    $('#btnApprove').attr('disabled',false);
                     $("#modal-approve").append('<br>'+data.msj);
                     if(data.state==200){ 
                      
                       setTimeout(function(){ 
                        $('#Modal1').modal('hide');
                        if ($('.modal-backdrop').is(':visible')) {
                        $('body').removeClass('modal-open'); 
                        $('.modal-backdrop').remove(); 
                           
                          }
                        location.reload(); 
                       }, 1000); 
                      }

                      if(data.state==500){
                        alert(data.msj);
                      }

                    },'json'); 
                });
              $("#btnDeny").click(function () {
                if($('#motivoRechazo').val()==""){
                  alert("debe ingresar motivo de rechazo");
                  $('#motivoRechazo').focus();
                  return false;
                }
                $('#btnDeny').attr('disabled','disabled');

                   $.post("{{route('deny')}}", { clientId : $('input[name="client_id_deny"]').val(),motivoRechazo:$('#motivoRechazo').val(), _token: '{{csrf_token()}}' }, function(  data){
                    $('#btnDeny').attr('disabled',false);
                    $("#modal-decline").append('<br>'+data.msj);
                     if(data.state==200){ 
                      
                       setTimeout(function(){ 
                        $('#Modal2').modal('hide');
                        if ($('.modal-backdrop').is(':visible')) {
                        $('body').removeClass('modal-open'); 
                        $('.modal-backdrop').remove(); 
                           
                          }
                        location.reload(); 
                       }, 1000); 
                      }

                      if(data.state==500){
                        alert(data.msj);
                      }                             
                    },'json'); 
                });

             
              function edit(coin){
                   $.ajax({
            type: "POST",
            url: "{{route('coin.getCoin')}}",
            dataType: "json",
            data: { coin:coin,
                    _token: '{{csrf_token()}}' },
            beforeSend(){
                $('#enter').fadeIn(200);
            },
            success: function (response) {
                if(response.state==500){
                    alertify.alert(response.msj);
                }

                if(response.state==200){
                   $('form#coinUpdate input[name="name"]').val(response.data.name);
                   $('form#coinUpdate input[name="initial"]').val(response.data.initial);
                   $('form#coinUpdate input[name="project_value"]').val(response.data.coin_price*response.data.issued_coins);
                   $('form#coinUpdate input[name="issued_coins"]').val(response.data.issued_coins);
                   $('form#coinUpdate input[name="cash_back_fund"]').val(response.data.cash_back_fund);
		   $('form#coinUpdate input[name="coin_price"]').val(response.data.coin_price);
                   $('form#coinUpdate input[name="actual_price"]').val(response.data.actual_price);
                   $('form#coinUpdate input[name="data_start"]').val(response.data.data_start);
                   $('form#coinUpdate input[name="data_end"]').val(response.data.data_end);
                   $('form#coinUpdate input[name="link"]').val(response.data.link);
                   $('form#coinUpdate select[name="category"]').val(response.data.category_id);
                   $('form#coinUpdate select[name="type_id"]').val(response.data.type_id);
                   $('form#coinUpdate input[name="coin"]').val(coin);

                    $('#edit-tab').click();

                }

                $('#enter').fadeOut(200);

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                $('#enter').fadeOut(200);
            }
        });
              }

              $('#coin #inputCoinPrice').blur(function(){
                $('#coin #projectValue').val($(this).val()*$("#coin #inputCoinPrice5").val());
              });
              $('#coin #inputCoinPrice5').blur(function(){
                $('#coin #projectValue').val($(this).val()*$("#coin #inputCoinPrice").val());
              });

            </script>
            @endsection
