@extends('layout.atrium')
@section('css-atrium')
    <style>
        .disabled{
            color: #b9afaf;
        }
        .disabledbtn , .disabledbtn2{
            background: #b9afaf !important;
            color: white !important;
            border-color: rgba(44,129,212,.25) !important;
        }
    </style>
@endsection

@section('container')
    <div class="col-md-7 col-xl-10 pr-3">
        <div class="card">
        <div class="card-body">
            <div class="cp" style="padding: 24px 29px 33px 43px;">
 
            <div class="row">
                <div class="col-md-1 text-right">
                    <label>Client</label>
                </div>
                <div class="col-md-2 text-left">
                    <label><strong>{{$client->FirstName .' '.$client->LastName}}</strong></label>
                </div>
                <div class="col-md-1 text-right">
                    <label>Email</label>
                </div>
                <div class="col-md-3 text-left">
                    <label><strong>{{$client->Email}}</strong></label>
                </div>
                <div class="col-md-2 text-right">
                    <label>Date Register</label>
                </div>
                <div class="col-md-1 text-left">
                    <label><strong>{{$client->created_at->format('Y.m.d')}}</strong></label>
                </div>
            </div>

            <label>Moviments</label>
               
            <div class="row" style="margin-top:10px;">
               <div class="table-responsive">
               <table class="table">
                 <thead>
                        <th>Coin</th>
                        <th>Quantity</th>
                        <th>Price Seld</th>
                        <th>State</th>
                        <th>Observation</th>
                        <th>Date Movement</th>
                 </thead>
                 <tbody>
                   @if(count($client->Movements)>0)
                    @foreach($client->Movements as $value)
                      <tr>
                        <td>{{$value->coin->name}}</td>
                        <td>{{$value->quantity}}</td>
                        <td>{{$value->price_actually}}</td>
                        <td>{{$value->stated}}</td>
                        <td>{{$value->observation}}</td>
                        <td>{{$value->created_at->format('Y.m.d')}}</td>
                      </tr>
                    @endforeach
                   @else
                    <tr><td colspan="6" class="text-center" >Not Movements Client  </td> </tr>
                   @endif 
                 </tbody>
              </table>
              </div>
            </div>
            </div>
            </div>   
        </div>
        </div>

@endsection         
@section('js-atrium')
    @include('error.error')
<script type="text/javascript">
</script>
@endsection