<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('atrium/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/bootstrap-select.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/animate.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/main.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/dashboard.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/register-w.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/preload.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/notiflix/notiflix-1.5.0.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/css/select2.min.css')}}"/>
    <title>Atrium</title>
</head>
<body id="dashboard">
<div id="enter" style="display: none">
<div id="bitcoin2">
<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 124.69 117.93" style="enable-background:new 0 0 124.69 117.93;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#070308;}
    .st1{fill:#C23029;}
    .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#B1B1B1;}
    .st3{fill-rule:evenodd;clip-rule:evenodd;fill:#C23029;}
</style>
 <filter id="dropshadow2" height="130%">
  <feGaussianBlur in="SourceAlpha" stdDeviation="5"/>
  <feOffset dx="0" dy="0" result="offsetblur"/>
  <feFlood flood-color="red"/>
<feComposite in2="offsetblur" operator="in"/>
<feMerge>
<feMergeNode/>
<feMergeNode in="SourceGraphic"/>
</feMerge>
</filter>    
<g>
    <g>
        <path class="st0 path2"  d="M22.59,98.23c-0.55-0.03-1.05-0.23-1.49-0.59c-0.45-0.36-0.78-0.8-1-1.29c-1.3-3.02-2.54-6.11-3.67-9.17
            C15.29,84.1,14.1,81,12.9,77.94l-0.04-0.07c-0.1-0.1-0.23-0.16-0.4-0.19c-0.2-0.03-0.38,0.06-0.46,0.23
            c-0.25,0.47-0.46,0.97-0.63,1.51c-0.16,0.51-0.34,1.02-0.53,1.51l-2.44,6.28c-0.6,1.54-1.2,3.08-1.81,4.61
            c-0.59,1.47-1.19,2.95-1.79,4.44L4.72,96.4c-0.23,0.48-0.57,0.9-1,1.26c-0.42,0.35-0.9,0.54-1.44,0.57l-0.05,0L1.8,98.52
            l0.23,0.46l0.18,0.12l0.06,0c1.55-0.09,3.1-0.14,4.62-0.16l0.43-0.21v-0.51L7.2,98.17C6.99,98.08,6.79,98,6.6,97.94
            c-0.09-0.03-0.14-0.16-0.14-0.38c0-0.02,0.01-0.12,0.17-0.57c0.12-0.35,0.28-0.77,0.47-1.27c0.19-0.5,0.41-1.03,0.64-1.6
            C7.98,93.56,8.2,93,8.41,92.47c0.21-0.53,0.4-1,0.56-1.4c0.13-0.32,0.23-0.57,0.31-0.75h5.28c0.06,0.15,0.15,0.38,0.27,0.69
            c0.16,0.4,0.35,0.87,0.57,1.41c0.22,0.53,0.45,1.1,0.68,1.69c0.24,0.59,0.45,1.14,0.64,1.64l0.49,1.26
            c0.11,0.29,0.14,0.41,0.16,0.45l0,0l0,0.05c0.01,0.14-0.03,0.25-0.12,0.35c-0.1,0.11-0.22,0.16-0.36,0.18l-0.45,0.25l0.21,0.42
            l0.21,0.18l0.06,0c0.95,0.04,1.9,0.06,2.85,0.08c0.95,0.02,1.9,0.05,2.84,0.1l0.06,0l0.49-0.27l-0.5-0.59L22.59,98.23z
             M11.92,88.5c-0.64,0.01-1.28,0.01-1.93-0.01c0.36-0.85,0.7-1.73,1.02-2.61c0.29-0.81,0.6-1.62,0.92-2.42l1.93,5.02
            C13.21,88.49,12.57,88.49,11.92,88.5z"/>
        <path class="st0 path2" d="M38.6,78.11c-0.04-0.07-0.14-0.16-0.34-0.16c-0.87,0.02-1.75,0.05-2.61,0.09c-0.84,0.04-1.73,0.07-2.62,0.09
            c-3.64,0.09-7.33,0.06-10.94-0.08l-0.03,0l-0.29,0.07l-0.04,0.04c-0.33,0.35-0.66,0.7-1,1.04c-0.33,0.34-0.67,0.69-1.02,1.04
            l-0.05,0.05l-0.05,0.45l0.44,0.26l0.34-0.08l0.04-0.04c0.38-0.36,0.82-0.61,1.31-0.74c0.51-0.14,1.02-0.2,1.52-0.2
            c1.78,0.05,3.58,0.08,5.36,0.08c0.02,2.75,0.03,5.5,0.03,8.24v8.43c0,0.42-0.15,0.78-0.46,1.1c-0.31,0.31-0.66,0.47-1.08,0.47
            l-0.5,0.25l0.08,0.17c0.02,0.03,0.04,0.09,0.07,0.18c0.09,0.25,0.28,0.26,0.32,0.27c0.23,0.02,0.46,0.04,0.7,0.04
            c0.11,0,0.23,0,0.35-0.01c0.33-0.02,0.67-0.02,1,0h4.12l0.39-0.22l-0.05-0.51l-0.36-0.14l-0.04,0c-0.44,0.02-0.81-0.13-1.1-0.45
            c-0.3-0.33-0.46-0.71-0.49-1.15c-0.04-0.87-0.05-1.78-0.05-2.7v-2.73c-0.02-1.9-0.02-3.82-0.01-5.71
            c0.01-1.88,0.03-3.74,0.06-5.55c0.86,0,1.72-0.02,2.56-0.07c0.91-0.04,1.83-0.06,2.72-0.04c0.15,0.02,0.27,0.04,0.35,0.09
            c0.04,0.02,0.08,0.11,0.11,0.23l0.06-0.01l-0.05,0.02c0.06,0.16,0.11,0.33,0.13,0.52c0.03,0.21,0.06,0.4,0.09,0.58l0.02,0.08
            l0.38,0.25l0.09-0.03c0.06-0.02,0.13-0.05,0.22-0.09c0.12-0.05,0.21-0.14,0.27-0.25l0.02-0.08c0.02-0.47,0.04-0.94,0.07-1.41
            c0.03-0.47,0.06-0.94,0.09-1.42l0.01-0.07l-0.04-0.06C38.65,78.19,38.62,78.15,38.6,78.11z"/>
        <path class="st0 path2" d="M62.51,97.5c-0.53-0.41-1.04-0.83-1.51-1.24c-0.47-0.41-0.95-0.86-1.44-1.33c-0.48-0.47-1.01-1.01-1.58-1.62
            c-0.57-0.6-1.22-1.32-1.94-2.14c-0.67-0.76-1.43-1.63-2.3-2.62c0.42-0.14,0.82-0.33,1.19-0.56c0.46-0.29,0.88-0.65,1.24-1.06
            c0.42-0.49,0.74-1.05,0.95-1.66c0.22-0.61,0.33-1.25,0.33-1.88c0-0.76-0.14-1.47-0.42-2.13c-0.28-0.65-0.68-1.23-1.17-1.72
            c-0.5-0.49-1.09-0.88-1.76-1.16c-0.67-0.28-1.38-0.42-2.12-0.42c-1.37-0.02-2.73-0.04-4.08-0.08c-1.36-0.04-2.72-0.08-4.12-0.13
            c-0.09,0.01-0.33,0.04-0.4,0.23c-0.05,0.14-0.06,0.26-0.01,0.35c0.03,0.07,0.1,0.13,0.17,0.15c0.07,0.02,0.14,0.05,0.2,0.08
            l0.08,0.02c0.46,0.02,0.82,0.19,1.12,0.52c0.31,0.35,0.47,0.74,0.5,1.19c0.09,1.3,0.13,2.62,0.14,3.93
            c0.01,1.31,0.01,2.63,0.01,3.94c0,1.4,0,2.8-0.01,4.2c-0.01,1.4-0.06,2.81-0.14,4.19c-0.03,0.45-0.19,0.83-0.48,1.16
            c-0.28,0.32-0.64,0.49-1.1,0.52l-0.51,0.24l0.09,0.17c0.02,0.03,0.04,0.09,0.07,0.16c0.05,0.11,0.12,0.2,0.2,0.25l0.05,0.04
            l0.06,0c1.02-0.05,2.04-0.09,3.03-0.12c1.01-0.03,2.02-0.05,3.03-0.07l0.48-0.22l-0.11-0.48l-0.32-0.14l-0.04,0
            c-0.42,0.02-0.78-0.13-1.05-0.46c-0.29-0.33-0.43-0.7-0.43-1.11c0-0.91,0.01-1.83,0.04-2.73c0.03-0.91,0.05-1.82,0.07-2.74
            c0.02-1.94,0.01-3.88-0.01-5.76c-0.03-1.83-0.06-3.7-0.11-5.56c0.41,0,0.82-0.01,1.24-0.03c0.47-0.02,0.94,0,1.4,0.06
            c0.46,0.06,0.9,0.17,1.31,0.33c0.4,0.16,0.77,0.41,1.1,0.75c0.37,0.38,0.64,0.82,0.81,1.31c0.17,0.49,0.25,0.99,0.22,1.49
            c-0.02,0.5-0.14,0.99-0.35,1.47c-0.21,0.47-0.51,0.88-0.91,1.23c-0.5,0.43-1.01,0.73-1.51,0.87c-0.52,0.15-1.12,0.25-1.75,0.32
            c-0.06,0-0.12,0.01-0.19,0.04c-0.06,0.02-0.12,0.04-0.16,0.06l-0.1,0.05l-0.05,0.46l0.07,0.07c1.37,1.37,2.72,2.74,4.07,4.11
            c1.35,1.38,2.71,2.76,4.07,4.14l0.57,0.56c0.23,0.23,0.46,0.44,0.69,0.64c0.24,0.21,0.49,0.4,0.74,0.58
            c0.26,0.18,0.5,0.32,0.72,0.41c0.5,0.21,1.1,0.39,1.77,0.52c0.68,0.13,1.26,0.27,1.77,0.41l0.15,0.04l0.36-0.68l-0.14-0.1
            C63.66,98.37,63.05,97.91,62.51,97.5z"/>
        <path class="st0 path2" d="M73.16,98.21c-0.41-0.06-0.76-0.26-1.05-0.6c-0.29-0.34-0.44-0.71-0.44-1.12V80.46c0-0.43,0.14-0.8,0.44-1.13
            c0.3-0.34,0.64-0.54,1.06-0.62l0.45-0.32l-0.07-0.14c-0.02-0.04-0.05-0.1-0.1-0.18c-0.07-0.11-0.15-0.18-0.25-0.21l-0.04-0.01
            l-0.05,0.01c-1.86,0.28-3.8,0.27-5.67,0.03l-0.05-0.01l-0.5,0.22l0.41,0.64l0.09,0.01c0.41,0.05,0.75,0.24,1.04,0.59
            c0.3,0.35,0.44,0.72,0.44,1.14v16.03c0,0.41-0.14,0.78-0.44,1.12c-0.3,0.34-0.64,0.54-1.05,0.6l-0.45,0.32l0.07,0.14
            c0.02,0.04,0.05,0.1,0.1,0.17c0.07,0.11,0.15,0.18,0.25,0.21l0.04,0.01l0.04-0.01c1.88-0.26,3.79-0.26,5.67,0l0.06,0.01l0.48-0.27
            l-0.41-0.59L73.16,98.21z"/>
        <path class="st0 path2" d="M98.19,78.54l0.05-0.86l-0.21,0.01c-0.86,0.04-1.71,0.07-2.56,0.09c-0.85,0.03-1.71,0.06-2.56,0.09l-0.05,0
            l-0.4,0.27l0.15,0.49l0.26,0.1l0.04,0c0.22-0.01,0.4,0.02,0.57,0.1c0.18,0.08,0.33,0.2,0.46,0.35c0.13,0.15,0.23,0.32,0.3,0.5
            c0.07,0.19,0.11,0.39,0.11,0.59c0,0.59-0.02,1.19-0.05,1.79c-0.04,0.61-0.06,1.21-0.08,1.8c-0.04,1.1-0.05,2.25-0.05,3.42
            c0,1.17,0.01,2.31,0.03,3.42c0,0.85-0.14,1.65-0.42,2.39c-0.28,0.73-0.68,1.38-1.2,1.93c-0.51,0.55-1.13,0.99-1.84,1.31
            c-0.71,0.32-1.5,0.48-2.36,0.48c-0.86,0-1.66-0.16-2.38-0.48c-0.72-0.32-1.35-0.76-1.88-1.31c-0.53-0.55-0.95-1.2-1.25-1.93
            c-0.3-0.73-0.45-1.54-0.45-2.38c0-1.05,0-2.13,0.01-3.24c0.01-1.11,0-2.19-0.01-3.22c-0.02-0.67-0.04-1.33-0.08-1.98
            c-0.04-0.65-0.04-1.29-0.03-1.93c0.02-0.41,0.16-0.79,0.42-1.12c0.25-0.32,0.59-0.46,1.04-0.45l0.05,0l0.45-0.25l-0.08-0.17
            c-0.02-0.03-0.04-0.09-0.07-0.18c-0.09-0.24-0.26-0.27-0.32-0.27c0,0,0,0,0,0c-0.34-0.04-0.69-0.05-1.04-0.04
            c-0.33,0.01-0.68,0-1.03-0.01l-2.04-0.05c-0.67-0.02-1.35-0.04-2.03-0.08l-0.05,0l-0.46,0.21l0.09,0.18
            c0.02,0.04,0.05,0.1,0.09,0.19c0.09,0.19,0.21,0.26,0.31,0.28c0.49,0.1,0.89,0.28,1.18,0.53c0.28,0.25,0.45,0.63,0.5,1.16
            c0.07,0.78,0.12,1.62,0.14,2.51c0.03,0.89,0.04,1.8,0.04,2.7c0,0.91-0.01,1.81-0.03,2.7c-0.02,0.89-0.03,1.75-0.03,2.55
            c0,1.12,0.19,2.21,0.58,3.23c0.38,1.03,0.98,1.96,1.76,2.76c0.79,0.8,1.72,1.42,2.78,1.82c1.05,0.4,2.14,0.6,3.25,0.6
            c1.12,0,2.22-0.21,3.25-0.62c1.04-0.41,1.96-1.03,2.75-1.83c0.79-0.8,1.39-1.73,1.79-2.76c0.4-1.02,0.6-2.11,0.58-3.24
            c0-0.81,0-1.66-0.01-2.55c-0.01-0.89-0.01-1.79-0.01-2.7c0-0.9,0.02-1.81,0.05-2.69c0.03-0.88,0.09-1.73,0.16-2.53
            c0.03-0.45,0.2-0.83,0.51-1.15c0.31-0.33,0.68-0.5,1.15-0.53L98.19,78.54z"/>
        <path class="st0 path2" d="M122.7,98.24c-0.12-0.05-0.23-0.06-0.32-0.05c-0.01,0-0.05,0-0.14-0.03c-0.33-0.09-0.59-0.27-0.79-0.52
            c-0.21-0.27-0.36-0.58-0.43-0.93c-0.19-0.97-0.34-1.96-0.44-2.95c-0.11-1-0.22-1.99-0.34-2.97c-0.24-2.09-0.45-4.29-0.62-6.53
            c-0.16-2.23-0.29-4.44-0.38-6.56l0-0.08l-0.31-0.31l-0.1,0.01c-0.39,0.05-0.51,0.1-0.57,0.21c-0.45,0.83-0.89,1.7-1.29,2.59
            l-1.21,2.63c-0.42,0.93-0.84,1.89-1.25,2.85c-0.41,0.97-0.82,1.96-1.22,2.96c-0.4,1-0.81,1.99-1.21,2.97
            c-0.35,0.85-0.69,1.67-1.04,2.48c-1.32-2.62-2.63-5.33-3.91-8.07c-1.35-2.89-2.78-5.71-4.26-8.38l-0.05-0.06
            c-0.1-0.07-0.23-0.12-0.41-0.15c-0.25-0.04-0.44,0.08-0.5,0.32c-0.06,0.26-0.08,0.58-0.07,0.94c0.01,0.34,0,0.64-0.04,0.91
            c-0.05,1.22-0.13,2.42-0.24,3.57c-0.12,1.59-0.27,3.29-0.43,5.07c-0.16,1.74-0.34,3.44-0.54,5.03c-0.09,0.59-0.16,1.19-0.22,1.81
            c-0.06,0.59-0.15,1.18-0.27,1.76c-0.08,0.42-0.26,0.76-0.55,1.04c-0.29,0.28-0.62,0.43-1.04,0.46l-0.49,0.23l0.12,0.36
            c0.05,0.14,0.15,0.24,0.28,0.27l0.03,0.01l0.03,0c0.86-0.04,1.71-0.06,2.57-0.08c0.86-0.02,1.72-0.04,2.58-0.08l0.5-0.27l-0.2-0.5
            l-0.25-0.1l-0.04,0c-0.43,0.03-0.76-0.09-1.04-0.38c-0.29-0.29-0.44-0.63-0.47-1.02c0.03-0.05,0.04-0.1,0.04-0.14
            c0.3-1.98,0.55-4.07,0.75-6.21c0.18-1.91,0.35-3.77,0.51-5.57c0.73,1.45,1.46,3,2.18,4.6c0.82,1.83,1.66,3.59,2.5,5.25l0.45,0.89
            c0.19,0.38,0.39,0.79,0.59,1.21c0.2,0.42,0.4,0.82,0.59,1.19c0.19,0.37,0.35,0.64,0.47,0.79c0.11,0.14,0.27,0.2,0.46,0.19
            c0.17-0.01,0.31-0.07,0.41-0.17l0.04-0.06c0.99-2.22,1.98-4.56,2.95-6.96c0.86-2.13,1.75-4.24,2.67-6.27
            c0.1,1.03,0.2,2.11,0.31,3.25c0.14,1.48,0.27,2.78,0.41,3.99c0.09,0.67,0.17,1.32,0.26,1.96c0.09,0.64,0.18,1.29,0.26,1.93v0.13
            c0,0.4-0.14,0.73-0.44,1.01c-0.3,0.28-0.63,0.42-1.01,0.39l-0.06,0l-0.41,0.26l0.09,0.16c0.18,0.33,0.26,0.44,0.41,0.44
            c0.09,0,0.3,0,0.64,0.01c0.34,0.01,0.73,0.02,1.18,0.04c0.45,0.02,0.92,0.04,1.42,0.05c0.5,0.02,0.96,0.03,1.4,0.04
            c0.43,0.01,0.79,0.01,1.07,0.01h0.47l0.05-0.01c0.2-0.06,0.32-0.19,0.35-0.38C123.1,98.6,123.09,98.35,122.7,98.24z"/>
    </g>
    <g>
        <path class="st1 path2" d="M37.35,111.39h4.49v1.07h-5.69v-8.59h5.51v1.06h-4.31v2.57h4.05v1.03h-4.05V111.39z"/>
        <path class="st1 path2" d="M44.78,109.44l-2.1-2.73h1.41l1.41,1.98l1.42-1.98h1.33l-2.07,2.72l2.32,3.03h-1.42l-1.6-2.24l-1.63,2.24
            h-1.37L44.78,109.44z"/>
        <path class="st1 path2" d="M53.48,108.16c-0.12-0.19-0.3-0.34-0.55-0.47c-0.24-0.13-0.5-0.19-0.78-0.19c-0.3,0-0.56,0.06-0.79,0.17
            c-0.23,0.11-0.43,0.26-0.59,0.45c-0.16,0.19-0.28,0.41-0.37,0.66c-0.08,0.25-0.13,0.51-0.13,0.79c0,0.27,0.04,0.54,0.13,0.79
            c0.08,0.25,0.21,0.47,0.37,0.66c0.16,0.19,0.36,0.34,0.59,0.45c0.23,0.11,0.5,0.17,0.8,0.17c0.29,0,0.56-0.06,0.79-0.17
            c0.24-0.11,0.43-0.27,0.59-0.46l0.76,0.69c-0.24,0.28-0.55,0.5-0.91,0.66c-0.36,0.16-0.78,0.24-1.24,0.24
            c-0.44,0-0.85-0.07-1.23-0.22c-0.38-0.15-0.7-0.35-0.98-0.61c-0.27-0.26-0.49-0.58-0.65-0.95c-0.16-0.37-0.24-0.79-0.24-1.25
            c0-0.45,0.08-0.87,0.23-1.24c0.15-0.37,0.37-0.69,0.64-0.96c0.27-0.27,0.6-0.47,0.98-0.62c0.38-0.15,0.78-0.22,1.22-0.22
            c0.4,0,0.81,0.08,1.21,0.24c0.4,0.16,0.72,0.4,0.95,0.7L53.48,108.16z"/>
        <path class="st1 path2" d="M56.63,107.58c0.14-0.28,0.37-0.53,0.7-0.73c0.33-0.2,0.7-0.3,1.12-0.3c0.37,0,0.69,0.06,0.96,0.19
            c0.27,0.13,0.49,0.29,0.67,0.5c0.18,0.21,0.31,0.46,0.39,0.74c0.08,0.28,0.13,0.59,0.13,0.91v3.57h-1.14v-3.19
            c0-0.23-0.02-0.46-0.05-0.67s-0.1-0.4-0.2-0.56c-0.1-0.16-0.23-0.29-0.39-0.39c-0.17-0.1-0.38-0.15-0.65-0.15
            c-0.45,0-0.82,0.17-1.12,0.52c-0.29,0.34-0.44,0.8-0.44,1.38v3.05h-1.14v-9.17h1.14v4.29H56.63z"/>
        <path class="st1 path2" d="M66.1,108.73c0-0.43-0.13-0.75-0.39-0.95c-0.26-0.21-0.6-0.31-1.03-0.31c-0.32,0-0.63,0.06-0.92,0.19
            c-0.29,0.13-0.53,0.29-0.72,0.48l-0.61-0.73c0.28-0.27,0.63-0.48,1.04-0.63c0.41-0.15,0.85-0.23,1.32-0.23
            c0.41,0,0.77,0.06,1.07,0.18c0.3,0.12,0.55,0.28,0.75,0.48c0.19,0.2,0.34,0.44,0.44,0.72c0.1,0.27,0.15,0.57,0.15,0.89v2.51
            c0,0.19,0.01,0.4,0.02,0.61c0.01,0.21,0.03,0.39,0.07,0.53h-1.03c-0.06-0.27-0.1-0.55-0.1-0.82h-0.04
            c-0.21,0.31-0.48,0.55-0.79,0.72c-0.32,0.17-0.7,0.25-1.13,0.25c-0.23,0-0.46-0.03-0.71-0.09c-0.25-0.06-0.47-0.16-0.67-0.3
            c-0.2-0.14-0.37-0.32-0.5-0.55c-0.13-0.23-0.2-0.51-0.2-0.84c0-0.44,0.12-0.78,0.35-1.04c0.23-0.25,0.54-0.45,0.92-0.58
            c0.38-0.13,0.81-0.22,1.29-0.26c0.48-0.04,0.96-0.06,1.44-0.06V108.73z M65.82,109.72c-0.28,0-0.58,0.01-0.88,0.04
            s-0.58,0.07-0.82,0.15c-0.25,0.07-0.45,0.18-0.61,0.33c-0.16,0.15-0.24,0.34-0.24,0.58c0,0.17,0.03,0.31,0.1,0.42
            c0.07,0.11,0.16,0.21,0.27,0.28c0.11,0.07,0.24,0.12,0.38,0.15c0.14,0.03,0.28,0.04,0.42,0.04c0.53,0,0.94-0.16,1.23-0.48
            c0.29-0.32,0.43-0.72,0.43-1.21v-0.3H65.82z"/>
        <path class="st1 path2" d="M70.13,106.71c0.02,0.15,0.03,0.31,0.04,0.49c0.01,0.18,0.01,0.33,0.01,0.45h0.04
            c0.07-0.15,0.17-0.3,0.3-0.43c0.13-0.13,0.27-0.25,0.43-0.35c0.16-0.1,0.34-0.18,0.53-0.24c0.19-0.06,0.4-0.08,0.61-0.08
            c0.37,0,0.69,0.06,0.96,0.19c0.27,0.13,0.49,0.29,0.67,0.5c0.18,0.21,0.31,0.46,0.39,0.74c0.08,0.28,0.13,0.59,0.13,0.91v3.57
            h-1.14v-3.19c0-0.23-0.02-0.46-0.05-0.67c-0.04-0.21-0.1-0.4-0.2-0.56c-0.1-0.16-0.23-0.29-0.4-0.39
            c-0.17-0.1-0.39-0.15-0.65-0.15c-0.45,0-0.82,0.17-1.11,0.52c-0.29,0.34-0.43,0.8-0.43,1.38v3.06H69.1v-4.51
            c0-0.15,0-0.35-0.01-0.59c-0.01-0.24-0.02-0.46-0.04-0.64H70.13z"/>
        <path class="st1 path2" d="M76.6,113.44c0.25,0.28,0.56,0.51,0.94,0.68s0.76,0.25,1.16,0.25c0.38,0,0.7-0.05,0.96-0.16
            c0.26-0.11,0.47-0.26,0.62-0.45c0.16-0.19,0.27-0.41,0.34-0.67c0.07-0.26,0.1-0.54,0.1-0.84v-0.74H80.7
            c-0.21,0.32-0.5,0.57-0.86,0.74c-0.36,0.17-0.75,0.25-1.16,0.25c-0.44,0-0.83-0.08-1.19-0.23c-0.36-0.15-0.67-0.36-0.92-0.63
            c-0.25-0.27-0.44-0.58-0.58-0.94c-0.14-0.36-0.21-0.75-0.21-1.16c0-0.41,0.07-0.8,0.21-1.16c0.14-0.36,0.33-0.68,0.58-0.95
            c0.25-0.27,0.55-0.49,0.91-0.64c0.36-0.16,0.75-0.24,1.19-0.24c0.41,0,0.81,0.09,1.18,0.27c0.38,0.18,0.67,0.45,0.89,0.81h0.02
            v-0.92h1.1v5.46c0,0.42-0.05,0.82-0.16,1.21c-0.11,0.38-0.28,0.72-0.53,1.02c-0.25,0.29-0.58,0.53-0.98,0.71
            c-0.4,0.18-0.91,0.27-1.5,0.27c-0.5,0-1-0.09-1.51-0.27c-0.51-0.18-0.94-0.44-1.3-0.77L76.6,113.44z M76.94,109.52
            c0,0.27,0.04,0.52,0.13,0.76c0.09,0.24,0.21,0.46,0.38,0.64c0.17,0.19,0.37,0.34,0.6,0.45c0.24,0.11,0.5,0.17,0.8,0.17
            c0.29,0,0.56-0.05,0.8-0.16c0.24-0.1,0.45-0.25,0.62-0.43c0.17-0.18,0.3-0.4,0.4-0.64c0.09-0.25,0.14-0.51,0.14-0.79
            c0-0.27-0.05-0.53-0.14-0.78c-0.09-0.24-0.23-0.46-0.4-0.64c-0.17-0.19-0.38-0.33-0.62-0.44c-0.24-0.11-0.5-0.16-0.8-0.16
            c-0.3,0-0.57,0.05-0.8,0.16c-0.24,0.11-0.44,0.25-0.6,0.44c-0.17,0.18-0.29,0.4-0.38,0.64C76.99,108.99,76.94,109.25,76.94,109.52
            z"/>
        <path class="st1 path2" d="M89.19,109.53c0,0.06,0,0.13,0,0.19c0,0.06,0,0.13-0.01,0.19h-4.57c0.01,0.24,0.06,0.47,0.16,0.68
            c0.1,0.21,0.24,0.4,0.41,0.55c0.17,0.16,0.37,0.28,0.59,0.37c0.22,0.09,0.46,0.13,0.71,0.13c0.39,0,0.72-0.08,1.01-0.25
            c0.28-0.17,0.51-0.38,0.67-0.62l0.8,0.64c-0.31,0.41-0.67,0.71-1.09,0.9c-0.42,0.19-0.88,0.29-1.39,0.29
            c-0.44,0-0.84-0.07-1.21-0.22c-0.37-0.15-0.69-0.35-0.96-0.61c-0.27-0.26-0.48-0.58-0.63-0.95c-0.15-0.37-0.23-0.78-0.23-1.24
            c0-0.44,0.07-0.86,0.22-1.23c0.15-0.38,0.36-0.7,0.63-0.96c0.27-0.27,0.58-0.47,0.95-0.62c0.36-0.15,0.76-0.22,1.18-0.22
            c0.42,0,0.8,0.07,1.15,0.21c0.34,0.14,0.64,0.34,0.88,0.59c0.24,0.26,0.42,0.57,0.55,0.94C89.13,108.66,89.19,109.07,89.19,109.53
            z M88.03,109.07c-0.01-0.23-0.05-0.44-0.12-0.64c-0.07-0.2-0.17-0.37-0.3-0.52c-0.13-0.15-0.3-0.26-0.5-0.35
            c-0.2-0.08-0.43-0.13-0.7-0.13c-0.24,0-0.47,0.04-0.69,0.13c-0.21,0.08-0.4,0.2-0.56,0.35c-0.16,0.15-0.29,0.32-0.39,0.52
            c-0.1,0.2-0.16,0.41-0.18,0.64H88.03z"/>
    </g>
    <g>
        <g id="XMLID_738_">
            <path id="XMLID_744_" class="st2 path2" d="M64.03,15.5V4.92h12.37l-6.94,12.02C67.77,16.18,65.95,15.68,64.03,15.5z"/>
            <path id="XMLID_743_" class="st2 path2" d="M77.74,25.23l9.15-5.28l6.18,10.71H79.19C79.01,28.74,78.51,26.91,77.74,25.23z"/>
            <path id="XMLID_742_" class="st2 path2" d="M76.16,41.98l9.15,5.29l-6.18,10.71l-6.94-12.02C73.72,44.86,75.06,43.52,76.16,41.98z"/>
            <path id="XMLID_741_" class="st2 path2" d="M60.86,48.98v10.57H48.5l6.94-12.02C57.12,48.3,58.94,48.8,60.86,48.98z"/>
            <path id="XMLID_740_" class="st2 path2" d="M47.15,39.24L38,44.53l-6.18-10.71H45.7C45.88,35.74,46.38,37.57,47.15,39.24z"/>
            <path id="XMLID_739_" class="st2 path2" d="M48.73,22.5l-9.15-5.29l6.18-10.71l6.94,12.02C51.17,19.62,49.83,20.96,48.73,22.5z"/>
        </g>
        <path id="XMLID_737_" class="st3 path2"  d="M72.19,18.53l5.29-9.16l10.71,6.18L76.16,22.5C75.09,20.99,73.76,19.64,72.19,18.53z"/>
        <path id="XMLID_736_" class="st3 path2" d="M79.19,33.82h10.57v12.37l-12.02-6.94C78.54,37.49,79.02,35.66,79.19,33.82z"/>
        <path id="XMLID_735_" class="st3 path2" d="M69.45,47.53l5.29,9.15l-10.71,6.18V48.98C65.9,48.81,67.74,48.31,69.45,47.53z"/>
        <path id="XMLID_734_" class="st3 path2" d="M52.7,45.95l-5.29,9.16l-10.71-6.18l12.02-6.94C49.8,43.49,51.13,44.83,52.7,45.95z"/>
        <path id="XMLID_733_" class="st3 path2" d="M45.7,30.66H35.13V18.29l12.02,6.94C46.35,26.99,45.87,28.82,45.7,30.66z"/>
        <path id="XMLID_732_" class="st3 path2" d="M55.44,16.95l-5.29-9.15l10.71-6.18V15.5C58.99,15.67,57.15,16.17,55.44,16.95z"/>
    </g>
</g>
</svg>
</div>
</div>
<main>
    <header>
        <div class="container-fluid">
            <div class="row no-gutters align-items-center">

            </div>
        </div>
    </header>

    <section class="panel">
        <div class="container-fluid" style="margin:0px 17%">
            <div class="row no-gutters">

                <div class="col-md-7 col-xl-8 pr-3">
                    <div class="card w-100 card-login" style="width: 18rem;">
                        <button type="button" class="close" aria-label="Close">
                            <a href="{{route('login')}}"><span aria-hidden="true">&times;</span></a>
                        </button>
                        <form id="storeUser" class="steps needs-validation" accept-charset="UTF-8" enctype="multipart/form-data"
                              novalidate="" autocomplete="off" action="{{route('store.user')}}" method="POST">

                            <div class="card-body">
                                <img src="{{asset('atrium/images/logo-c.png')}}" alt="logo" class="logo-app"/>
                                <div class="info">
                                    <h4>@Lang('register.welcome')</h4>
                                    <p>
                                        @Lang('register.welcome_subt')
                                    </p>
                                </div>


                                {{ csrf_field() }}
                                <ul id="progressbar">
                                    <li></li>
                                    <li></li>
                                    <li class="active"></li>
                                    <li></li>

                                </ul>

                                <!-- 3 FIELD SET -->
                                <fieldset id="page3">


                                    <!-- Begin type-doc -->

                                    <div class="form-row">
                                        <div class="col-12 col-sm-12">
                                            <label for="type-doc">@Lang('register.select_type_document')<span
                                                        class="ast">*</span></label>
                                            <select id="type-doc" class="form-control"
                                                    title="Choose one of the following..." name="DocumentType"
                                                    value="{{ old('DocumentType') }}">
                                                <option value="null" selected>@Lang('register.select_type_document')</option>
                                                <option value="DNI">DNI</option>
                                                <option value="PASSPORT">PASSPORT</option>
                                                <option value="ID">ID</option>
                                            </select>
                                            <div class="valid-feedback">
                                                @Lang('register.success_validation')
                                            </div>
                                            <div class="invalid-feedback">
                                                Please include type document
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End current-city -->
<input type="hidden" name="identy" value="{{$client->id}}">

                                    <!-- Begin number doc -->
                                    <div class="form-row">
                                        <div class="col-12 col-sm-12">
                                            <label for="num-doc2">@Lang('register.select_number')</label>
                                            <input id="num-doc2" class="form-control" type="text"
                                                   placeholder="@Lang('register.select_number')" data-msg-required=""
                                                   name="DocumentIdentification"
                                                   value="{{ old('DocumentIdentification') }}">

                                            <div class="valid-feedback">
                                                @Lang('register.success_validation')
                                            </div>
                                            <div class="invalid-feedback">
                                                Please include your document number.
                                            </div>
                                        </div>
                                    </div>

                                    <!-- End number doc -->

                                    <!-- Begin release date -->

                                    <div class="form-row">
                                        <div class="col-12 col-sm-12">
                                            <label for="re-date">@Lang('register.release_data')<span class="ast">*</span></label>
                                            <input id="re-date" class="form-control" type="date" data-msg-required=""
                                                   name="DocumentIssueDate" value="{{ old('DocumentIssueDate') }}">
                                            <div class="valid-feedback">
                                                @Lang('register.success_validation')
                                            </div>
                                            <div class="invalid-feedback">
                                                Please include release date
                                            </div>
                                        </div>
                                    </div>

                                    <!-- End release date -->


                                    <!-- Begin expiry date -->

                                    <div class="form-row">
                                        <div class="col-12 col-sm-12">
                                            <label for="ex-date">@Lang('register.expiry_data') <span class="ast">*</span></label>
                                            <input id="ex-date" class="form-control" type="date" placeholder=""
                                                   name="DocumentexpirationDate"
                                                   value="{{ old('DocumentexpirationDate') }}">
                                            <div class="valid-feedback">
                                                @Lang('register.success_validation')
                                            </div>
                                            <div class="invalid-feedback">
                                                Please include expiry date
                                            </div>
                                        </div>
                                    </div>

                                    <!-- End expiry date -->

                                    <div class="container mt-4">
                                        <div class="row">
                                            <div class="col-md-6">

                                                <div class="form-group files">
                                                    <input type="file" class="form-control" multiple=""
                                                           name="CertificationResidency">
                                                </div>
                                                <span style="color: #999999; font-style: italic;">@Lang('register.msj_front')</span>
                                            </div>

                                            <div class="col-md-6">

                                                <div class="form-group files color">
                                                    <input type="file" class="form-control" multiple=""
                                                           name="DocumentImage">
                                                </div>
                                                <span style="color: #999999; font-style: italic;">@Lang('register.msj_back')</span>
                                            </div>
                                        </div>
                                    </div>


                                    <a href="{{route('confirm.later', $client->ConfirmationCode)}}"  style="width: 153px !important;background: #f61a16;font-weight: bold;color: white;border: 0 none;border-radius: 3rem;cursor: pointer;float:left;padding: 10px 5px;margin: 25px auto;-webkit-transition: all 0.3s linear 0s;-moz-transition: all 0.3s linear 0s;-ms-transition: all 0.3s linear 0s;-o-transition: all 0.3s linear 0s;transition: all 0.3s linear 0s;display: block;text-decoration:none;" class="previous action-button">@Lang('register.complete_later')</a>
                                    <input type="button" data-page="3" name="next" style="width: 214px !important" class="next action-button"
                                           value="@Lang('register.complete_your_profile')"/>

                                </fieldset>


                                <!-- 3 FIELD SET -->
                                <fieldset>
                                    <h6>@Lang('register.select_method_pay')</h6>
                                    <!-- Begin icons -->
                                    <div class="row align-items-center">

                                        <div class="col">
                                            <p><b>@Lang('register.recommended')</b></p>
                                            <button type="button" class="btn btn-light" data-toggle="modal" id="bn"
                                                    data-target="#abc"><img src="atrium/images/icon-blank.svg" alt=""
                                                                            class="img-bank">
                                                +Add bank account
                                            </button>
                                            <p>No commissions</p>
                                        </div>
                                        <div class="col">
                                            <button type="button" class="btn btn-light" data-toggle="modal" data-target="#acc"><img src="atrium/images/icon-blank.svg"
                                                                                             alt=""
                                                                                             class="img-credit-card">
                                                +Add credit card
                                            </button>
                                        </div>
                                    </div>
                                    <!-- End icons-->

                                    <!-- Begin text -->
                                    <div class="row">
                                        <div class="col">
                                            <p>@Lang('register.msj_credit_card')</p>
                                        </div>
                                    </div>

                                    <!-- End text-->
<input type="hidden" name="NumberCard" id="NumberCard">
									
                                    <input type="hidden" name="EthereumWallet" >
                                    <input type="hidden" name="IBAN" >
                                    <input type="hidden" name="HeaderAccount">
                                    <input type="hidden" name="BankAccount">
                                    <input type="hidden" name="SwiftCode">
                                    <input type="hidden" name="Prefered">

                                    <input type="button" data-page="4" name="previous" class="previous action-button"
                                           value="@Lang('register.previous')"/>
                                    <!--input type="button" data-page="4" name="next" class="next action-button" value="Next" /-->
                                    <input id="submit"  class="hs-button primary large action-button next" 
                                           value="Complete" type="submit">


                                </fieldset>


                            </div>

                        </form>
                    </div>
                </div>

                <div class="modal fade" id="abc" tabindex="-1" role="dialog" aria-labelledby="abc" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6 class="modal-title" id="abc">@Lang('register.add_account_bank')</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label for="e">Ethereum wallet</label>
                                        <input id="e" class="form-control" required="required" type="text"
                                               placeholder="@Lang('register.select_number')" data-rule-required="true"
                                               data-msg-required="">

                                        <div class="valid-feedback">
                                            @Lang('register.success_validation')
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include ethereum wallet
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label for="i">IBAN of connected current account</label>
                                        <input class="form-control" required="required" type="text"
                                               placeholder="IBAN of connected current account" data-rule-required="true"
                                               data-msg-required="" name="IBAN" id="i">

                                        <div class="valid-feedback">
                                            @Lang('register.success_validation')
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include credit card connected
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label for="h">Header account</label>
                                        <input class="form-control" required="required" type="text"
                                               placeholder="Header account" data-rule-required="true"
                                               data-msg-required="" name="HeaderAccount" id="h">

                                        <div class="valid-feedback">
                                            @Lang('register.success_validation')
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include credit card connected
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label for="num-doc4">Bank account</label>
                                        <input class="form-control" required="required" type="text"
                                               placeholder="Bank account" data-rule-required="true"
                                               data-msg-required="" name="BankAccount" id="b">

                                        <div class="valid-feedback">
                                            @Lang('register.success_validation')
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include bank account
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label for="num-doc4">SWIFT code</label>
                                        <input class="form-control" required="required" type="text"
                                               placeholder="SWIFT Code" data-rule-required="true"
                                               data-msg-required="" name="SWIFTCode" id="s">

                                        <div class="valid-feedback">
                                            @Lang('register.success_validation')
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include SWIFT code
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="p" name="Prefered"
                                               required="required" >
                                        <label class="form-check-label" for="prefered">
                                            @Lang('register.prefered')
                                        </label>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" >@Lang('register.cancel')</button>
                                <button type="button" class="btn btn-success" onclick="saveModal();">@Lang('register.save')</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!--End Modal add bank -->

                <!-- Modal add credit card -->
                <div class="modal fade" id="acc" tabindex="-1" role="dialog" aria-labelledby="acc" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6 class="modal-title" id="acc">@Lang('register.add_account_bank')</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">


                                <div class="form-row">
                                    <div class="col-12 col-sm-12">
                                        <label for="num-doc4">@Lang('register.credit_card_connect')</label>
                                        <input class="form-control" required="required" type="text"
                                               placeholder="Document number" data-rule-required="true"
                                               data-msg-required="" name="NumCard" id="numberCard">

                                        <div class="valid-feedback">
                                            @Lang('register.success_validation')
                                        </div>
                                        <div class="invalid-feedback">
                                            Please include credit card connected
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('register.cancel')</button>
                                <button type="button" class="btn btn-success" onclick="$('#NumberCard').val($('#num-doc').val())" data-dismiss="modal">@Lang('register.save')</button>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </section>

    
</main>

<footer>
    <div class="container">
        <div class="row align-items-center">
           
        </div>
    </div>
</footer>

<script src="{{asset('atrium/js/jquery-3.3.1.slim.min.js')}}"></script>
<script src="{{asset('atrium/js/popper.min.js')}}"></script>
<script src="{{asset('atrium/js/jquery-2.1.3.min.js')}}"></script>
<script src="{{asset('atrium/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('atrium/js/jquery.validate.js')}}"></script>
<script src="{{asset('atrium/js/bootstrap.min.js')}}"></script>

<script src="{{asset('atrium/js/bootstrap-material-design.min.js')}}"></script>
<script src="{{asset('atrium/js/bootstrap-select.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{asset('atrium/notiflix/notiflix-1.5.0.min.js')}}"></script>
<script src="{{asset('atrium/notiflix/notiflix-globals.js')}}"></script>
@include('error.error')

<script type="text/javascript">

    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

    $(document).ready(function () {

        var current_fs, next_fs, previous_fs;
        var left, opacity, scale;
        var animating;
        $(".steps").validate({
            rules: {
                password: "required",
                password_confirmation: {
                    equalTo: "#password"
                },
                password_2: "required",
                password_confirmation_2: {
                    equalTo: "#password_2"
                }
            },
            messages: {
                password: " Enter Password",
                password_confirmation: "Enter Confirm Password Same as Password",
                password_2: " Enter Password",
                password_confirmation_2: "Enter Confirm Password Same as Password"
            },
            errorClass: 'invalid', errorElement: 'span', errorPlacement: function (error, element) {
                error.insertAfter(element.next('span').children());
                console.log("validate steps");
                cargarSelect();
            }, highlight: function (element) {
                $(element).next('span').show();
                console.log("higlight steps");
            }, unhighlight: function (element) {
                console.log("unhighlighn steps");
                $(element).next('span').hide();
            }
        });
        $(".next").click(function () {
            console.log("entro en next click");
            $(".steps").validate({
                errorClass: 'invalid',
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.insertAfter(element.next('span').children());
                    console.log("steps next error placement");
                },
                highlight: function (element) {
                    console.log("next highlight steps");
                    $(element).next('span').show();
                },
                unhighlight: function (element) {
                    console.log("next steps unhighligh");
                    $(element).next('span').hide();
                }
            });
            if ((!$('.steps').valid())) {
                console.log("steps valid");
                return true;
            }
            if (animating) return false;
            animating = true;
            current_fs = $(this).parent();
            console.log("current_fs: ");
            console.log(current_fs);
            next_fs = $(this).parent().next();
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            next_fs.show();
            current_fs.animate({opacity: 0}, {

                step: function (now, mx) {
                    scale = 1 - (1 - now) * 0.2;
                    left = (now * 50) + "%";
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale(' + scale + ')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                    console.log("fqwfqd658"+now+"mx:"+mx);
                }, duration: 800, complete: function () {
                    current_fs.hide();
                    animating = false;
                }, easing: 'easeInOutExpo'
            });
        });
        $(".submit").click(function () {
            $(".steps").validate({
                errorClass: 'invalid',
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.insertAfter(element.next('span').children());
                    console.log("submit steps error placemment");
                    },
                highlight: function (element) {
                    $(element).next('span').show();
                    console.log("submit steps highlight");

                },
                unhighlight: function (element) {
                    $(element).next('span').hide();
                    console.log("submit steps unhighlight");
                }
            });
            if ((!$('.steps').valid())) {
                return false;
            }
            if (animating) return false;
            animating = true;
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            next_fs.show();
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    scale = 1 - (1 - now) * 0.2;
                    left = (now * 50) + "%";
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale(' + scale + ')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                }, duration: 800, complete: function () {
                    current_fs.hide();
                    animating = false;
                }, easing: 'easeInOutExpo'
            });
        });

        $(".previous").click(function () {
            if (animating) return false;
            animating = true;
            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
            previous_fs.show();
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    scale = 0.8 + (1 - now) * 0.2;
                    left = ((1 - now) * 50) + "%";
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
                }, duration: 800, complete: function () {
                    current_fs.hide();
                    animating = false;
                }, easing: 'easeInOutExpo'
            });
        });
    });
    jQuery(document).ready(function () {
        jQuery("#edit-submitted-acquisition-amount-1,#edit-submitted-acquisition-amount-2,#edit-submitted-cultivation-amount-1,#edit-submitted-cultivation-amount-2,#edit-submitted-cultivation-amount-3,#edit-submitted-cultivation-amount-4,#edit-submitted-retention-amount-1,#edit-submitted-retention-amount-2,#edit-submitted-constituent-base-total-constituents").keyup(function () {
            console.log("line 761");
        });
    });

    var modules = {
        $window: $(window), $html: $('html'), $body: $('body'), $container: $('.container'), init: function () {
            $(function () {
                modules.modals.init();
            });
        }, modals: {
            trigger: $('.explanation'), modal: $('.modal'), scrollTopPosition: null, init: function () {
                var self = this;
                if (self.trigger.length > 0 && self.modal.length > 0) {
                    modules.$body.append('<div class="modal-overlay"></div>');
                    self.triggers();
                }
            }, triggers: function () {
                var self = this;
                self.trigger.on('click', function (e) {
                    e.preventDefault();
                    var $trigger = $(this);
                    self.openModal($trigger, $trigger.data('modalId'));
                });
                $('.modal-overlay').on('click', function (e) {
                    e.preventDefault();
                    self.closeModal();
                });
                modules.$body.on('keydown', function (e) {
                    if (e.keyCode === 27) {
                        self.closeModal();
                    }
                });
                $('.modal-close').on('click', function (e) {
                    e.preventDefault();
                    self.closeModal();
                });
            }, openModal: function (_trigger, _modalId) {
                var self = this, scrollTopPosition = modules.$window.scrollTop(), $targetModal = $('#' + _modalId);
                self.scrollTopPosition = scrollTopPosition;
                modules.$html.addClass('modal-show').attr('data-modal-effect', $targetModal.data('modal-effect'));
                $targetModal.addClass('modal-show');
                modules.$container.scrollTop(scrollTopPosition);
            }, closeModal: function () {
                var self = this;
                $('.modal-show').removeClass('modal-show');
                modules.$html.removeClass('modal-show').removeAttr('data-modal-effect');
                modules.$window.scrollTop(self.scrollTopPosition);
            }
        }
    }
    modules.init();

    function saveModal(){

        $('input[name="EthereumWallet"]').val($('#e').val());
        $('input[name="IBAN"]').val($('#i').val());
        $('input[name="HeaderAccount"]').val($('#h').val());
        $('input[name="BankAccount"]').val($('#b').val());
        $('input[name="SwiftCode"]').val($('#s').val());
        $('input[name="Prefered"]').val($('#p').val());

        $('#abc').modal('hide');
    }

    $('.country').select2({
        placeholder: 'Select an Country',
        ajax: {
            url: "{{route('autocomplete.fetch')}}",
            dataType: 'json',
            delay: 250,
            processResults: function (data) {

                return {
                    results: data
                };
            },
            cache: true
        }
    });
 

    $(window).load(function(){
        $('#enter').fadeOut(200);
    });

    function sendForm(){
        $('#storeUser').submit();
        //$('#enter').fadeIn(200);   
        
    }


 

    function cargarSelect() {
        if($('.country').val()==null){
        $('.country').select2({
            placeholder: 'Select an Country',
            ajax: {
                url: "{{route('autocomplete.fetch')}}",
                dataType: 'json',
                delay: 250,
                processResults: function (data) {

                    return {
                        results: data
                    };
                },
                cache: true
                }
            });
        }
        if($('.countryCurrent').val()==null) {
            $('.countryCurrent').select2({
                placeholder: 'Select an Country',
                ajax: {
                    url: "{{route('autocomplete.fetch')}}",
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {

                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });

        }
    }

    function compl() {
        animating = true;
        current_fs = $('#page2');
        console.log(current_fs);
        next_fs = $('#page3');
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        next_fs.show();
        current_fs.animate({opacity: 0}, {
            step: function (now, mx) {
                scale = 1 - (1 - now) * 0.2;
                left = (now * 50) + "%";
                opacity = 1 - now;
                current_fs.css({'transform': 'scale(' + scale + ')'});
                next_fs.css({'left': left, 'opacity': opacity});
            }, duration: 800, complete: function () {
                current_fs.hide();
                animating = false;
            }, easing: 'easeInOutExpo'
        });
    }

</script>
</body>
</html>

























