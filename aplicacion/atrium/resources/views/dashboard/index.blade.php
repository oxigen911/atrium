@extends('layout.atrium')
@section('container')
 <div class="col-md-7 col-xl-11 pr-3">
              <div class="card">

                <div class="card-body">
                  <h4>To Approve</h4>
                  <div class="cp">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a
                          class="nav-link active"
                          id="home-tab"
                          data-toggle="tab"
                          href="#new-users"
                          role="tab"
                          aria-controls="home"
                          aria-selected="true"
                          >New Users</a
                        >
                      </li>
                      <li class="nav-item">
                        <a
                          class="nav-link"
                          id="profile-tab"
                          data-toggle="tab"
                          href="#request"
                          role="tab"
                          aria-controls="profile"
                          aria-selected="false"
                          >Requests</a
                        >
                      </li>
                      <li class="nav-item">
                        <a
                          class="nav-link"
                          id="contact-tab"
                          data-toggle="tab"
                          href="#financial"
                          role="tab"
                          aria-controls="contact"
                          aria-selected="false"
                          >Financial</a
                        >
                      </li>
                      <li>
                        <a
                          class="nav-link"
                          id="contact-tab"
                          data-toggle="tab"
                          href="#chronology"
                          role="tab"
                          aria-controls="contact"
                          aria-selected="false"
                          >Chronology</a
                        >
                      </li>
                      <li>
                        <div class="input-wi">
                          <input
                            type="text"
                            class="form-control btn-sm"
                            id="search"
                            placeholder="Search"
                          /><i class="fa fa-search" aria-hidden="true"></i>
                        </div>
                      </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                      <div
                        class="tab-pane fade show active"
                        id="new-users"
                        role="tabpanel"
                        aria-labelledby="home-tab"
                      >
                        <div class="table-responsive">
                          <table class="table table-borderless">
                            <thead>
                              <tr>
                                <th scope="col">User ID</th>
                                <th scope="col">Atrium ID</th>
                                <th scope="col">Email</th>
                                <th scope="col">Name</th>
                                <th scope="col">Date registration</th>
                                <th scope="col">Approve</th>
                                <th scope="col">Decline</th>
                              </tr>
                            </thead>
                            <tbody>

                            @forelse($clients as $k=>$client)
                            
                            	<tr>
                                <th scope="row"><a href="#" 
                                    data-id="{{$client->id}}" 
                                    data-name="{{$client->FullName}}" data-email="{{$client->Email}}" 
                                    data-created="{{$client->FormatDateCreated}}" 
                                    class="colaborator" onclick="modalCol(this)">N{{str_pad($client->id,5,'0',STR_PAD_LEFT)}}</a></th>
                                <td>12301</td>
                                <td>{{$client->Email}}</td>
                                <td>{{$client->FirstName.' '.$client->LastName}}</td>
                                <td>{{$client->created_at->format('d.m.Y')}}</td>
                                <td>
                                  <div class="btn-group">
                                  @if($client->Confirmed==1)
                                  <a
                                    href="#"
                                    class="btn btn-success btn-sm approve"
                                    data-toggle="modal"
                                    data-target="#Modal1"
                                    data-id="{{$client->id}}"
                                    data-email="{{$client->Email }}"
                                    data-name="{{$client->FirstName.' '.$client->LastName}}"
                                    >Approve</a
                                  >
                                  @else
                                  
                                  <a href="#" class="btn btn-warning btn-sm" style="display: table-cell"><small>Waiting Confirmation</small></a>
                                  <a href="#" class="btn btn-info btn-sm" style="display: table-cell" onclick="confirmation({{$client->id}})"><small><i class="fa fa-envelope-o"></i> </small></a>
                                  @endif
                                  </div>
                                </td>
                                <td>
                                  <a
                                    href="#"
                                    class="btn btn-danger btn-sm decline"
                                    data-toggle="modal"
                                    data-target="#Modal2"
                                    data-id="{{$client->id}}"
                                    data-email="{{$client->Email}}"
                                    data-name="{{$client->FirstName.' '.$client->LastName}}"
                                    >Decline</a
                                  >

                                </td>
                              </tr>
                              @empty
                              <tr>
                                <td> Not user </td>
                              </tr>
                            @endforelse
                            </tbody>
                          </table>
                          <nav aria-label="Page navigation example">
                               {{ $clients->links('layout.pagination') }}
                          </nav>
                        </div>
                      </div>
                      <div
                        class="tab-pane fade"
                        id="request"
                        role="tabpanel"
                        aria-labelledby="profile-tab"
                      >
                        ...222
                      </div>
                      <div
                        class="tab-pane fade"
                        id="financial"
                        role="tabpanel"
                        aria-labelledby="contact-tab"
                      >
                        <div class="table-responsive">
                          <table class="table table-borderless">
                            <thead>
                              <tr>
                                <th scope="col">Operation ID</th>
                                <th scope="col">Type</th>
                                <th scope="col">UserId from</th>
                                <th scope="col">UserId to</th>
                                <th scope="col">Email From</th>
                                <th scope="col">Coin</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Method Payment</th>
                                <th scope="col">Approve</th>
                                <th scope="col">Decline</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                              use Illuminate\Support\Arr;

                                $array = ['Buy', 'Send', 'Sell','Send ERC-20'];

                              ?>
                            @foreach($movements as $k=>$movement)
                              <tr>
                                <th scope="row"><a href="#" 
                                    data-toggle="modal"
                                    data-target="#ModalFinancial1"
                                    data-id="{{$movement->id}}"
                                    data-email="{{$movement->client->Email}}"
                                    data-name="{{$movement->client->FirstName.' '.$movement->client->LastName}}"
                                    class="colaborator approveFinancial">F{{str_pad($movement->id,3,'0',STR_PAD_LEFT)}}</a></th>
                                <td>Buy</td>
                                <td>{{$movement->client->UserName_NDG}}</td>
                                <td></td>
                                
                                <td>{{$movement->client->Email}}</td>
                                <td>{{$movement->coin->initial}}</td>
                                <td>{{$movement->quantity}}</td>  
                                <td>{{$movement->methodPayment->name}}</td>  
                                 
                                <td>
                                  <a
                                    href="#"
                                    class="btn btn-success btn-sm approveFinancial"
                                    data-toggle="modal"
                                    data-target="#ModalFinancial1"
                                    data-id="{{$movement->id}}"
                                    data-email="{{$movement->client->Email}}"
                                    data-request="{{$movement->date_movement->format('Y.m.d H:i:s')}}"
                                    data-coin="{{$movement->coin->initial}}"
                                    data-value="{{$movement->quantity*$movement->price_actually}}"
                                    data-method="{{$movement->methodPayment->name}}"
                                    data-name="{{$movement->client->FirstName.' '.$movement->client->LastName}}"
                                    >Approve</a
                                  >
                                </td>
                                <td>
                                  <a
                                    href="#"
                                    class="btn btn-danger btn-sm decline2"
                                    data-toggle="modal"
                                    data-target="#ModalFinancial2"
                                    data-id="{{$movement->id}}"
                                    data-request="{{$movement->date_movement->format('Y.m.d H:i:s')}}"
                                    data-method="{{$movement->methodPayment->name}}"
                                    data-coin="{{$movement->coin->initial}}"
                                    data-value="{{$movement->quantity*$movement->price_actually}}"
                                    data-email="{{$movement->client->Email}}"
                                    data-name="{{$movement->client->FirstName.' '.$movement->client->LastName}}"
                                    >Decline</a
                                  >
                                </td>
                              </tr>
                            @endforeach
                            </tbody>
                          </table>
                          <nav aria-label="Page navigation example">
                               {{ $movements->links('layout.pagination') }}
                          </nav>
                        </div>
                      </div>
                      <div
                        class="tab-pane fade"
                        id="chronology"
                        role="tabpanel"
                        aria-labelledby="contact-tab"
                      >
                        ...444
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>



      <!----------Modals------------>
      <div class="modal fade in" id="ModalColaborator" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Detail Information Client</h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Name: <strong id="na"></strong></p>
              <p>Email: <strong id="em"></strong></p>
              <p>Date Register: <strong id="fec"></strong></p>
            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" class="btn btn-primary">
                Approve
              </button>
            </div>
          </div>
        </div>
      </div>
      <!----------Modals-end----------->

      <!----------Modals------------>
      <div class="modal fade in" id="Modal2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Decline</h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Do you want to Reject this Client?</p>
              <h3 id="modal-decline"></h3>
               <label>reason for rejection?:</label>
               <textarea class="form-control btn-sm" required="required" id="motivoRechazo"></textarea>
                           
              <input type="hidden" name="client_id_deny">

            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" class="btn btn-primary" id="btnDeny">
                Decline
              </button>
            </div>
          </div>
        </div>
      </div>
      <!----------Modals------------>
      <div class="modal fade in" id="Modal1" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Approve</h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>You want to approve the client ?.</p>
              <h3 id="modal-approve"></h3>
              <input type="hidden" name="client_id_approved">
            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" class="btn btn-primary" id="btnApprove">
                Approve
              </button>
            </div>
          </div>
        </div>
      </div>

      <!----------Modals------------>
      <div class="modal fade in" id="ModalFinancial1" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" >
              <h5 class="modal-title" id="titleFinancial" style="margin-left: 40%;"></h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <table style="margin: 0px 20%">
                <tr>
                  <td colspan="2" id="headerFinancial" style="text-align: center">ID 32441</td>  
                </tr>
                <br>
                <tr>
                  <td style="color:#d4cbcb; font-weight: bold">Type </td>
                  <td style="font-weight: bold">Buy</td>
                </tr>
                <tr>
                  <td style="color:#d4cbcb; font-weight: bold">User ID from </td>
                  <td style="font-weight: bold"> 34456 </td>
                </tr>
                <tr>
                  <td style="color:#d4cbcb; font-weight: bold">Coin </td>
                  <td style="font-weight: bold" id="coinV"></td>
                </tr>
                <tr>
                  <td style="color:#d4cbcb; font-weight: bold">Value </td>
                  <td style="font-weight: bold" id="valueV"></td>
                </tr>
                <tr>
                  <td style="color:#d4cbcb; font-weight: bold">Date of request</td>
                  <td style="font-weight: bold" id="requestV"></td>
                </tr>
                <tr>
                  <td style="color:#d4cbcb; font-weight: bold">Method Payment</td>
                  <td style="font-weight: bold" id="methodPayment"></td>
                </tr>

              </table>
              <input type="hidden" name="client_id_approved">
            </div>
            <div class="modal-footer" style="text-align: center">
              <button type="button" class="btn btn-primary" id="btnApproveFinancial" style="width: 309px;height: 87px; margin: 0px 13%">
                Authorisation levels<br> <span>1 , 2</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    <!----------Modals-end----------->

          <!----------Modals------------>
      <div class="modal fade in" id="ModalFinancial2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" >
              <h5 class="modal-title" id="titleFinancial" style="margin-left: 40%;"></h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <table style="margin: 0px 20%">
                <tr>
                  <td colspan="2" id="headerFinancial" style="text-align: center">ID 32441</td>  
                </tr>
                <br>
                <tr>
                  <td style="color:#d4cbcb; font-weight: bold">Type </td>
                  <td style="font-weight: bold">Buy</td>
                </tr>
                <tr>
                  <td style="color:#d4cbcb; font-weight: bold">User ID from </td>
                  <td style="font-weight: bold"> 34456 </td>
                </tr>
                <tr>
                  <td style="color:#d4cbcb; font-weight: bold">Coin </td>
                  <td style="font-weight: bold" id="coinV2"></td>
                </tr>
                <tr>
                  <td style="color:#d4cbcb; font-weight: bold">Value </td>
                  <td style="font-weight: bold" id="valueV2"></td>
                </tr>
                <tr>
                  <td style="color:#d4cbcb; font-weight: bold">Date of request</td>
                  <td style="font-weight: bold" id="requestV2"></td>
                </tr>
                <tr>
                  <td style="color:#d4cbcb; font-weight: bold">Method Payment</td>
                  <td style="font-weight: bold" id="methodPayment2"></td>
                </tr>

              </table>
              <input type="hidden" name="movDeny">
            </div>
            <div class="modal-footer" style="text-align: center">
              <button type="button" class="btn btn-alert" id="btnDeclineFinancial" style="width: 309px;height: 87px; margin: 0px 13%">
                Decline</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    <!----------Modals-end----------->
            @endsection

            @section('js-atrium')
                @include('error.error')
             <script type="text/javascript">

              $("#btnApprove").click(function () {
                $('#btnApprove').attr('disabled','disabled');

                   $.post("{{route('approve')}}", { clientId : $('input[name="client_id_approved"]').val(), _token: '{{csrf_token()}}' }, function(data){
                    $('#btnApprove').attr('disabled',false);
                     $("#modal-approve").append('<br>'+data.msj);
                     if(data.state==200){ 
                      
                       setTimeout(function(){ 
                        $('#Modal1').modal('hide');
                        if ($('.modal-backdrop').is(':visible')) {
                        $('body').removeClass('modal-open'); 
                        $('.modal-backdrop').remove(); 
                           
                          }
                        location.reload(); 
                       }, 1000); 
                      }

                      if(data.state==500){
                        alert(data.msj);
                      }

                    },'json'); 
                });

                $("#btnApproveFinancial").click(function () {
                $('#btnApproveFinancial').attr('disabled','disabled');

                   $.post("{{route('approveTransaction')}}", { movId : $('input[name="client_id_approved"]').val(), _token: '{{csrf_token()}}' }, function(data){
                    console.log(data);
                    $('#btnApproveFinancial').attr('disabled',false);
                     $("#modal-approveFinancial").append('<br>'+data.msj);
                     if(data.state==200){ 
                      
                       setTimeout(function(){ 
                        $('#Modal1').modal('hide');
                        if ($('.modal-backdrop').is(':visible')) {
                        $('body').removeClass('modal-open'); 
                        $('.modal-backdrop').remove(); 
                           
                          }
                        location.reload(); 
                       }, 1000); 
                      }

                      if(data.state==500){
                        alert(data.msj);
                      }

                    },'json'); 
                });


                $("#btnDeclineFinancial").click(function () {
                $('#btnDeclineFinancial').attr('disabled','disabled');

                   $.post("{{route('denyTransaction')}}", { movId : $('input[name="movDeny"]').val(), _token: '{{csrf_token()}}' }, function(data){
                    console.log(data);
                    $('#btnApproveFinancial').attr('disabled',false);
                     $("#modal-approveFinancial").append('<br>'+data.msj);
                     if(data.state==200){ 
                      
                       setTimeout(function(){ 
                        $('#Modal1').modal('hide');
                        if ($('.modal-backdrop').is(':visible')) {
                        $('body').removeClass('modal-open'); 
                        $('.modal-backdrop').remove(); 
                           
                          }
                        location.reload(); 
                       }, 1000); 
                      }

                      if(data.state==500){
                        alert(data.msj);
                      }

                    },'json'); 
                });
                


              $("#btnDeny").click(function () {
                if($('#motivoRechazo').val()==""){
                  alert("debe ingresar motivo de rechazo");
                  $('#motivoRechazo').focus();
                  return false;
                }
                $('#btnDeny').attr('disabled','disabled');

                   $.post("{{route('deny')}}", { clientId : $('input[name="client_id_deny"]').val(),motivoRechazo:$('#motivoRechazo').val(), _token: '{{csrf_token()}}' }, function(  data){
                    $('#btnDeny').attr('disabled',false);
                    $("#modal-decline").append('<br>'+data.msj);
                     if(data.state==200){ 
                      
                       setTimeout(function(){ 
                        $('#Modal2').modal('hide');
                        if ($('.modal-backdrop').is(':visible')) {
                        $('body').removeClass('modal-open'); 
                        $('.modal-backdrop').remove(); 
                           
                          }
                        location.reload(); 
                       }, 1000); 
                      }

                      if(data.state==500){
                        alert(data.msj);
                      }                             
                    },'json'); 
                });


              function modalCol(elem){
                  $('#na').html($(elem).data('name'));
                  $('#em').html($(elem).data('email'));
                  $('#fec').html($(elem).data('created'));
                  $('#ModalColaborator').modal('show');
              }

              function confirmation(elem){
                  Notiflix.Confirm.Show('Email Confirm',
                 'Do you want to resend the confirmation email?',
                  'Yes','No',
                  function(){
                      $.post("{{route('sendConfirmMail')}}", { client : elem , _token: '{{csrf_token()}}' }, function(data){
                         Notiflix.Notify.Success(data);                          
                        },'json').fail(function(xhr, status, error){
                          Notiflix.Notify.Failure(xhr);
                        }); 
                  }
                );
              }
            </script>
            @endsection
