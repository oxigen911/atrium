@extends('layout.atrium')
@section('container')
<div class="col-md-7 col-xl-10 pr-3">
              <div class="card">

                <div class="card-body">
                  <h4>Register</h4>
                  <div class="cp">
                    <form action="{{route('store.user')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label>Atrium ID:</label>
                      <input
                        type="text"
                        class="form-control"
                        id="atriumID"
                        placeholder="Atrium ID"
                        name="UserName_NDG"
                        value="{{ old('UserName_NDG') }}"
                      />
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input
                        type="email"
                        class="form-control"
                        id="exampleInputEmail1"
                        aria-describedby="emailHelp"
                        placeholder="Enter email"
                        name="Email"
                        value="{{ old('Email') }}"
                      />
                    </div>
                    <div class="form-group">
                      <label>Nombres</label>
                      <input
                        type="text"
                        class="form-control"
                        id="nombres"
                        placeholder="Nombres"
                        name="FirstName"
                        value="{{ old('FirstName') }}"
                      />
                    </div>
                    <div class="form-group">
                      <label>Apellidos</label>
                      <input
                        type="text"
                        class="form-control"
                        id="apellidos"
                        placeholder="Apellidos"
                        name="LastName"
                        value="{{ old('LastName') }}"
                      />
                    </div>
                    <div class="form-group">
                      <label for="formGroupExampleInput"
                        >Fecha de nacimiento</label
                      >
                      <input
                        type="date"
                        class="form-control"
                        id="fechaNac"
                        placeholder="10-02-1989"
                        name="BirthDate"
                        value="{{ old('BirthDate') }}"
                      />
                    </div>
                    <div class="form-group">
                      <label for="inputCountry">País de nacimiento</label>
                      <select id="inputCountry" class="form-control" name="CountryBirth" value="{{ old('CountryBirth') }}">
                        <option selected>Choose...</option>
                        <option value="1">1</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="inputState">State</label>
                      <select id="inputState" class="form-control" name="DistrictBirth" value="{{ old('DistrictBirth') }}">
                        <option selected>Choose...</option>
                        <option value="1">1</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="inputCity">Ciudad de nacimiento</label>
                      <select id="inputCity" class="form-control" name="CityBirth" value="{{ old('CityBirth') }}">
                        <option selected>Choose...</option>
                        <option value="1">1</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="inputCountryR">País de residencia</label>
                      <select id="inputCountryR" class="form-control" name="CountryResidency" value="{{ old('CountryResidency') }}">
                        <option selected>Choose...</option>
                        <option value="1">1</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Direccion</label>
                      <input
                        type="text"
                        class="form-control"
                        id="dir"
                        placeholder="Direccion"
                        name="Address"
                        value="{{ old('Address') }}"
                      />
                    </div>
                    <div class="form-group">
                      <label for="tipoDoc">Tipo de documento</label>
                      <select id="tipoDoc" class="form-control" name="DocumentType" value="{{ old('DocumentType') }}">
                        <option selected>Choose...</option>
                        <option>C.C</option>
                        <option value="1">1</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Número de documento</label>
                      <input
                        type="text"
                        class="form-control"
                        id="numDoc"
                        placeholder="Número documento"
                        name="DocumentIdentification"
                        value="{{ old('DocumentIdentification') }}"
                      />
                    </div>
                    <div class="form-group">
                      <label for="formGroupExampleInput"
                        >Fecha de erogación</label
                      >
                      <input
                        type="date"
                        class="form-control"
                        id="fechaDocE"
                        placeholder="10-02-1989"
                        name="DocumentIssueDate"
                        value="{{ old('DocumentIssueDate') }}"
                      />
                    </div>
                    <div class="form-group">
                      <label for="formGroupExampleInput"
                        >Fecha de vencimiento</label
                      >
                      <input
                        type="date"
                        class="form-control"
                        id="fechaVen"
                        placeholder="10-02-1989"
                        name="DocumentexpirationDate"
                        value="{{ old('DocumentexpirationDate') }}"
                      />
                    </div>

                    <div class="form-group">
                      <div class="custom-file">
                        <input
                          type="file"
                          class="custom-file-input"
                          id="validatedCustomFile"
                          required
                          name="CertificationResidency"
                        />
                        <label
                          class="custom-file-label"
                          for="validatedCustomFile"
                          >Choose file...</label
                        >
                        <div class="invalid-feedback">
                          Example invalid custom file feedback
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="custom-file">
                        <input
                          type="file"
                          class="custom-file-input"
                          id="validatedCustomFile2"
                         name="DocumentImage"
                        />
                        <label
                          class="custom-file-label"
                          for="validatedCustomFile2"
                          >Choose file...</label
                        >
                        <div class="invalid-feedback">
                          Example invalid custom file feedback
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary">
                      Submit
                    </button>
                  </form>
                    
                  </div>
                </div>
              </div>
            </div>

@endsection