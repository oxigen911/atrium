@extends('layout.atrium')
@section('css-atrium')
    <style>
        .disabled{
            color: #b9afaf;
        }
        .disabledbtn , .disabledbtn2{
            background: #b9afaf !important;
            color: white !important;
            border-color: rgba(44,129,212,.25) !important;
        }
    </style>
@endsection



@section('container')
          <div class="col-md-7 col-xl-11 pr-3">
             <div class="card">

                <div class="card-body">
                           
                        <div class="table-responsive">
                          <table class="table table-borderless">
                            <thead>
                              <tr>
                                <th scope="col">Atrium ID</th>
                                <th scope="col">Email</th>
                                <th scope="col">Name</th>
                                <th scope="col">Activity</th>
                                <th scope="col">Coin</th>
                                <th scope="col">Add Coin</th>
                                <th scope="col">Date registration</th>
                                <th scope="col">Approved by</th>
                                <th scope="col">Approved Date</th>
                                <th scope="col">Delete</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($clients as $k=>$client)
                              <tr>
                                <td scope="row"><a href="#" onclick="modalAtriumID(this)" data-id="{{$client->id}}">{{str_pad($client->UserName_NDG,5,'0',STR_PAD_LEFT)}}</a></td>
                                <td>{{$client->Email}}</td>
                                <td>{{$client->FirstName.' '.$client->LastName}}</td>
                                
                                <td><a href="#" onclick="modalCol(this)"
                                    data-id="{{$client->id}}" >List</a></td>
                                <td><a href="#"  onclick="modalCoinActivity(this)" 
                                data-id="{{$client->id}}">List</a></td>
                                

                                <td><a href="#" data-coin="{{'AtriumID '.$client->UserName_NDG.' - '.$client->FullName}}" data-client="{{$client->id}}" class="modal-coin">Select Coin</a></td>
                                <td>{{$client->created_at->format('d.m.Y')}}</td>
                                <td>{{$client->ApprobedByYUser->name}}</td>
                                <td>{{$client->Approved->format('d.m.Y')}}</td>
                                <td>
                                <div class="btn-group">
                                    <a href="#" onclick="del({{$client->id}})" class="btn btn-danger btn-sm" ><small>Delete</small></a>
                                </div>
                                </td>
                              </tr>
                            @endforeach
                            </tbody>
                          </table>
                          <nav aria-label="Page navigation example">
                               {{ $clients->links('layout.pagination') }}
                          </nav>
                        </div>
                      </div>   
                              </div>
            </div>

          <!----------Modals------------>
          <div class="modal fade in" id="ModalAddCoin" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div class="modal-body">

                          <form method="POST" action="{{route('coin.saveBuyUser')}}" id="modalForm">
                              <input type="hidden" name="_token" value="{{csrf_token()}}">
                              <input type="hidden" name="client" >
                              <div class="form-group text-center">
                              <select class="form-control" id="selectCoin" style="text-align-last: center;" name="coin">
                                  <option value="null">Select Coin</option>
                                  @foreach($coins as $coin)
                                      <option value="{{$coin->id}}"> {{$coin->initial.' - '. $coin->name}} </option>
                                  @endforeach
                              </select>
                          </div>
                              <div class="form-group text-center">
                                  <label for="full_name_id" class="control-label disabled">Coin Available</label><br>
                                  <label class="control-label"><strong id="infoCoinAvailable"></strong></label>
                              </div>

                              <div class="form-group text-center">
                                  <label for="full_name_id" class="control-label disabled">Quantity of coin to add to</label><br>
                                  <label class="control-label"><strong id="infoCoin2Available"></strong></label>
                                  <div class="row">
                                      <div class="col-sm-4"></div>
                                      <div class="col-sm-4 text-center">
                                          <input type="number" class="form-control" name="quantity" id="qty" style="text-align: center">
                                      </div>
                                      <div class="col-sm-4"></div>
                                  </div>
                              </div>

                              <div class="form-group text-center"> <!-- Street 1 -->
                                  <label for="street1_id" class="control-label disabled">Are You Sure?</label>
                                  <div class="row">
                                      <div class="col-sm-3">
                                      </div>
                                      <div class="col-sm-3">
                                          <a href="#" class="btn btn-success disabledbtn" disabled style="width: 100px" onclick="yes();">Yes</a>
                                      </div>
                                      <div class="col-sm-3">
                                          <a href="#" class="btn btn-danger disabledbtn"  disabled style="width: 100px">No</a>
                                      </div>
                                      <div class="col-sm-3">
                                      </div>

                                  </div>
                              </div>
                              <div class="form-group text-center">
                                  <label for="full_name_id" class="control-label disabled">you are sending </label> <strong id="infoCoin"> </strong><br>
                                  <label class="control-label disabled">to <strong id="infoCoin2"> </strong></label>
                              </div>
                              <!--
                              <div class="form-group text-center">
                                  <label for="full_name_id" class="control-label disabled">Request the Code</label><br>
                                  <div class="row">
                                  <div class="col-sm-4"></div>
                                  <div class="col-sm-4 text-center">
                                      <input type="text" class="form-control" disabled name="requestNumber" id="requestNumber"  style="text-align: center">
                                  </div>
                                  <div class="col-sm-4"></div>
                                  </div>
                              </div>-->
                              <div class="row">
                                  <div class="col-sm-2"></div>
                                  <div class="col-sm-8 text-center">
                                      <a href="#" class="btn btn-success disabledbtn2" id="sendForm" disabled style="width: 220px;">
                                          Approve
                                      </a>
                                  </div>

                                  <div class="col-sm-2"></div>
                              </div>

                          </form>
                      </div>
                  </div>
              </div>
          </div>
          <!----------Modals-end----------->
        <div class="modal fade in" id="ModalColaborator" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="max-width: 785px;">
          <div class="modal-content">
            <div class="modal-header" style="text-align:center">
              <h5 class="modal-title" id="titleModal" ></h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div id="rsDetail" style="padding:10px 10px 10px 10px">
              </div>
            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
      <!----------Modals-end----------->

@endsection         
@section('js-atrium')
    @include('error.error')
<script type="text/javascript">

    $('.modal-coin').click(function(){
        $('#infoCoin2Available').css('display','none');
        $('#infoCoin2Available').html($(this).data('coin'));
        $('input[name="client"]').val($(this).data('client'));
        $('#ModalAddCoin').modal('show');
    });


    $('#selectCoin').change(function(){
        $('#infoCoinAvailable').html('');
        var coin = $(this).val();
       $.ajax({
            type: "POST",
            url: "{{route('coin.available')}}",
            dataType: "json",
            data: { coin: coin, 
                    _token: '{{csrf_token()}}' },
            success: function (response) {
                if(response.state==200){
                    $('#infoCoinAvailable').html(response.available);
                    $('#infoCoin2Available').css('display','block');
                    $('label').removeClass('disabled');
                    $('.disabledbtn').removeAttr('disabled');
                    $('a').removeClass('disabledbtn');

                }


            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#enter').fadeOut(200);
            }
        });

        

    });

    function modalCol(elem){
        Notiflix.Loading.Hourglass('Loading...');
        $('#titleModal').html('History'); 
        $('#rsDetail').html('');
             var table ="";
        table='<div class="row" style="margin-top:10px;">'+
               '<div class="table-responsive">'+
               '<table class="table">'+
                 '<thead>'+
                        '<th>Coin</th>'+
                        '<th>Quantity</th>'+
                        '<th>Price Seld</th>'+
                        '<th>State</th>'+
                 '</thead>'+
                 '<tbody>';
                    
        $.post("{{route('client.detail')}}", { client : $(elem).data('id') , _token: '{{csrf_token()}}' }, function(data){
            Notiflix.Loading.Remove();       
            if(data.state==500){
                            Notiflix.Notify.Failure(data.data);
                        }
                        if(data.state==200){
                            $.each(data.data, function(k,v){
                                table +='<tr>'+
                                    '<td>'+v.coin+'</td>'+
                                    '<td>'+v.quantity+'</td>'+
                                    '<td>'+v.price_actually+'</td>'+
                                    '<td>'+v.state+'</td>'+
                                '</tr>';
                            });
                            
                 table+='</tbody></table></div></div>';      
                            $('#rsDetail').html(table);
                            $('#ModalColaborator').modal('show');
                        }
                        
        },'json').fail(function(xhr, status, error){
            Notiflix.Loading.Remove();

                          Notiflix.Notify.Failure(xhr);
                        });
         
    }

    function modalAtriumID(elem){
        Notiflix.Loading.Hourglass('Loading...');

        $('#rsDetail').html('');
             var table ="";
        table='<div class="row" style="margin-top:10px;">'+
               '<div class="table-responsive" style="text-align:center">';
               
                    
        $.post("{{route('client.getClient')}}", { client : $(elem).data('id') , _token: '{{csrf_token()}}' }, function(data){
            Notiflix.Loading.Remove();       
                        if(data.state==500){
                            Notiflix.Notify.Failure(data.data);
                        }
                        if(data.state==200){
                               table +='<div>'+
                                    '<img src="{{asset("/atrium/images/user-default.png")}}" width="120" class="rounded-circle">'+
                                    '<p style="margin-top:10px">'+data.data.FirstName+' '+data.data.LastName +'</p>'+
                                    '<p>'+data.data.Email+'</p>'+
                                    '<p> Atrium ID'+data.data.UserName_NDG+'</p>'+
                                    '<p> Position Advisor</p>'+
                                    '<p> '+data.data.Country+'</p>'+
                                    '<p> Start colaboration '+data.data.created+'</p>'+
                                '</div>';
                            
                            
                            $('#rsDetail').html(table);
                            $('#ModalColaborator').modal('show');
                        }
                        
        },'json').fail(function(xhr, status, error){
            Notiflix.Loading.Remove();

                          Notiflix.Notify.Failure(xhr);
                        });
         
    }

    function modalCoinActivity(elem){
        Notiflix.Loading.Hourglass('Loading...');
        $('#titleModal').html('Coin List'); 

        $('#rsDetail').html('');
             var table ="";
        table='<div class="row" style="margin-top:10px; text-align:center">'+
               '<div class="table-responsive">'+
               '<table class="table">';
                
                    
        $.post("{{route('client.getCoinPercentaje')}}", { client : $(elem).data('id') , _token: '{{csrf_token()}}' }, function(data){
            Notiflix.Loading.Remove();       
            if(data.state==500){
                            Notiflix.Notify.Failure(data.data);
                        }
                        if(data.state==200){
                            $.each(data.data, function(k,v){
                                table +='<tr>'+
                                    '<td class="border-0">'+v.initial+' - '+v.name+'</td>'+
                                '</tr>';
                            });
                            
                 table+='</tbody></table></div></div>';      
                            $('#rsDetail').html(table);
                            $('#ModalColaborator').modal('show');
                        }
                        
        },'json').fail(function(xhr, status, error){
            Notiflix.Loading.Remove();

                          Notiflix.Notify.Failure(xhr);
                        });
         
    }



    function yes() {
        var top = $("#infoCoinAvailable").html();
        if($('#qty').val()==''){
            alert("Quantity Is Null");
            return false;
        }
        $('#infoCoin2').html();
        $("#selectCoin option:selected").text();
        var coin = $("#selectCoin option:selected").text();
        var init = coin.split("-");
        $('#infoCoin').html($('#qty').val()+' '+init[0]);
        var r = $('#infoCoin2Available').html();
        var t = r.split("-");
        $('#infoCoin2').html(t[1]);
        $('#requestNumber').removeAttr('disabled');
        $('.disabledbtn2').removeAttr('disabled');
        $('.disabledbtn2').removeClass('disabledbtn2');
    }

    $('#sendForm').click(function(){
        if($('#requestNumber').val()==''){
            alert("Request Numbes is not valid");
            return false;
        }

        $('#modalForm').submit();
    });


    function del(id){


        var pre = document.createElement('pre');
        //custom style.
        pre.style.maxHeight = "400px";
        pre.style.margin = "0";
        pre.style.padding = "24px";
        pre.style.whiteSpace = "pre-wrap";
        pre.style.textAlign = "justify";
        pre.appendChild(document.createTextNode('Do you want to delete this client?'));
        //show as confirm
        alertify.confirm(pre, function(){
            var url = '{{ route("client.delete", ":id") }}';
            window.location.href = url.replace(':id', id);

        },function(){
            alertify.error('Declined');
        }).set({labels:{ok:'Accept', cancel: 'Decline'}, padding: false});


    }
</script>
@endsection