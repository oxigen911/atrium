@extends('layout.master')
@section('container')
<!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
     <div class="col-lg-12 col-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Customtab vertical Tab</h3>
              <h6 class="box-subtitle">Use default tab with class <code>vtabs, tabs-vertical & customvtab</code></h6>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            	<!-- Nav tabs -->
				<div class="vtabs customvtab">
					<ul class="nav nav-tabs tabs-vertical" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home3" role="tab" aria-expanded="true"><span class="hidden-sm-up"><i class="ion-home"></i></span> <span class="hidden-xs-down">Home</span> </a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile3" role="tab" aria-expanded="false"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span class="hidden-xs-down">Profile</span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages3" role="tab" aria-expanded="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Messages</span></a> </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="home3" role="tabpanel" aria-expanded="true">
							<div class="pad">
								<div class="table-responsive">
				  <table class="table table-hover">
					<tr>
					  <th>AtriumId</th>
					  <th>Email</th>
					  <th>Name</th>
					  <th>Date Register</th>
					  <th>Approve</th>
					  <th>Decline</th>
					</tr>
					@foreach($clients as $client)
						<tr>
						  <td><a href="javascript:void(0)">Order #123456</a></td>
						  <td>{{$client->Email}}</td>
						  <td>{{$client->FirstName.' '.$client->LastName}}</td>
						  <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{$client->created_at->format('d.m.Y')}} </span> </td>
						  <td><span class="label label-info">Approve</span></td>
						  <td><span class="label label-danger">Decline</span></td>
						</tr>
					@endforeach
					
				  </table>
				</div>
							</div>
						</div>
						<div class="tab-pane pad" id="profile3" role="tabpanel" aria-expanded="false">2</div>
						<div class="tab-pane pad" id="messages3" role="tabpanel" aria-expanded="false">3</div>
					</div>
				</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      
    </section>
    <!-- /.content -->
  
  
@endsection