<script>
    $(document).ready(function(){

        @if(count($errors)>=1)
            @foreach ($errors->all() as $err)
                Notiflix.Notify.Failure('{{$err}}');
            @endforeach
        @endif


        @if(\Session::has('success'))
            Notiflix.Report.Success('Success','{{Session::get('success')}}','Click');

        @endif

        @if(\Session::has('error'))
            Notiflix.Notify.Failure('{{Session::get('error')}}');
        @endif

    });
</script>






