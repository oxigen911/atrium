<div class="col-md-2 col-xl-1">
              <ul class="list-group list-group-flush menu-bar">
                <li class="list-group-item">
                  <a href="{{route('dash.user_approbe')}}" class="{{ (\Request::route()->getName() == 'dash.user_approbe') ? 'active' : '' }}"
                    ><i class="fa fa-th-large"></i> To Approve</a
                  >
                </li>
                <li class="list-group-item">
                  <a href="{{route('register.user')}}" class="{{ (\Request::route()->getName() == 'register.user') ? 'active' : ''  }}"><i class="fa fa-plus"></i> Users</a>
                </li>
                <li class="list-group-item">
                  <a href="{{route('coin.list')}}" class="{{ (\Request::route()->getName() == 'coin.list') ? 'active' : '' }}"><i class="fa fa-check-circle"></i> Coin</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-briefcase"></i> Partnership</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-circle"></i> Point</a>
                </li>
                <li class="list-group-item">
                  <a href="#"><i class="fa fa-cog"></i> Settings</a>
                </li>
              </ul>
            </div>