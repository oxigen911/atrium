<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('atrium/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/css/bootstrap-select.min.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/css/animate.min.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/css/main.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/css/dashboard.css')}}" />

    <!-- Font-awesome -->
    <link rel="stylesheet" href="{{asset('atrium/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('atrium/alertifyjs/css/alertify.css')}}"/>
    <link rel="stylesheet" href="{{asset('atrium/notiflix/notiflix-1.5.0.min.css')}}"/>

    @yield('css-atrium')
    <title>Atrium</title>
  </head>
  <body id="dashboard">
    <main>
      <header>
        <div class="container-fluid">
          <div class="row no-gutters align-items-center">
            <div class="col-md-2 col-xl-1 text-center">
              <img src="{{asset('atrium/images/logo-c.png')}}" alt="logo" class="logo-app" />
            </div>
            <div class="col">
              <div class="input-group">
                <div class="input-group-prepend">
                  <div class="input-group-text input-group-text-top">
                    <i class="fa fa-search"></i>
                  </div>
                </div>
                <input
                  type="text"
                  class="form-control form-control-top"
                  id="inlineFormInputGroupUsername"
                  placeholder="Search"
                />
              </div>
            </div>
            <div class="col-md-3 offset-md-2">
              <div class="media media-db ml-5">
                <img
                  src="{{asset('atrium/images/user-default.png')}}"
                  class="img-thumbnail rounded-circle img-account-db mr-3"
                  alt="..."
                />
                <div class="media-body">
                  <h6 class="username-db mt-3 mb-2">
                    {{Auth::user()->name}}<span class="badge notification badge-danger"
                      >3</span
                    ><br>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form> 
                  </h6>
                  <p>Level 4</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>

      <section class="panel">
        <div class="container-fluid">
          <div class="row no-gutters">
            @include('layout.atrium-menu')
            @yield('container')
            <!--
            <div class="col-md-3">
              <div class="card mb-3">
                <div class="card-body">
                  <h4>Authorisation levels</h4>
                  <nav aria-label="...">
                    <ul class="pagination pagination-sm">
                      <li class="page-item active" aria-current="page">
                        <span class="page-link">
                          1
                          <span class="sr-only">(current)</span>
                        </span>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">2</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">3</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">4</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">5</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">6</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">7</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">8</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">9</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">
                          <i class="fa fa-search"></i>
                        </a>
                      </li>
                    </ul>
                    <div class="input-wi">
                      <input
                        type="text"
                        class="form-control btn-sm"
                        id="search"
                        placeholder="Search"
                      /><i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                  </nav>
                  <p>&nbsp;</p>
                  <h4>Chat</h4>
                  <div class="card-o">
                    <div class="card-body">
                      This is some text within a card body.
                    </div>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-body">
                  <h4>Corporate comunication</h4>

                  <div class="row no-gutters log">
                    <div class="col-md-7">
                      <span class="font-black">from</span>
                      <a href="" class="">Sam Jackson</a>
                    </div>
                    <div class="col-md-5 text-right">
                      <span>04.02.2019</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          -->
          </div>
        </div>
      </section>

      
    </main>

    <footer>
      <div class="container">
        <div class="row align-items-center">
          <div class="col">Todos los derechos reservados</div>
        </div>
      </div>
    </footer>

    <script src="{{asset('atrium/js/jquery-3.3.1.slim.min.js')}}"></script>
    <script src="{{asset('atrium/js/popper.min.js')}}"></script>
    <script src="{{asset('atrium/js/jquery-2.1.3.min.js')}}"></script>
    <script src="{{asset('atrium/js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('atrium/js/jquery.validate.js')}}"></script>
    <script src="{{asset('atrium/js/bootstrap.min.js')}}"></script> 

    <script src="{{asset('atrium/js/bootstrap-material-design.min.js')}}"></script>
    <script src="{{asset('atrium/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('atrium/alertifyjs/alertify.js')}}"></script>
    <script src="{{asset('atrium/notiflix/notiflix-1.5.0.js')}}"></script>
    <script src="{{asset('atrium/notiflix/notiflix-globals.js')}}"></script>

    @yield('js-atrium')
    <script type="text/javascript">
      $("a.approve").click(function () {
            $("#modal-approve").html('');
            var my_id_value = $(this).data('id');
            var email = $(this).data('email');
            var name = $(this).data('name');
            $('input[name="client_id_approved"]').val(my_id_value);
            $("#modal-approve").append(email+'<br>');
            $("#modal-approve").append(name);
        });

      $("a.approveFinancial").click(function () {
            $("#modal-approveFinancial").html('');
            $("#titleFinancial").html('');
            var my_id_value = $(this).data('id');
            var email = $(this).data('email');
            var name = $(this).data('name');
            var coin = $(this).data('coin');
            var request = $(this).data('request');
            var value = $(this).data('value');
            var method = $(this).data('method');
            $('input[name="client_id_approved"]').val(my_id_value);
            $("#titleFinancial").append(my_id_value);
            $("#coinV").html(coin.toLocaleString('de-DE'));
            $("#requestV").html(request);

            $("#valueV").html(value.toLocaleString('de-DE'));
            $("#methodPayment").html(method);
            $("#modal-approveFinancial").append(email+'<br>');
            $("#modal-approveFinancial").append(name);
        });

      $("a.decline").click(function () {
         $("#modal-decline").html('');
             var my_id_value = $(this).data('id');
            var email = $(this).data('email');
            var name = $(this).data('name');
            $('input[name="client_id_deny"]').val(my_id_value);
            $("#modal-decline").append(email+'<br>');
            $("#modal-decline").append(name);
        });

      $("a.decline2").click(function () {
         $("#modal-decline").html('');
            var my_id_value = $(this).data('id');
            var email = $(this).data('email');
            var name = $(this).data('name');
            var request = $(this).data('request');
            var method = $(this).data('method');
            var value = $(this).data('value');
            var coin = $(this).data('coin');

            $('input[name="movDeny"]').val(my_id_value);
            $('#coinV2').html(coin);
            $('#valueV2').html(value);
            $('#requestV2').html(request);
            $('#methodPayment2').html(method);
            $("#modal-decline").append(email+'<br>');
            $("#modal-decline").append(name);
        });
      $("a.colaborator").click(function () {
            var my_id_value = $(this).data('id');
            $("#modal-colaborator").html(my_id_value);
        });
    </script>
  </body>
</html>
