<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
		 <div class="ulogo">
			 <a href="{{route('home')}}">
			  <!-- logo for regular state and mobile devices -->
			  <span><b>Unique </b>Admin</span>
			</a>
		</div>
        <div class="image">
          <img src="{{asset('img/user2-160x160.jpg')}}" class="rounded-circle" alt="User Image">
        </div>
        <div class="info">
          <p>Admin Template</p>
			<a href="" class="link" data-toggle="tooltip" title="" data-original-title="Settings"><i class="ion ion-gear-b"></i></a>
            <a href="" class="link" data-toggle="tooltip" title="" data-original-title="Email"><i class="ion ion-android-mail"></i></a>
            <a href="" class="link" data-toggle="tooltip" title="" data-original-title="Logout"><i class="ion ion-power"></i></a>
        </div>
      </div>
      <!-- sidebar menu -->
      <ul class="sidebar-menu" data-widget="tree">
		<li class="nav-devider"></li>
        <li class="header nav-small-cap">BACKEND</li>		
        <li class="treeview">
          <a href="#">
            <i class="icon-home"></i>
            <span>@Lang('menu.main')</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('dash.user_approbe')}}">@Lang('menu.user_aprobe_menu')</a></li>
            <li><a href="index-2.html">@Lang('menu.user_add_menu')</a></li>	
            <li><a href="index-3.html">@Lang('menu.user_manage_menu')</a></li>
            <li><a href="index-3.html">@Lang('menu.add_cash_menu')</a></li>
            <li><a href="index-3.html">@Lang('menu.manage_current_menu')</a></li>
            <li><a href="index-3.html">@Lang('menu.add_colaborate_menu')</a></li>
            <li><a href="index-3.html">@Lang('menu.manage_colaborate_menu')</a></li>	  
          </ul>
        </li>
      </ul>
    </section>
  </aside>