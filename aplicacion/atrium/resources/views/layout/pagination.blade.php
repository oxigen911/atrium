      @if ($paginator->lastPage() > 1)
      <ul class="pagination justify-content-end">
        <li class="{{ ($paginator->currentPage() == 1) ? 'page-item disabled' : '' }}">
            <a tabindex="-1" aria-disabled="true" class="page-link" href="{{ $paginator->url(1) }}">Previous</a>
        </li>  

        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <li class="page-item {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
            </li>
        @endfor
        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? 'page-item disabled' : '' }}">
            <a class="page-link" href="{{ $paginator->url($paginator->currentPage()+1) }}" >Next</a>
        </li>

      </ul>
      @endif
