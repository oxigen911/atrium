@extends('layout.atrium')
@section('css-atrium')
<link rel="stylesheet" href="{{asset('atrium/css/register-w.css')}}" />
@endsection



@section('container')

<div class="col-md-7 col-xl-8 pr-3">
        @include('error.error')
                <div class="info">
                        <h4>Welcome to Atrium, lets's start</h4>
                        <p>
                          Complete the following steps to create your account
                          
                        </p>
                      </div>
                      
                  <form class="steps" accept-charset="UTF-8" enctype="multipart/form-data" novalidate="" action="{{route('store.user')}}" method="POST">
                     {{ csrf_field() }}
                    <ul id="progressbar">
                      <li class="active"></li>
                      <li></li>
                      <li></li>
                      <li></li>
                    </ul>
                  
                  
                  
                    <!-- USER INFORMATION FIELD SET --> 
                    <fieldset>
                      <!-- Begin What's Your First Name Field -->
                          <div class="hs_firstname field hs-form-field">
                          
                            <label for="firstname-99a6d115-5e68-4355-a7d0-529207feb0b3_2983">Atrium Code / Promo Code </label>
                  
                            <input id="firstname-99a6d115-5e68-4355-a7d0-529207feb0b3_2983" type="text"  placeholder="Atrium Code / Promo Code" name="UserName_NDG"
                        value="{{ old('UserName_NDG') }}">
                            <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>
                      <!-- End What's Your First Name Field -->
                  
                      <!-- Begin What's Your Email Field -->
                          <div class="hs_email field hs-form-field">
                          
                            <label for="email-99a6d115-5e68-4355-a7d0-529207feb0b3_2983">First Name / Nome *</label>
                  
                            <input id="email-99a6d115-5e68-4355-a7d0-529207feb0b3_2983" required="required" type="text" placeholder="First Name / Nome" data-rule-required="true" data-msg-required="Please include your first name" name="FirstName"
                        value="{{ old('FirstName') }}">
                            <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>


                          <div class="hs_email field hs-form-field">
                          
                            <label for="email-99a6d115-5e68-4355-a7d0-529207feb0b3_2983">Last Name / Cognome *</label>
                  
                            <input id="email-99a6d115-5e68-4355-a7d0-529207feb0b3_2983" required="required" type="text" placeholder="Last Name / Cognome" data-rule-required="true" data-msg-required="Please include your first name" name="LastName"
                        value="{{ old('FirstName') }}">
                            <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>


                       <div class="hs_email field hs-form-field hs_total_number_of_constituents_in_your_database">
                          
                            <label >Email *</label>
                  
                            <input class="form-text hs-input" required="required" type="email" placeholder="Email" data-rule-required="true" data-msg-required="Please enter a valid email" name="Email"
                        value="{{ old('Email') }}">
                            <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>

                           <div class="hs_email field hs-form-field hs_total_number_of_constituents_in_your_database">
                          
                            <label>Confirm email *</label>
                  
                            <input  class="form-text hs-input" name="total_number_of_constituents_in_your_database" required="required" size="60" type="email" value="" placeholder="Confirm Email" data-rule-required="true" data-msg-required="Please enter a valid number" >
                            <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>
                    
                           <div class="hs_email field hs-form-field hs_total_number_of_constituents_in_your_database">
                          
                            <label>Password *</label>
                  
                            <input class="form-text hs-input" name="total_number_of_constituents_in_your_database" required="required" size="60" type="Password" value="" placeholder="Password" data-rule-required="true" data-msg-required="Please enter a valid number" >
                            <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>

                           <div class="hs_email field hs-form-field hs_total_number_of_constituents_in_your_database">
                          
                            <label >Confirm Password*</label>
                  
                            <input class="form-text hs-input" name="total_number_of_constituents_in_your_database" required="required" size="60" type="Password" value="" placeholder="Confirm Password" data-rule-required="true" data-msg-required="Please enter a valid number" >
                            <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>  

                      <!-- End Total Number of Constituents in Your Database Field -->
                      <input type="button" data-page="1" name="next" class="next action-button" value="Next" />
                      <div class="explanation btn btn-small modal-trigger" data-modal-id="modal-3"><i class="question-log fa fa-question-circle"></i> What Is This?</div>
                    </fieldset>
                  
                  
                  
                    <!-- ACQUISITION FIELD SET -->  
                    <fieldset>
                          <div class="form-item webform-component webform-component-textfield hs_total_number_of_donors_in_year_1 field hs-form-field" id="webform-component-acquisition--amount-1">
                          
                            <label >Date of Birth*</label>
                  
                            <input class="form-text hs-input" required="required" type="date" placeholder="Enter Date Birth" data-rule-required="true" data-msg-required="Please enter a valid date" name="BirthDate"
                        value="{{ old('BirthDate') }}">
                            <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>
                        <!-- End Total Number of Donors in Year 1 Field -->
                  
                        <!-- Begin Total Number of Donors in Year 2 Field -->
                          <div class="form-item webform-component webform-component-textfield hs_total_number_of_donors_in_year_2 field hs-form-field" id="webform-component-acquisition--amount-2">
                          
                            <label >Place of birth*</label>
                  
                            <select  class="form-text hs-input" required="required" data-rule-required="true" data-msg-required="Please enter a valid number" name="CountryBirth" value="{{ old('CountryBirth') }}">
                              <option value="null">Select Country</option>
                              @foreach($countries as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                              @endforeach
                            </select>

                            <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>
                          <!-- End Total Number of Donors in Year 2 Field -->
                  
                        <!-- Begin Calc of Total Number of Donors Fields -->
                        <!-- THIS FIELD IS NOT EDITABLE | GRAYED OUT -->
                          <div class="form-item webform-component webform-component-textfield webform-container-inline hs_total_donor_percent_change field hs-form-field">
                          
                            <label>District of birth*</label>
                  
                            <select  class="form-text hs-input" name="DistrictBirth" value="{{ old('DistrictBirth') }}" required="required" data-rule-required="true" data-msg-required="Please enter a valid number" >
                              <option value="null">Select State</option>
                            </select> 
                            <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>

                          <div class="form-item webform-component webform-component-textfield webform-container-inline hs_total_donor_percent_change field hs-form-field">
                          
                            <label >City of Birth*</label>
                  
                            <select  class="form-text hs-input" name="CityBirth" value="{{ old('CityBirth') }}"
                              required="required" data-rule-required="true" data-msg-required="Please enter a valid number"
                            >
                                  <option value="null">Select City</option>
                               
                            </select>
                             <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>


                          <div class="form-item webform-component webform-component-textfield webform-container-inline hs_total_donor_percent_change field hs-form-field">
                          
                            <label>Current country*</label>
                  
                           <select  class="form-text hs-input" required="required" data-rule-required="true" data-msg-required="Please enter a valid number" name="CountryCurrent" value="{{ old('CountryCurrent') }}">
                              <option value="null">Select Current Country</option>
                              @foreach($countries as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                              @endforeach
                            </select>
                             <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>



                          <div class="form-item webform-component webform-component-textfield webform-container-inline hs_total_donor_percent_change field hs-form-field">
                          
                            <label >Current city*</label>
                  
                            <select  class="form-text hs-input" name="CityCurrent" value="{{ old('CityCurrent') }}" style="width: 300px"  required="required" data-rule-required="true" data-msg-required="Please enter a valid number">
                                  <option value="null">Select Current City</option>
                                @foreach($cities as $city)
                                  <option value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select>
                             <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>


                          <!-- End Calc of Total Number of Donors Fields -->
                      <input type="button" data-page="2" name="previous" class="previous action-button" value="Previous" />
                      <input type="button" data-page="2" name="next" class="next action-button" value="Next" />
                      <div class="explanation btn btn-small modal-trigger" data-modal-id="modal-3">What Is This?</div>
                    </fieldset>
                  
                  
                  
                    <!-- Cultivation FIELD SET -->  
                    <fieldset>
                        <!-- Begin Average Gift Size in Year 1 Field -->
                          <div class="form-item webform-component webform-component-textfield hs_average_gift_size_in_year_1 field hs-form-field" id="edit-submitted-cultivation-amount-1 average_gift_size_in_year_1-99a6d115-5e68-4355-a7d0-529207feb0b3_3256">
                          
                            <label for="edit-submitted-cultivation-amount-1 average_gift_size_in_year_1-99a6d115-5e68-4355-a7d0-529207feb0b3_3256">Type of document</label>
                  
                            <select id="edit-submitted-cultivation-amount-1" class="form-text hs-input" required="required" data-rule-required="true" data-msg-required="Please enter a valid number"
                            name="DocumentType" value="{{ old('DocumentType') }}">
                            <option selected>Select type of document</option>
                            <option value="1">DNI</option>
                            <option value="2">C.C</option>
                            </select>
                            <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>
                          <!-- End Average Gift Size in Year 1 Field -->
                  
                        <!-- Begin Average Gift Size in Year 2 Field -->
                          <div class="form-item webform-component webform-component-textfield hs_average_gift_size_in_year_2 field hs-form-field" id="webform-component-cultivation--amount-2">
                          
                            <label for="edit-submitted-cultivation-amount-2 average_gift_size_in_year_2-99a6d115-5e68-4355-a7d0-529207feb0b3_3256">Document number</label>
                  
                            <input id="edit-submitted-cultivation-amount-2" class="form-text hs-input" required="required" size="60" maxlength="128" type="test" placeholder="Document Number" name="DocumentIdentification" value="{{ old('DocumentIdentification') }}" data-rule-required="true" data-msg-required="Please enter a valid number">
                            <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>
                          <!-- End Average Gift Size in Year 2 Field -->
                  
                        <!-- Begin Average Gift Size Perchent Change Field -->
                        <!-- THIS FIELD IS NOT EDITABLE | GRAYED OUT -->
                          <div class="form-item webform-component webform-component-textfield webform-container-inline hs_average_gift_size_percent_change field hs-form-field" id="webform-component-cultivation--percent-change1">
                          
                            <label >Release date</label>
                  
                            <input  class="form-text hs-input" type="date" value="" placeholder="Release Date"
                            name="DocumentIssueDate" value="{{ old('DocumentIssueDate') }}">
                          </div>



                          <div class="form-item webform-component webform-component-textfield webform-container-inline hs_average_gift_size_percent_change field hs-form-field" id="webform-component-cultivation--percent-change1">
                          
                            <label >Expiry date</label>
                  
                            <input  class="form-text hs-input" type="date" placeholder="Expiry Date"
                             name="DocumentexpirationDate" value="{{ old('DocumentexpirationDate') }}">
                          </div>



                          <div class="form-item webform-component webform-component-textfield webform-container-inline hs_average_gift_size_percent_change field hs-form-field" id="webform-component-cultivation--percent-change1">
                          
                            <label >Photo document Front</label>
                  
                             <div class="custom-file">
                              <input
                                type="file"
                                class="custom-file-input"
                                id="validatedCustomFile"
                                required
                                name="CertificationResidency"
                              />
                              <label
                                class="custom-file-label"
                                for="validatedCustomFile"
                                >Choose file Front...</label
                              >
                              <div class="invalid-feedback">
                                Example invalid custom file feedback
                              </div>
                            </div>
                            
                            
                          </div>


                          <div class="form-item webform-component webform-component-textfield webform-container-inline hs_average_gift_size_percent_change field hs-form-field" id="webform-component-cultivation--percent-change1">
                          
                            <label >Photo document Back</label>
                  
                            <div class="custom-file">
                              <input
                                type="file"
                                class="custom-file-input"
                                id="validatedCustomFile2"
                                required
                                name="DocumentImage"
                              />
                              <label
                                class="custom-file-label"
                                for="validatedCustomFile2"
                                >Choose file Back...</label
                              >
                              <div class="invalid-feedback">
                                Example invalid custom file feedback
                              </div>
                            </div>
                            
                            
                          </div> 



                          <!-- End Average Gift Size Perchent Change Field -->
                      <input type="button" data-page="3" name="previous" class="previous action-button" value="Previous" />
                      <input type="button" data-page="4" name="next" class="next action-button" value="Next" />
                      <div class="explanation btn btn-small modal-trigger" data-modal-id="modal-3">What Is This?</div>
                    </fieldset>
                  
                  
                  
                    <!-- Cultivation2 FIELD SET --> 
                    <fieldset>
                              <!-- Begin Total Giving In Year 1 Field -->
                          <div class="form-item webform-component webform-component-textfield" id="webform-component-cultivation--amount-3 hs_total_giving_in_year_1 field hs-form-field">
                          
                            <label for=" edit-submitted-cultivation-amount-3 total_giving_in_year_1-99a6d115-5e68-4355-a7d0-529207feb0b3_4902">Data Credit Card
                            Cooming Soon</label>
                  
                            <input id="edit-submitted-cultivation-amount-3" class="form-text hs-input" name="total_giving_in_year_1"  type="number" value="" placeholder="" >
                            <span class="error1" style="display: none;">
                                <i class="error-log fa fa-exclamation-triangle"></i>
                            </span>
                          </div>
                          <!-- End Total Giving In Year 1 Field -->
                  
                          
                          <!-- End Average Gift Size Percent Change Field 2 -->
                       <input type="button" data-page="4" name="previous" class="previous action-button" value="Previous" />
                      <input id="submit" class="hs-button primary large action-button next" type="submit" value="Submit">
                      <div class="explanation btn btn-small modal-trigger" data-modal-id="modal-3">What Is This?</div>
                    </fieldset>
                  
                  </form>
               
</div>
@endsection         

@section('js-atrium')   
 <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
 <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js"></script>


    <script>
      


$(document).ready(function(){var current_fs,next_fs,previous_fs;var left,opacity,scale;var animating;$(".steps").validate({errorClass:'invalid',errorElement:'span',errorPlacement:function(error,element){error.insertAfter(element.next('span').children());},highlight:function(element){$(element).next('span').show();},unhighlight:function(element){$(element).next('span').hide();}});$(".next").click(function(){$(".steps").validate({errorClass:'invalid',errorElement:'span',errorPlacement:function(error,element){error.insertAfter(element.next('span').children());},highlight:function(element){$(element).next('span').show();},unhighlight:function(element){$(element).next('span').hide();}});if((!$('.steps').valid())){return true;}
if(animating)return false;animating=true;current_fs=$(this).parent();next_fs=$(this).parent().next();$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");next_fs.show();current_fs.animate({opacity:0},{step:function(now,mx){scale=1-(1-now)*0.2;left=(now*50)+"%";opacity=1-now;current_fs.css({'transform':'scale('+scale+')'});next_fs.css({'left':left,'opacity':opacity});},duration:800,complete:function(){current_fs.hide();animating=false;},easing:'easeInOutExpo'});});$(".submit").click(function(){$(".steps").validate({errorClass:'invalid',errorElement:'span',errorPlacement:function(error,element){error.insertAfter(element.next('span').children());},highlight:function(element){$(element).next('span').show();},unhighlight:function(element){$(element).next('span').hide();}});if((!$('.steps').valid())){return false;}
if(animating)return false;animating=true;current_fs=$(this).parent();next_fs=$(this).parent().next();$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");next_fs.show();current_fs.animate({opacity:0},{step:function(now,mx){scale=1-(1-now)*0.2;left=(now*50)+"%";opacity=1-now;current_fs.css({'transform':'scale('+scale+')'});next_fs.css({'left':left,'opacity':opacity});},duration:800,complete:function(){current_fs.hide();animating=false;},easing:'easeInOutExpo'});});$(".previous").click(function(){if(animating)return false;animating=true;current_fs=$(this).parent();previous_fs=$(this).parent().prev();$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");previous_fs.show();current_fs.animate({opacity:0},{step:function(now,mx){scale=0.8+(1-now)*0.2;left=((1-now)*50)+"%";opacity=1-now;current_fs.css({'left':left});previous_fs.css({'transform':'scale('+scale+')','opacity':opacity});},duration:800,complete:function(){current_fs.hide();animating=false;},easing:'easeInOutExpo'});});});jQuery(document).ready(function(){jQuery("#edit-submitted-acquisition-amount-1,#edit-submitted-acquisition-amount-2,#edit-submitted-cultivation-amount-1,#edit-submitted-cultivation-amount-2,#edit-submitted-cultivation-amount-3,#edit-submitted-cultivation-amount-4,#edit-submitted-retention-amount-1,#edit-submitted-retention-amount-2,#edit-submitted-constituent-base-total-constituents").keyup(function(){calcTotal();});});function calcTotal(){var grade=0;var donorTotal=Number(jQuery("#edit-submitted-constituent-base-total-constituents").val().replace(/,/g,""));if(donorTotal)
{donorTotal=parseFloat(donorTotal);}
else
{donorTotal=0;}
grade+=getBonusDonorPoints(donorTotal);var acqAmount1=Number(jQuery("#edit-submitted-acquisition-amount-1").val().replace(/,/g,""));var acqAmount2=Number(jQuery("#edit-submitted-acquisition-amount-2").val().replace(/,/g,""));var acqTotal=0;if(acqAmount1){acqAmount1=parseFloat(acqAmount1);}else{acqAmount1=0;}
if(acqAmount2){acqAmount2=parseFloat(acqAmount2);}else{acqAmount2=0;}
if(acqAmount1>0&&acqAmount2>0){acqTotal=((acqAmount2-acqAmount1)/ acqAmount1*100).toFixed(2);}else{acqTotal=0;}
jQuery("#edit-submitted-acquisition-percent-change").val(acqTotal+'%');grade+=getAcquisitionPoints(acqTotal);console.log(grade);var cultAmount1=Number(jQuery("#edit-submitted-cultivation-amount-1").val().replace(/,/g,""));var cultAmount2=Number(jQuery("#edit-submitted-cultivation-amount-2").val().replace(/,/g,""));var cultTotal=0;if(cultAmount1){cultAmount1=parseFloat(cultAmount1);}else{cultAmount1=0;}
if(cultAmount2){cultAmount2=parseFloat(cultAmount2);}else{cultAmount2=0;}
if(cultAmount1>0&&cultAmount2>0){cultTotal=((cultAmount2-cultAmount1)/ cultAmount1*100).toFixed(2);}else{cultTotal=0;}
jQuery("#edit-submitted-cultivation-percent-change1").val(cultTotal+'%');grade+=getAcquisitionPoints(cultTotal);var cultAmount3=Number(jQuery("#edit-submitted-cultivation-amount-3").val().replace(/,/g,""));var cultAmount4=Number(jQuery("#edit-submitted-cultivation-amount-4").val().replace(/,/g,""));if(cultAmount3){cultAmount3=parseFloat(cultAmount3);}else{cultAmount3=0;}
if(cultAmount4){cultAmount4=parseFloat(cultAmount4);}else{cultAmount4=0;}
if(cultAmount3>0&&cultAmount4>0){cultTotal2=((cultAmount4-cultAmount3)/ cultAmount3*100).toFixed(2);}else{cultTotal2=0;}
jQuery("#edit-submitted-cultivation-percent-change2").val(cultTotal2+'%');grade+=getAcquisitionPoints(cultTotal2);var retAmount1=Number(jQuery("#edit-submitted-retention-amount-1").val().replace(/,/g,""));var retAmount2=Number(jQuery("#edit-submitted-retention-amount-2").val().replace(/,/g,""));var retTotal=0;if(retAmount1){retAmount1=parseFloat(retAmount1);}else{retAmount1=0;}
if(retAmount2){retAmount2=parseFloat(retAmount2);}else{retAmount2=0;}
if(retAmount1>0&&retAmount2>0){retTotal=(retAmount2 / retAmount1*100).toFixed(2);}else{retTotal=0;}
jQuery("#edit-submitted-retention-percent-change").val(retTotal+'%');grade+=getAcquisitionPoints(retTotal);jQuery("#edit-submitted-final-grade-grade").val(grade+' / 400');}
function getAcquisitionPoints(val)
{if(val<1)
{return 0;}
else if(val>=1&&val<6)
{return 50;}
else if(val>=6&&val<11)
{return 60;}
else if(val>=11&&val<16)
{return 70;}
else if(val>=16&&val<21)
{return 75;}
else if(val>=21&&val<26)
{return 80;}
else if(val>=26&&val<31)
{return 85;}
else if(val>=31&&val<36)
{return 90;}
else if(val>=36&&val<41)
{return 95;}
else if(val>=41)
{return 100;}}
function getCultivationGiftPoints(val)
{if(val<1)
{return 0;}
else if(val>=1&&val<4)
{return 50;}
else if(val>=4&&val<7)
{return 60;}
else if(val>=7&&val<10)
{return 70;}
else if(val>=10&&val<13)
{return 75;}
else if(val>=13&&val<16)
{return 80;}
else if(val>=16&&val<21)
{return 85;}
else if(val>=21&&val<26)
{return 90;}
else if(val>=26&&val<51)
{return 95;}
else if(val>=51)
{return 100;}}
function getCultivationDonationPoints(val)
{if(val<1)
{return 0;}
else if(val>=1&&val<6)
{return 50;}
else if(val>=6&&val<11)
{return 60;}
else if(val>=11&&val<16)
{return 70;}
else if(val>=16&&val<21)
{return 75;}
else if(val>=21&&val<26)
{return 80;}
else if(val>=26&&val<31)
{return 85;}
else if(val>=31&&val<36)
{return 90;}
else if(val>=36&&val<41)
{return 95;}
else if(val>=41)
{return 100;}}
function getRetentionPoints(val)
{if(val<1)
{return 0;}
else if(val>=1&&val<51)
{return 50;}
else if(val>=51&&val<56)
{return 60;}
else if(val>=56&&val<61)
{return 70;}
else if(val>=61&&val<66)
{return 75;}
else if(val>=66&&val<71)
{return 80;}
else if(val>=71&&val<76)
{return 85;}
else if(val>=76&&val<81)
{return 90;}
else if(val>=81&&val<91)
{return 95;}
else if(val>=91)
{return 100;}}
function getBonusDonorPoints(val)
{if(val<10001)
{return 0;}
else if(val>=10001&&val<25001)
{return 10;}
else if(val>=25001&&val<50000)
{return 15;}
else if(val>=50000)
{return 20;}}
var modules={$window:$(window),$html:$('html'),$body:$('body'),$container:$('.container'),init:function(){$(function(){modules.modals.init();});},modals:{trigger:$('.explanation'),modal:$('.modal'),scrollTopPosition:null,init:function(){var self=this;if(self.trigger.length>0&&self.modal.length>0){modules.$body.append('<div class="modal-overlay"></div>');self.triggers();}},triggers:function(){var self=this;self.trigger.on('click',function(e){e.preventDefault();var $trigger=$(this);self.openModal($trigger,$trigger.data('modalId'));});$('.modal-overlay').on('click',function(e){e.preventDefault();self.closeModal();});modules.$body.on('keydown',function(e){if(e.keyCode===27){self.closeModal();}});$('.modal-close').on('click',function(e){e.preventDefault();self.closeModal();});},openModal:function(_trigger,_modalId){var self=this,scrollTopPosition=modules.$window.scrollTop(),$targetModal=$('#'+_modalId);self.scrollTopPosition=scrollTopPosition;modules.$html.addClass('modal-show').attr('data-modal-effect',$targetModal.data('modal-effect'));$targetModal.addClass('modal-show');modules.$container.scrollTop(scrollTopPosition);},closeModal:function(){var self=this;$('.modal-show').removeClass('modal-show');modules.$html.removeClass('modal-show').removeAttr('data-modal-effect');modules.$window.scrollTop(self.scrollTopPosition);}}}
modules.init();

    $('').change(function(){


    });

    $('select[name="CountryBirth"]').on('change', function () {
        $("select[name='CountryBirth'] option:selected").each(function () {
            sel=$(this).val();
            $.post("{{route('get.State')}}", { country : sel, _token: '{{csrf_token()}}' }, function(data){
                console.log(data);
                $('select[name="DistrictBirth"]').html(data);
            });     
        });
   });


    $('select[name="DistrictBirth"]').on('change', function () {
        $("select[name='DistrictBirth'] option:selected").each(function () {
            sel=$(this).val();
            $.post("{{route('get.City')}}", { city : sel, _token: '{{csrf_token()}}' }, function(data){
                console.log(data);
                $('select[name="CityBirth"]').html(data);
            });     
        });
   });


    $('select[name="CountryCurrent"]').on('change', function () {
        $("select[name='CountryCurrent'] option:selected").each(function () {
            sel=$(this).val();
            $.post("{{route('get.CityWithOutState')}}", { city : sel, _token: '{{csrf_token()}}' }, function(data){
                console.log(data);
                $('select[name="CityCurrent"]').html(data);
            });     
        });
   });




    </script>
@endsection
