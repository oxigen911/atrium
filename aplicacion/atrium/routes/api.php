<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login','Api\LoginController@loginApi');
Route::post('register','Api\LoginController@registerApi');
Route::get('coins','Api\CoinController@list');
Route::post('coin','Api\CoinController@getCoin');
Route::post('buy','Api\CoinController@saveMovement');
Route::get('country/all', 'CountryController@countryAll');
Route::get('city/{pais}', 'CountryController@getCityApi');
Route::get('coin/{user_id}', 'Api\CoinController@getCoinUser');
Route::get('movements/{client_id}', 'Api\CoinController@infoByClient');

