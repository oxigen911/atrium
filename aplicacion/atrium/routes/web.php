<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/email_available/check', 'ClientController@check')->name('email_available.check');
Route::get('/locale/{locale}', function ($locale) {
    Session::put('locale',$locale);
    return redirect()->back();
})->name('locale');

Route::get('confirm/later/{ConfirmationCode}','DashboardController@confirmWithOutFix')->name('confirm.later');
#https://www.atriumprivatebanker.com

Auth::routes(['verify'=>true]);
Route::get('/home','DashboardController@index')->name('dash.user_approbe')->middleware(['auth'],['verified']);
Route::get('/','DashboardController@index')->name('dash.user_approbe')->middleware(['auth'],['verified']);

Route::get('client/delete/{id}', 'DashboardController@delete')->name('client.delete')->middleware(['auth']);

Route::get('dash/user','DashboardController@index')->name('dash.user_approbe')->middleware('auth');
Route::get('register/user','DashboardController@register')->name('register.user')->middleware('auth');
Route::post('store/user','DashboardController@storeUser')->name('store.user');
Route::post('approve','DashboardController@approve')->name('approve')->middleware('auth');
Route::post('/approveTransaction','DashboardController@approveTransaction')->name('approveTransaction')->middleware('auth');
Route::post('/denyTransaction','DashboardController@denyTransaction')->name('denyTransaction')->middleware('auth');

Route::post('deny','DashboardController@deny')->name('deny')->middleware('auth');
Route::post('client/details','DashboardController@detailClient')->name('client.detail')->middleware('auth');
Route::post('client/getClient','DashboardController@getClient')->name('client.getClient')->middleware('auth');
Route::post('client/getCoinPercentaje','DashboardController@getCoinPercentaje')->name('client.getCoinPercentaje')->middleware('auth');

Route::get('coin','CoinController@index')->name('coin.list')->middleware('auth');
Route::post('coin','CoinController@store')->name('store.coin')->middleware('auth');
Route::post('coinUpdate','CoinController@update')->name('update.coin')->middleware('auth');
Route::post('getCoin','CoinController@getCoin')->name('coin.getCoin')->middleware('auth');
Route::post('available','CoinController@available')->name('coin.available')->middleware('auth');
Route::post('saveBuyUser','CoinController@saveBuyUser')->name('coin.saveBuyUser')->middleware('auth');
Route::post('sendConfirmMail','DashboardController@sendConfirmMail')->name('sendConfirmMail')->middleware('auth');


Route::get('sign','DashboardController@registerWithOutAutenticate')->name('register.withOutLogin');
Route::post('state/get','CountryController@getState')->name('get.State');
//Route::post('city/get','CountryController@getCity')->name('get.City');
Route::post('city/get/withOutState','CountryController@getCityWithOutState')->name('get.CityWithOutState');


Route::get('/autocomplete/fetch', 'CountryController@fetch')->name('autocomplete.fetch');
Route::get('/autocomplete/city/fetch', 'CountryController@fetchCity')->name('autocomplete.fetch.city');


Route::get('/test', 'DashboardController@test')->name('test');

Route::get('/register/verify/{code}', 'ClientController@verify');

Route::get('/login','Auth\ClientLoginController@showLoginForm')->name('login');

Route::post('/login', 'Auth\ClientLoginController@login')->name('client.login.submit');
Route::post('/logout', 'Auth\ClientLoginController@logout')->name('logout');

Route::group(['prefix'=>'client','middleware' => 'auth:customusers'], function() {
    Route::get('/', 'ClientController@index')->name('client.dashboard');
    Route::get('/buyer/{coin_id?}', 'ClientController@buyer')->name('client.buyer');
    Route::post('/buy', 'ClientController@buy')->name('client.buy');
    Route::get('/settings', 'ClientController@settings')->name('client.settings');
    Route::get('/profile/account', 'ClientController@profileAccount')->name('client.account');
    Route::post('/secondPassword','ClientController@sendRecoverySecondPassword')->name('sendRecoverySecondPassword');

//payment form
Route::get('/payment', 'PaymentController@index');
// route for processing payment
Route::post('paypal', 'PaymentController@payWithpaypal')->name('client.paypal');
// route for check status of the payment
Route::get('status', 'PaymentController@getPaymentStatus')->name('status');

});


