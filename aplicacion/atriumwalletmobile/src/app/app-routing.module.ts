import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  { path: 'tab', loadChildren: './tabs/tabs.module#TabsPageModule' },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'login', loadChildren: './public/login/login.module#LoginPageModule' },
  { path: 'dashboard', loadChildren: './members/dashboard/dashboard.module#DashboardPageModule' },
  { path: 'register', loadChildren: './public/register/register.module#RegisterPageModule' },
  { path: 'my-wallet', loadChildren: './members/my-wallet/my-wallet.module#MyWalletPageModule' },   
  { path: 'welcome', loadChildren: './welcome/welcome.module#WelcomePageModule' },
  { path: 'newhome', loadChildren: './members/tab/tab.module#TabPageModule' },
  { path: 'buy-creditcard', loadChildren: './buy-creditcard/buy-creditcard.module#BuyCreditcardPageModule' },
  { path: 'buy-wiretransfer', loadChildren: './buy-wiretransfer/buy-wiretransfer.module#BuyWiretransferPageModule' },
  { path: 'buy-atm', loadChildren: './buy-atm/buy-atm.module#BuyAtmPageModule' },
  { path: 'tabs', loadChildren: './members/tab/tab.module#TabPageModule' },  { path: 'confirm-register', loadChildren: './confirm-register/confirm-register.module#ConfirmRegisterPageModule' }


 


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
