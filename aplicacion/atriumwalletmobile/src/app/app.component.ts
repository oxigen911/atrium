import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { LanguageService } from './services/lenguage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Inicio',
      url: '/newhome/tab1',
      icon: 'home'
    },
    {
      title: 'Home',
      url: '/tab/tab1',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    },
    {
      title: 'DCW cash machine',
      url: '/logout',
      icon: 'assets/side-menu/newatm.svg'
    },
    {
      title: 'Friends',
      url: '/tabs/tab1',
      icon: 'assets/side-menu/address.svg'
    },
    {
      title: 'Settings',
      url: '/logout',
      icon: 'assets/side-menu/config.svg'
    },
    {
      title: 'inbox',
      url: '/logout',
      icon: 'assets/side-menu/message.svg'
    },
    {
      title: 'FAQs',
      url: '/logout',
      icon: 'assets/side-menu/faq.svg'
    },
    {
      title: 'Legal Information',
      url: '/logout',
      icon: 'assets/side-menu/address.svg'
    },
    {
      title: 'Support',
      url: '/logout',
      icon: 'assets/side-menu/address.svg'
    },
    {
      title: 'Logout',
      url: '/logout',
      icon: 'assets/side-menu/logout.svg'
    }  

  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private storage: Storage,
    private languageService: LanguageService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    console.log("appComponent.");
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    this.platform.backButton.subscribeWithPriority(999, () => {
      navigator['app'].exitApp();
    });

    
    this.storage.get('session_storage').then((res)=>{
      if(res == null){
        this.router.navigate(['/login']);
      }else{
        this.router.navigate(['newhome/tab1']);
      }
    });

    this.languageService.setInitialAppLanguage();
  }

  logout(){
    console.log("logout");
    this.storage.remove('session_storage');
    this.router.navigate(['/login']);
  }
}
