import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpModule } from '@angular/http';
import { NavController} from 'ionic-angular';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ApiService } from 'src/app/services/api.service';
import { CoinmanagementPopoverComponent } from './coinmanagement-popover/coinmanagement-popover.component';
import { PortfolioPopoverComponent } from './portfolio-popover/portfolio-popover.component';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { MarketsharePopoverComponent } from './marketshare-popover/marketshare-popover.component';
import { IonicSelectableModule } from 'ionic-selectable';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function createTranslateLoader(http: HttpClient){
  return new TranslateHttpLoader(http,'assets/i18n/','.json');
}


@NgModule({
  declarations: [AppComponent,CoinmanagementPopoverComponent, PortfolioPopoverComponent, MarketsharePopoverComponent],
  entryComponents: [CoinmanagementPopoverComponent, PortfolioPopoverComponent, MarketsharePopoverComponent],
  imports: [
    BrowserModule,
    // tslint:disable-next-line: deprecation
    HttpModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader:{
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps:[HttpClient]
      } 
    }),
    IonicModule.forRoot(),
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    IonicSelectableModule,
    MatInputModule,
    FormsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
