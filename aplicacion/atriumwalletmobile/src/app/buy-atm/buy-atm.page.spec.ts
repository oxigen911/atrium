import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyAtmPage } from './buy-atm.page';

describe('BuyAtmPage', () => {
  let component: BuyAtmPage;
  let fixture: ComponentFixture<BuyAtmPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyAtmPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyAtmPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
