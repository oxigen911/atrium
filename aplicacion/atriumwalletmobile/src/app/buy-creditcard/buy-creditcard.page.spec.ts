import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyCreditcardPage } from './buy-creditcard.page';

describe('BuyCreditcardPage', () => {
  let component: BuyCreditcardPage;
  let fixture: ComponentFixture<BuyCreditcardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyCreditcardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyCreditcardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
