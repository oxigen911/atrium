import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router,NavigationExtras} from '@angular/router';
import { Chart } from 'chart.js';
import { PopoverController, LoadingController, AlertController } from '@ionic/angular';
import { CoinmanagementPopoverComponent } from '../coinmanagement-popover/coinmanagement-popover.component';
import { Coin } from '../coin';
import { ApiService } from '../services/api.service';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Storage } from '@ionic/storage';




@Component({
  selector: 'app-buy-creditcard',
  templateUrl: './buy-creditcard.page.html',
  styleUrls: ['./buy-creditcard.page.scss'],
})
export class BuyCreditcardPage implements OnInit {
  coin: any;
  quantity: any;
  id: any;
  loaderToShow: any;
  constructor( 
    private route: ActivatedRoute, 
    private router: Router,
    private popoverController: PopoverController,
    private apiService: ApiService,
    public formBuilder: FormBuilder,    
    private storage: Storage,
    private loadingController: LoadingController,
    private alertController: AlertController,
    ) {
      
      this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.coin = this.router.getCurrentNavigation().extras.state.coin;
        this.quantity = this.router.getCurrentNavigation().extras.state.quantity;
        console.log(this.coin);
      }
    });


   }
  validations_form: FormGroup;

  validation_messages = {
    'NameCard': [
      { type: 'required', message: 'Name Card is required.' },
      { type: 'minlength', message: 'Name card must be at least 3 characters long.' },
      { type: 'maxlength', message: 'Name card cannot be more than 30 characters long.' }
    ],
    'NumberCard': [
      { type: 'required', message: 'Number card is required.' },
      { type: 'minlength', message: 'Number card must be at least 3 characters long.' },
      { type: 'maxlength', message: 'Number card cannot be more than 30 characters long.' }
    ],
    'ValidThru': [
      { type: 'required', message: 'Valid thru is required.' },
      { type: 'minlength', message: 'Valid thru must be at least 3 characters long.' },
      { type: 'maxlength', message: 'Valid thru cannot be more than 30 characters long.' }
    ],
    'CCV': [
      { type: 'required', message: 'CCV is required.' },
      { type: 'minlength', message: 'CCV must be at least 3 characters long.' },
      { type: 'maxlength', message: 'CCV cannot be more than 30 characters long.' }
    ],  
  };
  

  ngOnInit() {
    
    this.storage.get('session_storage').then((res)=>{
      if(res == null){
        this.id = null;
      }else{
       this.id =  res.id;
      }

    });  
    
    this.validations_form = this.formBuilder.group({
      NameCard: new FormControl('', Validators.compose([
        Validators.maxLength(30),
        Validators.minLength(3),
        Validators.required
      ])),
      NumberCard: new FormControl('', Validators.compose([
        Validators.maxLength(30),
        Validators.minLength(3),
        Validators.required
      ])),
      ValidThru: new FormControl('', Validators.compose([
        Validators.maxLength(30),
        Validators.minLength(3),
        Validators.required
      ])),
      CCV: new FormControl('', Validators.compose([
        Validators.maxLength(30),
        Validators.minLength(3),
        Validators.required
      ]))  
    
    
    });
 
  }

  register(value){
    this.showLoader();

    this.apiService.postData({
                'coin':this.coin.id,
                'unity':this.quantity,
                'id':this.id,
                'method_payment':3 }
    ,'buy').subscribe(data => {
      this.presentAlert(data.msj);
      if(data.state==200){
        this.router.navigate(['/tab/tab1']);
      }
   });


  }

  async presentAlert(msj) {
    const alert = await this.alertController.create({
      /*header: 'Alert',
      subHeader: 'Subtitle',*/
      message:msj, 
      buttons: ['OK'],
      cssClass: 'alertCustomCss'
    });

    await alert.present();
  }

 
  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: 'Loading.....'
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed!');
      });
    });
    this.hideLoader();
  }

  hideLoader() {
    setTimeout(() => {
      this.loadingController.dismiss();
    }, 4000);
  }


}
