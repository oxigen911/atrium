import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router,NavigationExtras} from '@angular/router';
import { Chart } from 'chart.js';
import { PopoverController, LoadingController } from '@ionic/angular';
import { CoinmanagementPopoverComponent } from '../coinmanagement-popover/coinmanagement-popover.component';
import { Coin } from '../coin';
import { ApiService } from '../services/api.service';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-buy-wiretransfer',
  templateUrl: './buy-wiretransfer.page.html',
  styleUrls: ['./buy-wiretransfer.page.scss'],
})
export class BuyWiretransferPage implements OnInit {
  coin: any;
  quantity: any;
  id: any;
  loaderToShow: any;

  constructor(
          private route: ActivatedRoute, 
          private router: Router,
          private popoverController: PopoverController,
          private apiService: ApiService,
          public formBuilder: FormBuilder,
          public alertController: AlertController,
          public loadingController: LoadingController,
          private storage: Storage
   ) { 

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.coin = this.router.getCurrentNavigation().extras.state.coin;
        this.quantity = this.router.getCurrentNavigation().extras.state.quantity;
        console.log(this.coin);
      }
    });

   }

  ngOnInit() {

    
    this.storage.get('session_storage').then((res)=>{
      if(res == null){
        this.id = null;
      }else{
       this.id =  res.id;
      }

    });
  }

  
   register(){
    
    this.showLoader();
     this.apiService.postData({
                'coin':this.coin.id,
                'unity':this.quantity,
                'id':this.id,
                'method_payment':3 }
    ,'buy').subscribe(data => {
        this.presentAlert(data.msj);
          if(data.state==200){
              this.router.navigate(['/tab/tab1']);
         }
   });


  }

  async presentAlert(msj) {
    const alert = await this.alertController.create({
      /*header: 'Alert',
      subHeader: 'Subtitle',*/
      message:msj, 
      buttons: ['OK'],
      cssClass: 'alertCustomCss'
    });

    await alert.present();
  }

 
  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: 'Loading.....'
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed!');
      });
    });
    this.hideLoader();
  }

  hideLoader() {
    setTimeout(() => {
      this.loadingController.dismiss();
    }, 4000);
  }

}
