import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-confirm-register',
  templateUrl: './confirm-register.page.html',
  styleUrls: ['./confirm-register.page.scss'],
})
export class ConfirmRegisterPage implements OnInit {

  constructor(public alertController: AlertController) { }

  ngOnInit() {
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      /*header: 'Alert',
      subHeader: 'Subtitle',*/
      message: "Congratulations, your request has been sent. Son you will be contacted by our human research department",
      buttons: ['OK'],
      cssClass: 'alertCustomCss'
    });

    await alert.present();
  }

}
