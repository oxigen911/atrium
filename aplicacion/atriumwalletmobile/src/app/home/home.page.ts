import { Component } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Coin } from '../coin';
import { Router,NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss','../../common.scss'],
})
export class HomePage {
  loaderToShow: any;
   
  user = {
    name: 'Simon Grimm',
    website: 'www.ionicacademy.com',
    address: {
      zip: 48149,
      city: 'Muenster',
      country: 'DE'
    },
    interests: [
      'Ionic', 'Angular', 'YouTube', 'Sports'
    ]
  };

  currentFilter: string = '';
  visibleCoins: Coin[] = []; /* Coins that are visible after filtering */
  coins: Coin[] = []; /* declare coins as array */

  constructor(
    private apiService: ApiService,
    private router: Router,
    private storage: Storage,

    ) {
      this.apiService.getCoins().subscribe(data => {
      this.coins = data;
      this.visibleCoins = data;
    });


  }

  ngOnInit() {
    this.storage.get('session_storage').then((res)=>{
      if(res == null){
        this.router.navigate(['/login']);
      }
    });
  }


  openDetailsWithState(coin: Coin) {
    let navigationExtras: NavigationExtras = {
      state: {
        user: coin
      }
    };
    this.router.navigate(['list'], navigationExtras);
  }


}
