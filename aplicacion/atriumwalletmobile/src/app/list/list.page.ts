import { Component, OnInit, ViewChild, ElementRef, AfterViewInit  } from '@angular/core';
import { ActivatedRoute, Router,NavigationExtras} from '@angular/router';
import { Chart } from 'chart.js';
import { PopoverController, LoadingController, AlertController } from '@ionic/angular';
import { CoinmanagementPopoverComponent } from '../coinmanagement-popover/coinmanagement-popover.component';
import { Coin } from '../coin';
import { ApiService } from '../services/api.service';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-list',
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        height: '200px',
        opacity: 1,
        backgroundColor: 'yellow'
      })),
      state('closed', style({
        height: '100px',
        opacity: 0.5,
        backgroundColor: 'green'
      })),
      transition('open => closed', [
        animate('1s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
    ]),
  ],
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss','../../main.css']
})


export class ListPage implements OnInit {
  coin: any;
  coins : any;
  methodPagoPaypal = false;
  methodCreditCard = false;
  methodWireTranfer = false;
  methodAtm = false;
  dataValid2= false;
  public show:boolean = false;
  public buttonName:any = 'Show';
  qty=0;
  price = 0;
  currency: any;
  qtyConv=0;
  txtMethod ='';
  loaderToShow: any;
  id: any;

  constructor(private route: ActivatedRoute, 
              private router: Router,
              private popoverController: PopoverController,
              private apiService: ApiService, 
              private payPal: PayPal,
              private loadingController: LoadingController,
              private storage: Storage,
              private alertController: AlertController) {
    
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.coin = this.router.getCurrentNavigation().extras.state.user;
      }
    });

    this.apiService.getCoins().subscribe(data => {
      this.coins = data;
    });
    
    
}

  ngOnInit() {
    this.currency = this.coin.id;
    this.price = this.coin.actual_price;
    this.storage.get('session_storage').then((res)=>{
        if(res == null){
          this.id = null;
        }else{
        this.id =  res.id;
        }

      });
  }
  
 
  async showPopover(coin: Coin) {
    const popoverElement = await this.popoverController.create({
      component: CoinmanagementPopoverComponent,
      componentProps: {
        source: 'page.coins',
        object: coin
      }
    });
    return await popoverElement.present();
  }

  toggle(opcion) {
    this.show = !this.show;
    switch(opcion){
      case 'paypal':
        this.methodPagoPaypal=true;
        this.methodAtm=false;
        this.methodCreditCard=false;
        this.methodWireTranfer=false;
        this.txtMethod='PayPal';
        break;
      case 'atm':
          this.methodPagoPaypal=false;
          this.methodAtm=true;
          this.methodCreditCard=false;
          this.methodWireTranfer=false;
          this.txtMethod='ATM';
          break;
       case 'credit':
          this.methodPagoPaypal=false;
          this.methodAtm=false;
          this.methodCreditCard=true;
          this.methodWireTranfer=false;
          this.txtMethod='Credit Card';
         break;
       case 'wire':
          this.methodPagoPaypal=false;
          this.methodAtm=false;
          this.methodCreditCard=false;
          this.methodWireTranfer=true; 
          this.txtMethod='Wire Transfer';
                  break;
       default:
         console.log("revisar esta opcion"+opcion);       
    }
    // CHANGE THE NAME OF THE BUTTON.
    if(this.show)  
      this.buttonName = "Hide";
    else
      this.buttonName = "Show";
  }
  get dataValid() : boolean {
     if( (this.methodAtm!=false || this.methodCreditCard!=false || this.methodPagoPaypal!=false || this.methodWireTranfer!=false) 
     && this.qty!=0 && this.currency!=0 && this.currency!=''){
      return true;

     }else{
        return false;
     }
    
  }

  confirm() {
    this.dataValid2=true;
 }

 changeEnvironment(event){

  this.apiService.postData({'id':event},'coin').subscribe(data => {
     this.price = data.actual_price;
     this.coin = data;
  });
 
  }
  toProceed(){


    let navigationExtras: NavigationExtras = {
      state: {
        coin: this.coin,
        method: this.txtMethod,
        quantity: this.qty, 
        quantityConv: this.qtyConv,
      }
    };


    if(this.methodPagoPaypal){
      this.comprar();
    }

    if(this.methodCreditCard){
      this.router.navigate(['buy-creditcard'], navigationExtras);
    }

    if(this.methodWireTranfer){
      this.router.navigate(['buy-wiretransfer'], navigationExtras);
    }

    if(this.methodAtm){
      this.router.navigate(['buy-atm'], navigationExtras);
    }


    

  }

  comprar(){
    this.showLoader();

    this.payPal.init({
        PayPalEnvironmentProduction: '',
        PayPalEnvironmentSandbox: 'AaKuOAk0y4TWUJJAkNSSerLF0JMbFpgHRkqbq0mfZfvLC-XZlK-ukxF5WkyTZF8y4abg7_NGSVuydg8u'
    }).then(() => {
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        acceptCreditCards: true,
        languageOrLocale: 'en_US',
        merchantName: 'Description',
        merchantPrivacyPolicyURL: '',
        merchantUserAgreementURL: ''
      })).then(() => {
        //let detail = new PayPalPaymentDetails('19.99', '0.00', '0.00');
        let payment = new PayPalPayment(this.qty.toString(), 'EUR', 'Buy: '+ this.qtyConv+' '+ this.coin.initial, 'sale');
        this.payPal.renderSinglePaymentUI(payment).then((response) => {
          console.log('pagamento efetuado')
          if(response.response.state=="approved"){
            this.apiService.postData({
              'coin':this.coin.id,
              'unity':this.qty,
              'id':this.id,
              'method_payment':2 }
                ,'buy').subscribe(data => {
                    this.presentAlert(data.msj);
                      if(data.state==200){
                        this.presentAlert("buy approved");
                        this.router.navigate(['/tab/tab1']);
                    }
              });
            
            
          }else{
            this.presentAlert("buy failed");
          }
        
        }, (response) => {
          console.log('erro ao renderizar o pagamento do paypal'+response);
        })
      })
    })
  }

  
  async presentAlert(msj) {
    const alert = await this.alertController.create({
      /*header: 'Alert',
      subHeader: 'Subtitle',*/
      message:msj, 
      buttons: ['OK'],
      cssClass: 'alertCustomCss'
    });

    await alert.present();
  }

 
  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: 'Loading.....'
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed!');
      });
    });
    this.hideLoader();
  }

  hideLoader() {
    setTimeout(() => {
      this.loadingController.dismiss();
    }, 4000);
  }


}
