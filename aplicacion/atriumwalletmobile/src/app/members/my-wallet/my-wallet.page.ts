import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Coin } from '../../coin';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-my-wallet',
  templateUrl: './my-wallet.page.html',
  styleUrls: ['./my-wallet.page.scss'],
})
export class MyWalletPage implements OnInit {

  coins: Coin[] = []; /* declare coins as array */
  idClient: any;

  constructor(private apiService: ApiService, private storage: Storage) {

    storage.get('session_storage').then((val) => {
      this.idClient = val.id;
      //console.log('val' + val.id);
      //console.log('iduser  dentro' + this.idClient);

       //id ejemplo: 213
        console.log('iduser ' + this.idClient);
        this.apiService.getWalletUser(this.idClient).subscribe(data => {
        this.coins = data;
        console.log(data);
        //this.total = data[1][0].total;
        //console.log(this.total);
      });

    });



   }

  ngOnInit() {
  }

}
