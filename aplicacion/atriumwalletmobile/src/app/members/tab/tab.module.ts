import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabPage } from './tab.page';

const routes: Routes = [
  {
   path:'',
   component: TabPage,
   children:[
      {
        path:'tab1',
        loadChildren:'../../newhome/newhome.module#NewhomePageModule'
      },
      {
        path:'tab2',
        loadChildren:'../dashboard/dashboard.module#DashboardPageModule'
      }, 
      { path: 'my-wallet', loadChildren: '../my-wallet/my-wallet.module#MyWalletPageModule' }  
   ] 
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabPage]
})
export class TabPageModule {}
