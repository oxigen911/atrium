import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ViewChild } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Coin } from '../coin';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-newhome',
  templateUrl: './newhome.page.html',
  styleUrls: ['./newhome.page.scss'],
})
export class NewhomePage implements OnInit {

  currentFilter: string = '';
  visibleCoins: Coin[] = []; /* Coins that are visible after filtering */
  total:any;
  coins: Coin[] = []; /* declare coins as array */
  ishiddenTotal: any;
  idClient: any;

  constructor(private apiService: ApiService, private storage: Storage) {
   
    storage.get('session_storage').then((val) => {
      this.idClient = val.id;
      //console.log('val' + val.id);
      //console.log('iduser  dentro' + this.idClient);

       //id ejemplo: 213
        console.log('iduser ' + this.idClient);
        this.apiService.getCoinsUser(this.idClient).subscribe(data => {
        this.coins = data[0];
        this.visibleCoins = data[0];
        this.total = data[1][0].total;
        //console.log(this.total);
      });

    });

  
 
    
  }

  ngOnInit() {

    setTimeout(() => {
      //this.createBarChart();
    }, 6000);
    this.ishiddenTotal = true;  

  }

  @ViewChild('barChart') barChart;

  bars: any;
  colorArray: any;

  createBarChart() {

    var coinsNames = [];

    for (var i = 0; i < this.coins.length; i+=1) {
      coinsNames.push([this.coins[i].initial,this.coins[i].name]);
    }

    this.bars = new Chart(this.barChart.nativeElement, {
      type: 'bar',
      data: {
        //labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
        //labels: [this.coins[0].name, this.coins[1].name, 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
        labels: coinsNames,
        datasets: [{
          label: 'Viewers in millions',
          data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
          backgroundColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
          borderColor: 'rgb(38, 194, 129)',// array should have same number of elements as number of dataset
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  ShowHideTotal(){
    if(this.ishiddenTotal){
      this.ishiddenTotal = false;
    }else
      {this.ishiddenTotal = true;}   
  }

}
