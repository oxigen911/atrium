import { Component, OnInit } from '@angular/core';
import { ElementRef } from '@angular/core';
import { Renderer2 } from '@angular/core';
import { ViewChild } from '@angular/core';
import { ApiService } from '../../services/api.service';
import {Storage} from '@ionic/storage';
import { IonicPage, Loading } from 'ionic-angular';
import { AlertController, LoadingController,ToastController } from '@ionic/angular';
import { HomePage } from '../../home/home.page';
import { async } from '@angular/core/testing';
import { Route } from '@angular/router';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
 
  @ViewChild("myButton") myButton: ElementRef;
  @ViewChild("inputPasswordName") inputPasswordName: ElementRef;
  loading: Loading;
  registerCredentials = { email: '', password: '' };

  constructor(
    private auth: ApiService,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private router: Router,
    private storage: Storage,
    public toastCtrl: ToastController,
    public formBuilder: FormBuilder,
  ) {}

  validations_form: FormGroup;

  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email must be at least 3 characters long.' },
      { type: 'pattern', message: 'Email not valid.' },
    ],
    'password': [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password must be at least 3 characters long.' },
    ]  
  };
  

  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.minLength(3),
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ]))  
    
    
    });
  }

  public toRegister() {
    this.router.navigate(['welcome']);
  }

  public login(value) {
    
    let body = {
      email: value.email,
      password: value.password,
    };

    this.auth.postData(body, 'login').subscribe(async data =>{
      var alertpesan = data.msg;
      if(data.success){
        this.storage.set('session_storage', data.result);
        this.router.navigate(['newhome']);        
        console.log(data);
      }else{
        const toast = await this.toastCtrl.create({
        message: alertpesan,
        duration: 2000
        });
        toast.present();
      }
    });

  }

  async  showLoading() {
        const loadingElement = await this.loadingCtrl.create({
        message: 'Please wait...',
        spinner: 'crescent',
        duration: 2000
      });
      return await loadingElement.present();

  }

  async  showError(text) {
    const alert = await this.alertCtrl.create({
      header: 'Fail',
      message: text,
      buttons: ['OK']
    });
    return await alert.present();
  }


  get Formvalid() : boolean {
     if(this.registerCredentials.email=='' || this.registerCredentials.password=='' || this.registerCredentials.password.length<=5){
       
        return false;

     }else{
        return true;
     }
    
  }

}
