import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router';
import {Storage} from '@ionic/storage';
import {ToastController } from '@ionic/angular';
import { IonicSelectableComponent } from 'ionic-selectable';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UsernameValidator } from '../../validators/username.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(private api: ApiService,    private storage: Storage,private router: Router,
              public toastCtrl: ToastController,    public formBuilder: FormBuilder    ) { 
      
                this.api.getCountry().subscribe(data => {
                  this.country = data;
                  this.country2 = data;
                });
                        
    }
  validations_form: FormGroup;
  matching_passwords_group: FormGroup;

country = [];
country2 = [];
city = [];
city2 = [];
document = [{"id":"DNI","text":"DNI"},{"id":"ID","text":"ID"},{"id":"PASSPORT","text":"PASSPORT"}];
  registerCredentials = { 
    FirstName: '', 
    LastName: '', 
    Email: '', 
    Password: '',
    Password2: '', 
    BirthDate: '', 
    CountryBirth: { "id":"null","text":"Select Country"},
    CountryResidency:{ "id":"null","text":"Select Country"},
    CurrentCountry: { "id":"null","text":"Select Country"},
    
    DocumentBack:'',
    DocumentFront:'',
    DocumentNumber:'',
    DocumentType:{ "id":"null","text":"Select Type Document"},
    CurrentCity:'',
    CityBirth:'',
    DocumentIssueDate:'',
    DocumentexpirationDate:''
  };	
  
  validation_messages = {
    'FirstName': [
      { type: 'required', message: 'name is required.' },
      { type: 'minlength', message: 'name must be at least 3 characters long.' },
      { type: 'maxlength', message: 'name cannot be more than 30 characters long.' },
      { type: 'validUsername', message: 'Your name has already been taken.' }
    ],
    'LastName': [
      { type: 'required', message: 'Surname is required.' },
      { type: 'minlength', message: 'Surname must be at least 3 characters long.' },
      { type: 'maxlength', message: 'surname cannot be more than 30 characters long.' },
      { type: 'validUsername', message: 'Your surname has already been taken.' }
    ],
    'Email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please wnter a valid email.' }
    ],
    'Password': [
      { type: 'required', message: 'Surname is required.' },
      { type: 'minlength', message: 'Surname must be at least 6 characters long.' }
    ],
    'SecondPassword': [
      { type: 'required', message: 'Surname is required.' },
      { type: 'minlength', message: 'Surname must be at least 6 characters long.' }
    ],
    'terms1': [
      { type: 'pattern', message: 'You must accept terms and conditions.' }
    ], 
    'terms2': [
      { type: 'pattern', message: 'You must accept terms and conditions.' }
    ],
    'terms3': [
      { type: 'pattern', message: 'You must accept terms and conditions.' }
    ],
    'BirthDate': [
      { type: 'required', message: 'DateBirth is required.' }
    ],
    
    'CountryBirth': [
      { type: 'required', message: 'BirthCountry is required.' }
    ],
    
    'CountryResidency': [
      { type: 'required', message: 'CurrentCountry is required.' }
    ]
    
  };
    
  ngOnInit() {
     
    this.validations_form = this.formBuilder.group({
        FirstName: new FormControl('', Validators.compose([
          UsernameValidator.validUsername,
          Validators.maxLength(30),
          Validators.minLength(3),
          Validators.required
        ])),
        LastName: new FormControl('', Validators.compose([
          UsernameValidator.validUsername,
          Validators.maxLength(30),
          Validators.minLength(3),
          Validators.required
        ])),
        Email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])),

        Password: new FormControl('', Validators.compose([
          UsernameValidator.validUsername,
          Validators.minLength(6),
          Validators.required
        ])),

        SecondPassword: new FormControl('', Validators.compose([
          UsernameValidator.validUsername,
          Validators.minLength(6),
          Validators.required
        ])),
        terms1: new FormControl(false, Validators.pattern('true')),
        terms2: new FormControl(false, Validators.pattern('true')),
        terms3: new FormControl(false, Validators.pattern('true')),
        BirthDate: new FormControl('', Validators.required),
        CountryBirth: new FormControl(null, Validators.required),
        CountryResidency: new FormControl(null, Validators.required),
      });
    
  
    
  }
  
  public register(value) {
    this.api.postData(value, 'register').subscribe(async data =>{
      var alertpesan = data.msg;
      console.log(data);
      if(data.success){
        /*const toast = await this.toastCtrl.create({
            message: 'Login Succesfully.',
            duration: 2000
         });
        toast.present();*/
        this.router.navigate(['confirm-register']);
       
     }else{
        const toast = await this.toastCtrl.create({
          message: alertpesan,
          duration: 2000
           });
        toast.present();
      }
    });

  }
    portChangeBirthCountry(event: {
      component: IonicSelectableComponent,
      value: any 
      }) {
      this.api.getCity(event.value.id).subscribe(data => {
          this.city= data;
        });
    }
    
    portChangeCurrentCountry(event: {
      component: IonicSelectableComponent,
      value: any 
      }) {
        this.api.getCity(event.value.id).subscribe(data => {
          this.city2= data;
        });
    }

}
