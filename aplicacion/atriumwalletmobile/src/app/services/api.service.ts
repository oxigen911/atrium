import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Headers, RequestOptions,Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { LoadingController } from '@ionic/angular';


@Injectable()
export class ApiService {
  url: string;
  apiKey: string;
  usuario: string;
  queryNotFound: string;
  api_token: string;

  static readonly LOGIN_URL = 'https://atriumwallet.com/api/login'
  static readonly REGISTER_URL = 'http://contoh.dev/api/register';
  access: boolean;
  token: string;
  loaderToShow: any;

  constructor(public http: Http, public loadingController: LoadingController) {
    this.url = 'https://atriumwallet.com/api/';
  }

    postData(body, file){
      let type = "application/json; charset=UTF-8";
      let headers = new Headers({ 'Content-Type': type });
      let options2 = new RequestOptions({ headers: headers });
  
      return this.http.post(this.url + file, JSON.stringify(body), options2)
      .map(res => res.json());
    }

    getCoins() {
      return this.http.get('https://atriumwallet.com/api/coins')
        .pipe(
          map(data => {
            return this.extractData(data);
          }),
          catchError(err => {
            return this.catchError(err);
          }),
          tap(response => {
            this.logResponse(response);
          })
        );
    }


    getCountry() {
      return this.http.get('https://atriumwallet.com/api/country/all')
        .pipe(
          map(data => {
            return this.extractData(data);
          }),
          catchError(err => {
            return this.catchError(err);
          }),
          tap(response => {
            this.logResponse(response);
          })
        );
    }

    getCity(pais) {
      return this.http.get('https://atriumwallet.com/api/city/'+pais)
        .pipe(
          map(data => {
            return this.extractData(data);
          }),
          catchError(err => {
            return this.catchError(err);
          }),
          tap(response => {
            this.logResponse(response);
          })
        );
    }

      /* Coinmarketcap */
  getGlobal() {
    return this.http.get('https://api.coinmarketcap.com/v1/global/')
      .pipe(
        map(data => {
          return this.extractData(data);
        }),
        catchError(err => {
          return this.catchError(err);
        }),
        tap(response => {
          return this.logResponse(response);
        })
      );
  }


  getCoinsUser(user) {
    return this.http.get('https://atriumwallet.com/api/coin/'+user)
      .pipe(
        map(data => {
          return this.extractData(data);
        }),
        catchError(err => {
          return this.catchError(err);
        }),
        tap(response => {
          this.logResponse(response);
        })
      );
  }

  getWalletUser(user) {
    return this.http.get('https://atriumwallet.com/api/movements/'+user)
      .pipe(
        map(data => {
          return this.extractData(data);
        }),
        catchError(err => {
          return this.catchError(err);
        }),
        tap(response => {
          this.logResponse(response);
        })
      );
  }
    
    private catchError(error: Response | any) {
      console.log(error);
      return Observable.throw(error.json().error || "Server error!");
    }
    private logResponse(res: Response) {
      //this.showLoader();
    }
    private extractData(res: Response) {
      //this.hideLoader();
      return res.json();
    }


    showLoader() {
      this.loaderToShow = this.loadingController.create({
        message: 'This Loader will Not AutoHide'
      }).then((res) => {
        res.present();
  
        res.onDidDismiss().then((dis) => {
          console.log('Loading dismissed!');
        });
      });
      this.hideLoader();
    }
  
    hideLoader() {
      setTimeout(() => {
        this.loadingController.dismiss();
      }, 4000);
    }

}
