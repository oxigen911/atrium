import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { LanguageService } from '../services/lenguage.service';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  color1: string;
  color2: string;
  color3: string;
  color4: string;

  constructor(private router: Router, public alertController: AlertController, private languageService: LanguageService) {
   }

  ngOnInit() {
    this.languageService.setLanguage('en');
    this.color1 = 'danger';
    this.color2 = this.color3= this.color4 ='medium';
    
  }

  public toRegister() {
    this.router.navigate(['register']);
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      /*header: 'Alert',
      subHeader: 'Subtitle',*/
      message: "Siamo spiacenti ma la lingua da te scelta non è momentaneamente disponibile peraggiornamenti. Scegli un'altra lingua e dopo I'apertura del wallet potrai cambiarla nel menu impostazioni del tuo profilo personale.",
      buttons: ['OK'],
      cssClass: 'alertCustomCss'
    });

    await alert.present();
  }

  selectLang(lng){
    if(lng=='en'){
      this.color1='danger';
      this.color2=this.color3=this.color4='medium';
    }else if(lng=='it'){
      this.color2='danger';
      this.color1=this.color3=this.color4='medium';
  
    }else if(lng=='es'){
      this.color3='danger';
      this.color1=this.color2=this.color4='medium';
    }else{
      this.color4='danger';
      this.color1=this.color2=this.color3='medium';
    }
     this.languageService.setLanguage(lng);
  }

}
